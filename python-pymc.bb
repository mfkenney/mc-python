#
# Bitbake build script for python-pymc.
#
DESCRIPTION = "Python modules and scripts for the Mooring Controller"
SECTION = "devel/python"
PRIORITY = "optional"
LICENSE = "GPL"
RDEPENDS = "python-core python-io python-dbus python-misc python-pygobject python-textutils python-threading python-subprocess python-urwid python-pyserial"
SRCNAME = "pymc"
PR = "r1"

SRC_URI = "http://wavelet.apl.washington.edu/~mike/python/${SRCNAME}-${PV}.tar.gz"
S = "${WORKDIR}/${SRCNAME}-${PV}" 

inherit distutils

FILES_${PN} += "${datadir}/pymc"
FILES_${PN} += "/etc/dbus-1/system.d"
FILES_${PN} += "/usr/share/dbus-1/services"

do_install_append() {
    install -d ${D}/etc/dbus-1/system.d
    install -d ${D}/usr/share/dbus-1/services
    install -m 0644 ${S}/cfg/archiver.conf ${D}/etc/dbus-1/system.d/
    install -m 0644 ${S}/cfg/eng.conf ${D}/etc/dbus-1/system.d/
    install -m 0644 ${S}/cfg/hmc.Archiver.service ${D}/usr/share/dbus-1/services/
}
