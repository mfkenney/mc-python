# -*- coding: utf-8 -*-
import pytest
from pymc.nmea import Sentence


DATA = b'$GPGGA,163901,2115.8711,N,11735.8436,E,2,7,0.4,17,M,11,M,38,0653'

@pytest.fixture
def sentence():
    return Sentence(DATA)


def test_sentence_parse(sentence):
    assert sentence.id == 'GPGGA'
    assert len(sentence) == 14
    assert sentence[0] == '163901'
    assert sentence[-1] == '0653'
