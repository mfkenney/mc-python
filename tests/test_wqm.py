#
# Test parsing the SeaFET data records
#
import pytest
import logging
import yaml
from decimal import Decimal
from io import BytesIO
from pymc.sensors.device import sensor_config


WQMINPUT = b"""\x00kjshlhdlk
WQM,145,100516,173514,20.2342,-0.06,0.010,8.865,0.146,0.506,998.164\r

BLIS:  Active every 1 Hours\r
Pumped BLIS: NOT Selected\r
"""

WQMCONFIG = r"""
---
name: wqm/1
description: Wetlabs Water Quality Monitor
variables: [temperature, pressure, salinity, dissolved_oxy, chlorophyll, turbidity, density]
units: {temperature: degC, pressure: dbars, salinity: psu, dissolved_oxy: mg/l, chlorophyll: ug/l, turbidity: 'NTU', density: 'kg/m^3'}
precision: {temperature: '0.0001', pressure: '0.01', salinity: '0.001', dissolved_oxy: '0.001', chlorophyll: '0.001', turbidity: '0.001', density: '0.001'}
interface:
  module: wqm
  device: /dev/ttyUSB2
  baud: 19200
  eor: "\r\n"
result:
  fields: [4, 5, 6, 7, 8, 9, 10]
"""


@pytest.fixture(autouse=True)
def patch_serial_read(monkeypatch):
    buf = BytesIO(WQMINPUT)
    monkeypatch.setattr('pymc.sensors.device.SerialPort.read', buf.read)


@pytest.fixture
def sensor():
    doc = yaml.load(WQMCONFIG)
    # Not connecting to real hardware ...
    del doc['interface']['device']
    if 'gpio' in doc['interface']:
        del doc['interface']['gpio']
    config, klass = sensor_config(doc)
    assert klass is not None
    return klass(config)


def test_sample(caplog, sensor):
    caplog.set_level(logging.INFO)
    with sensor:
        sensor.wait_for_ready()
        sensor.pre_sample()
        ts, result = sensor.sample()
    assert len(result) == 7
    assert result[0] == Decimal('20.2342')
