# -*- coding: utf-8 -*-
import pytest
from pymc.power import PowerSwitch
import logging


def test_dio(dioport):
    dioport.set(0x55)
    assert dioport.test(0) is True
    assert dioport.test(3) is False
    assert dioport.test(6) is True


@pytest.mark.parametrize("line", list(range(0, 8)))
def test_switch_basic(line, dioport):
    sw = PowerSwitch(line, dioport)
    sw.off()
    assert dioport.test(line) is False
    sw.on()
    assert dioport.test(line) is True
    sw.off()
    assert dioport.test(line) is False


def test_switch_ref(dioport, caplog):
    caplog.set_level(logging.INFO)
    a = PowerSwitch(1, dioport)
    b = PowerSwitch(1, dioport)
    a.off()
    assert dioport.test(1) is False
    a.on()
    assert dioport.test(1) is True
    assert 'Power on sw-1' in caplog.text
    b.on()
    a.off()
    b.off()
    assert 'Power off sw-1' in caplog.text
