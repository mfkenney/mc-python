# -*- coding: utf-8 -*-
import pytest
from pymc.cli.genconfig import main
from pymc.dio import DioPort, MemoryMap


# Sample sensor configuration
CONFIG = """
---
name: ctd/1
description: Seabird SBE 37-SMP CTD
interface:
  module: sbe
  device: /dev/ttyUSB2
  baud: 9600
  gpio: 3
  warmup: 500
  timeout: 5000
  eor: "S>"
variables: [temperature, conductivity, pressure]
units: {temperature: degC, conductivity: S/m, pressure: dbars}
precision: {temperature: "0.0001", conductivity: "0.00001", pressure: "0.002"}
init:
  - outputformat=1
sample:
  command: sl
  post: qs
result:
  line: 1
  fields: [0, 3]
manufacturer: Seabird
"""


@pytest.fixture
def genconfig(request, tmpdir):
    src = tmpdir.join('config.yaml')
    cfg = getattr(request.module, 'sensconfig', CONFIG)
    src.write(cfg)
    dest = tmpdir.join('config.bin')
    status = main([str(src), str(dest)])
    return status, str(dest)


@pytest.fixture
def dioport():
    dp = DioPort(0, 0, mm=MemoryMap())
    dp.outputs = 0xff
    return dp
