#
# Test the Gill MetPack
#
import pytest
import logging
import yaml
from decimal import Decimal
from io import BytesIO
from pymc.sensors.device import sensor_config


GILLINPUT = b"""kjshlhdlk
\x02Q,1018.9,+020.1,042.5,+007.0,+00.056,+0005.0,+12.9,00,\x0372\r
\x02Q,1019.0,+020.2,042.4,+007.0,+00.056,+0007.5,+12.9,00,\x037F\r
\x02Q,1019.0,+020.2,042.4,+007.0,+00.056,+0053.1,+12.9,00,\x037A\r
\x02Q,1019.0,+020.2,042.5,+007.1,+00.056,+0055.0,+12.9,00,\x037D\r
"""

GILLCONFIG = r"""
---
name: met/1
description: Gill MetPack with Rain Gauge and Pyranometer
variables: [pressure, temp, rh, dwpt, rain, swrad, vbatt]
units: {pressure: "mbar", temp: "degC", rh: "%", dwpt: "degC", rain: "mm", swrad: "w/m^2", vbatt: "volts"}
precision: {pressure: "0.1", temp: "0.1", rh: "0.1", dwpt: "0.1", rain: "0.001", swrad: "0.1", vbatt: "0.1"}
interface:
  module: metpack
  device: /dev/ttyUSB3
  baud: 19200
  gpio: "8160_LCD_D4"
  eor: "\r\n"
"""


@pytest.fixture(autouse=True)
def patch_serial_read(monkeypatch):
    buf = BytesIO(GILLINPUT)
    monkeypatch.setattr('pymc.sensors.device.SerialPort.read', buf.read)


@pytest.fixture
def sensor():
    doc = yaml.load(GILLCONFIG)
    # Not connecting to real hardware ...
    del doc['interface']['device']
    del doc['interface']['gpio']
    config, klass = sensor_config(doc)
    assert klass is not None
    return klass(config)


def test_sample(caplog, sensor):
    caplog.set_level(logging.INFO)
    logging.info('EOR = %r', sensor.eor)
    with sensor:
        sensor.pre_sample()
        for i in range(4):
            ts, result = sensor.sample()
            assert len(result) == 7
            if i == 1:
                assert result[2] == Decimal('42.4')
