#
# Test parsing the SeaFET data records
#
import pytest
import logging
import yaml
from decimal import Decimal
from io import BytesIO
from pymc.sensors.device import sensor_config
from pymc.cli.gps_convert import convert_gps


GPSINPUT = b"""\x00kjdhakjh
$GPGGA,194252,4739.3072,N,12218.9528,W,1,04,5.2,,M,-18.4,M,,\r
$GPRMC,194253,A,4739.3051,N,12218.9538,W,000.0,000.0,291012,018.2,E,A*02\r
$GPGGA,194253,4739.3051,N,12218.9538,W,1,04,5.2,,M,-18.4,M,,*54\r
"""

GPSCONFIG = r"""
---
name: gps/1
description: Garmin 15H GPS NMEA output
interface:
  module: gps
  device: /dev/ttyAT6
  gpio: "8160_LCD_D2"
  baud: 4800
  eor: "\r\n"
"""


@pytest.fixture(autouse=True)
def patch_serial_read(monkeypatch):
    buf = BytesIO(GPSINPUT)
    monkeypatch.setattr('pymc.sensors.device.SerialPort.read', buf.read)


@pytest.fixture
def sensor():
    doc = yaml.load(GPSCONFIG)
    # Not connecting to real hardware ...
    del doc['interface']['device']
    if 'gpio' in doc['interface']:
        del doc['interface']['gpio']
    config, klass = sensor_config(doc)
    assert klass is not None
    return klass(config)


def test_sample(caplog, sensor):
    caplog.set_level(logging.INFO)
    with sensor:
        sensor.wait_for_ready()
        sensor.pre_sample()
        ts, result = sensor.sample()
    values = convert_gps(result)
    assert values[0] == 1351539773
    assert values[1] == Decimal('47.655085')
    assert values[2] == Decimal('-122.315897')
