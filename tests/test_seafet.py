#
# Test parsing the SeaFET data records
#
import pytest
import logging
import yaml
from decimal import Decimal
from io import BytesIO
from pymc.sensors.device import sensor_config


SFINPUT = b"""kjshlhdlk
SATPHB0211,2015112,23.1526947,6.66657, 6.11143,23.5286,nan,nan,nan,nan,0x0000,161\r
"""

SFCONFIG = r"""
---
name: seafet/1
description: Satlantic SEAFet pH sensor
variables: [internal_pH, external_pH, temperature]
units: {temperature: degC, internal_pH: pH, external_pH: pH}
precision: {temperature: '0.0001', internal_pH: '0.00001', external_pH: '0.00001'}
interface:
  module: seafet
  device: /dev/ttyUSB6
  baud: 19200
  gpio: "8160_LCD_D7"
  eor: "\r\n"
result:
  fields: [3, 6]
"""

# For logging driver output
OUTPUT = BytesIO()


@pytest.fixture(autouse=True)
def patch_serial_read(monkeypatch):
    buf = BytesIO(SFINPUT)
    monkeypatch.setattr('pymc.sensors.device.SerialPort.read', buf.read)
    monkeypatch.setattr('pymc.sensors.device.SerialPort.write', OUTPUT.write)


@pytest.fixture
def sensor():
    doc = yaml.load(SFCONFIG)
    # Not connecting to real hardware ...
    del doc['interface']['device']
    del doc['interface']['gpio']
    config, klass = sensor_config(doc)
    assert klass is not None
    return klass(config)


def test_sample(caplog, sensor):
    caplog.set_level(logging.INFO)
    with sensor:
        sensor.wait_for_ready()
        sensor.pre_sample()
        ts, result = sensor.sample()
    assert len(result) == 3
    assert result[1] == Decimal('6.11143')
    out = OUTPUT.getvalue()
    # The driver should send a <CR> to wake the sensor
    # and an 's' to read a sample.
    assert out == b'\rs'
