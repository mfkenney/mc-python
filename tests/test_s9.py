#
# Test the Soundnine Compass driver.
#
import pytest
import logging
import yaml
from io import BytesIO
from mock import Mock
from pymc.sensors.device import sensor_config


S9INPUT = b"""
kjhgdk
<creport v='1' t='89'>
<S9CD v='1'>140.04,71.75,313,-1.46,88.65,88.63,18.22,4,4</S9CD>
<S9CRD v='1'>-67.25, 299.75, 70.00, 25.50, 991.75, 23.50, 28.50,  4</S9CRD>
<PORT1>
       $00.11,-00.09,-00.06,33.83,00.11,24.90,1,1,0,0,12.2
</PORT1>
</creport>
tgedd
"""

S9CONFIG = """
---
name: s9/1
description: Soundnine logger
interface:
  module: s9
  baud: 19200
---
name: ica/1
description: Soundnine Compass and Accelerometer
variables: [psi, inc, mag, theta, phi, tilt, temp, rate, ns]
units: {psi: 'deg', inc: 'deg', mag: 'gauss', theta: 'deg', phi: 'deg',
        tilt: 'deg', temp: 'degC', rate: 'Hz', ns: ''}
precision: {psi: '0.01', inc: '0.01', mag: '1', theta: '0.01', phi:
  '0.01', tilt: '0.01', temp: '0.01', rate: '1', ns: '1'}
interface:
  module: s9ica
  proxy: s9/1
---
name: icaraw/1
description: Soundnine Compass and Accelerometer raw data
variables: [mx, my, mz, ax, zy, az, temp, ns]
units: {mx: 'gauss', my: 'gauss', mz: 'gauss', ax: 'g', ay: 'g', az: 'g',
temp: '', ns: ''}
precision: {mx: '0.01', my: '0.01', mz: '1', ax: '0.01', ay: '0.01', az: '0.01', temp: '0.01', ns: '1'}
interface:
  module: s9icaraw
  proxy: s9/1
---
name: wind/1
description: Vaisala wind sensor
variables: [ws, wx, wy, wd, gu, temp, va, fb, fs, fv, vi]
units: {ws: "m/s", wx: "m/s", wy: "m/s", wd: "degM", gu: "m/s", temp:
  "degC", va: "", fb: "", fs: "", fv: "", vi: "volts"}
precision: {ws: "0.01", wx: "0.01", wy: "0.01", wd: "0.01", gu: "0.01", temp:
  "0.01", va: "1", fb: "1", fs: "1", fv: "1", vi: "0.1"}
interface:
  module: s9wind
  proxy: s9/1
"""


@pytest.fixture(autouse=True)
def patch_serial_read(monkeypatch):
    buf = BytesIO(S9INPUT)
    monkeypatch.setattr('pymc.sensors.device.SerialPort.read', buf.read)
    monkeypatch.setattr('pymc.sensors.device.SerialPort.purge', Mock())


@pytest.fixture
def sensors():
    devices = {}
    for doc in yaml.load_all(S9CONFIG):
        config, klass = sensor_config(doc)
        assert klass is not None
        devices[config['name']] = klass(config)
    return devices


def test_config(caplog, sensors):
    assert 's9master/1' in sensors
    proxy = sensors['s9master/1']
    for name in ('ica/1', 'icaraw/1', 'wind/1'):
        assert name in sensors
        assert sensors[name].proxy is proxy


def test_sample(caplog, sensors):
    caplog.set_level(logging.INFO)
    assert 's9master/1' in sensors
    dev = sensors['s9master/1']
    with dev:
        dev.pre_sample()
        ts, result = dev.sample()
    assert len(result) == 3
    assert result[0][0] == 'ica/1'
    assert len(result[0][2]) == 9
    assert result[1][0] == 'icaraw/1'
    assert len(result[1][2]) == 8
    assert result[2][0] == 'wind/1'
    assert len(result[2][2]) == 11
