# -*- coding: utf-8 -*-
import os


# Sample invalid sensor configuration
sensconfig = """
---
name: ctd/1
description: Seabird SBE 37-SMP CTD
interface:
  module: unknown
  device: /dev/ttyS0
  baud: 9600
  gpio: "3"
  warmup: 500
  timeout: 5000
  eor: "S>"
variables: [temperature, conductivity, pressure]
units: {temperature: degC, conductivity: S/m, pressure: dbars}
precision: {temperature: "0.0001", conductivity: "0.00001", pressure: "0.002"}
init:
  - outputformat=1
sample:
  command: sl
  post: qs
result:
  line: 1
  fields: [0, 3]
manufacturer: Seabird
"""


def test_config(genconfig, capsys):
    status, fn = genconfig
    # Invalid module class
    assert status == 2
    assert not os.path.isfile(fn)
