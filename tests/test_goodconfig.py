# -*- coding: utf-8 -*-
import cPickle as pickle
import os
from pymc.sensors.device import Master


# Sample sensor configuration
sensconfig = r"""
---
name: ctd/1
description: Seabird SBE 37-SMP CTD
interface:
  module: sbe
  device: /dev/ttyUSB2
  baud: 9600
  gpio: "8160_LCD_D3"
  warmup: 500
  timeout: 5000
  eor: "S>"
variables: [temperature, conductivity, pressure]
units: {temperature: degC, conductivity: S/m, pressure: dbars}
precision: {temperature: "0.0001", conductivity: "0.00001", pressure: "0.002"}
init:
  - outputformat=1
sample:
  command: sl
  post: qs
result:
  line: 1
  fields: [0, 3]
manufacturer: Seabird
---
name: sim/1
description: Seabird Surface Inductive Modem
interface:
  module: sim
  device: /dev/ttyUSB0
  baud: 19200
  warmup: 2000
  timeout: 5000
  eor: "S>"
---
name: imctd/1
description: Seabird SBE 37-IMP MicroCAT CTD @ 10m
interface:
  module: sbeuim
  uim_addr: 95
  proxy: sim/1
  warmup: 1000
variables: [temperature, conductivity, pressure]
units: {temperature: degC, conductivity: S/m, pressure: dbars}
precision: {temperature: '0.0001', conductivity: '0.00001', pressure: '0.002'}
sample:
  command: sl
result:
  line: 1
  fields: [1, 4]
"""


def test_config(genconfig):
    status, fn = genconfig
    assert status == 0
    assert os.path.isfile(fn)


def test_loadconfig(genconfig):
    status, fn = genconfig
    with open(fn, 'rb') as f:
        sens = pickle.load(f)
    assert len(sens) == 3
    klass, cfg = sens[0]
    assert cfg['name'] == 'ctd/1'
    assert str(klass) == "<class 'pymc.sensors.device.Sbe'>"
    klass, cfg = sens[1]
    assert cfg['name'] == 'sim/1'
    assert isinstance(klass(cfg), Master)
