#
import pytest
import logging
import xml.etree.ElementTree as ET
from StringIO import StringIO
from pymc.sensors.ports import SerialPort


XMLINPUT = b"""
blahblahblah
</creport>
<creport v='1' t='827'>
<S9CD v='1'>249.56,65.27,317,-0.45,89.82,89.82,18.09,4,40</S9CD>
<S9CRD v='1'>-43.38, 290.50, -123.43, 7.90, 988.88, 3.03, 12.98,  40</S9CRD>
<PORT1>
       $999.00,00.00,00.00,999.00,00.00,00.00,00.00,99.90,0,0,12.5
</PORT1>
</creport>
kjhgdktgedd
"""


@pytest.fixture(autouse=True)
def patch_serial_read(monkeypatch):
    src = StringIO(XMLINPUT)
    monkeypatch.setattr(__name__ + '.SerialPort.read', src.read)


def test_xml_read(caplog):
    caplog.set_level(logging.INFO)
    sp = SerialPort()
    prefix = b'<creport'
    suffix = b'</creport>'
    rec = sp.read_record(prefix, suffix)
    assert rec is not None
    doc = ET.fromstring(prefix + rec + suffix)
    assert doc.find('S9CD') is not None
