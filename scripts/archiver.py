#!/usr/bin/env python
"""
Manage the sensor data archive.
"""
import sys
import os
import hashlib
import redis
import simplejson as json
import time
import logging
import argparse
import signal
import msgpack
from decimal import Decimal


def metadata_uid(m):
    h = hashlib.sha1()
    h.update(m)
    return h.digest()


def signal_handler(signum, frame):
    sys.exit(0)


def mp_decoder(obj):
    if b'__decimal__' in obj:
        obj = Decimal(obj['s'])
    return obj


class Archive(object):
    """
    Class to manage a JSON data archive file.

    :ivar ardir: archive directory path
    :ivar t_rollover: file roll-over interval in seconds
    :ivar ftmpl: filename template in :func:`time.strftime` format
    """

    def __init__(self, ardir, ftmpl, t_rollover=86400, logger=None):
        """
        Instance initializer.

        :param ardir:
        :param ftmpl:
        :param t_rollover:
        :return:
        """
        self.ardir = ardir
        self.ftmpl = ftmpl
        self.t_rollover = t_rollover
        self.timestamp = 0, 0
        self._file = None
        if not os.path.isdir(self.ardir):
            os.makedirs(self.ardir)
        self.logger = logger or logging.getLogger()
        self.mdcache = {}

    def _openfile(self):
        """
        Open the current archive file.

        :return: file object
        """
        t = divmod(int(time.time()), self.t_rollover)
        if (self._file is None) or (t[0] > self.timestamp[0]):
            fname = time.strftime(self.ftmpl, time.gmtime())
            if self._file:
                self._file.close()
            pathname = os.path.join(self.ardir, fname)
            # If file exists, open in append mode. Otherwise open for writing
            # and add the header.
            if os.path.isfile(pathname):
                self._file = open(pathname, 'a')
            else:
                self._file = open(pathname, 'w')
                for uid, m in self.mdcache.values():
                    self._file.write(json.dumps(m) + '\n')
            self.logger.info('Opened archive file %s', pathname)
            self.timestamp = t
        return self._file

    def _closefile(self):
        if self._file:
            self._file.close()
            self._file = None

    def __enter__(self):
        return self

    def __exit(self, *args):
        self._closefile()
        return False

    def add_record(self, name, secs, usecs, data):
        d = {'sensor': name, 'clock': [secs, usecs], 'data': data}
        ofp = self._openfile()
        ofp.write(json.dumps(d) + '\n')
        ofp.flush()

    def add_metadata(self, name, md):
        uid = metadata_uid(str(md))
        try:
            # Don't write the metadata unless it has changed
            old_uid, _ = self.mdcache[name]
            if old_uid == uid:
                return
        except KeyError:
            pass
        self.mdcache[name] = uid, md
        ofp = self._openfile()
        ofp.write(json.dumps(md) + '\n')
        ofp.flush()


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('dir', help='archive directory')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    parser.add_argument('--host',
                        default='localhost',
                        help='Redis host (%(default)s)')
    parser.add_argument('--port',
                        type=int,
                        default=6379,
                        help='Redis TCP port (%(default)d)')
    parser.add_argument('--db',
                        type=int,
                        default=0,
                        help='Redis DB number (%(default)d)')
    parser.add_argument('--tmpl', '-t',
                        default='data-%Y%m%d.json',
                        help='file-name template (%(default)s)')

    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logger = logging.getLogger('archiver')
    logger.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(
        logging.Formatter('%(levelname)-8s %(message)s'))
    logger.addHandler(ch)

    ar = Archive(args.dir, args.tmpl)
    rd = redis.StrictRedis(host=args.host,
                           port=args.port,
                           db=args.db)
    pubsub = rd.pubsub()
    logger.info('Connected to Redis')

    signal.signal(signal.SIGTERM, signal_handler)
    logger.info('Listening for messages')
    pubsub.psubscribe('data.*', 'metadata.*')

    try:
        for msg in pubsub.listen():
            if msg['type'] == 'pmessage':
                logger.debug('< %r', msg['data'])
                mtype, name = msg['channel'].split('.', 1)
                if mtype == 'data':
                    t_sec, t_usec, data = msgpack.unpackb(
                        msg['data'], use_list=False, object_hook=mp_decoder)
                    ar.add_record(name, t_sec, t_usec, data)
                else:
                    md = json.loads(msg['data'])
                    ar.add_metadata(name, md)

    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    finally:
        pubsub.punsubscribe('data.*', 'metadata.*')
        rd.close()


if __name__ == '__main__':
    main()
