#!/usr/bin/env python
#
#
from pymc.seascan import Clock, Timestamp, set_sys_time
import time
import sys
from optparse import OptionParser

def main():
    """
    %prog [options] clkID

    Query and set a Seascan real-time clock.
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(set=False, write=False, offset=False,
                        device='/dev/ttyS2',
                        epoch_file='/etc/seascan_epoch',
                        wakeup_line=76)
    parser.add_option('-s', '--sstosys',
                      action='store_true',
                      dest='set',
                      help='set the system time from the Seascan clock')
    parser.add_option('-w', '--systoss',
                      action='store_true',
                      dest='write',
                      help='write the current system time to the Seascan clock')
    parser.add_option('-o', '--offset',
                      action='store_true',
                      dest='offset',
                      help='print system time offset to stdout')
    parser.add_option('-d', '--device',
                      dest='device',
                      type="string",
                      help='serial device [default %default]',
                      metavar='DEV')
    parser.add_option('-f', '--epoch-file',
                      dest='epoch_file',
                      type="string",
                      help='clock epoch file [default %default]',
                      metavar='FILE')
    parser.add_option('-l', '--wakeup-line',
                      dest='wakeup_line',
                      type="int",
                      help='GPIO wakeup line [default %default]',
                      metavar='GPIO')

    options, args = parser.parse_args()
    if len(args) < 1:
        parser.error('Missing clock ID number')
        
    clk = Clock(options.device, int(args[0]), options.wakeup_line)
    clk.epoch_file = options.epoch_file
    clk.wakeup(count=4)
    if not clk.activate():
        sys.stderr.write('Cannot address clock\n')
        return 1

    if not options.write:
        ss_time, t = clk.get_time()
        ss_t = ss_time.tounixtime()
        if options.offset:
            dt = t - ss_t
            print ss_t, dt
        elif options.set:
            set_sys_time(ss_time)
        else:
            print time.asctime(time.gmtime(ss_t))
    else:
        if clk.unlock():
            clk.set_time()
        else:
            sys.stderr.write('Cannot unlock clock\n')
            return 1
        
    clk.deactivate()
    return 0
    

if __name__ == '__main__':
    sys.exit(main())
