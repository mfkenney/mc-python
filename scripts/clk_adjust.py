#!/usr/bin/env python
#
# Analyze a file of system-clock offset values written by ssclock.py and
# call adjtimex with the appropriate values to correct the clock frequency
# error.
#
import sys
from subprocess import Popen, PIPE, call
from optparse import OptionParser

def msum(iterable):
    "Full precision summation using multiple floats for intermediate values"
    # Rounded x+y stored in hi with the round-off stored in lo.  Together
    # hi+lo are exactly equal to x+y.  The inner loop applies hi/lo summation
    # to each partial so that the list of partial sums remains exact.
    # Depends on IEEE-754 arithmetic guarantees.  See proof of correctness at:
    # www-2.cs.cmu.edu/afs/cs/project/quake/public/papers/robust-arithmetic.ps

    partials = []               # sorted, non-overlapping partial sums
    for x in iterable:
        i = 0
        for y in partials:
            if abs(x) < abs(y):
                x, y = y, x
            hi = x + y
            lo = y - (hi - x)
            if lo:
                partials[i] = lo
                i += 1
            x = hi
        partials[i:] = [x]
    return sum(partials, 0.0)


def read_data(filename):
    infile = open(filename, 'r')
    x = []
    y = []
    for line in infile:
        if line.startswith('Cannot'):
            continue
        f = [float(e) for e in line.strip().split()]
        x.append(f[-2])
        y.append(f[-1])
    infile.close()
    return x, y

def stats(x, y):
    n = len(x)
    sum_x = msum(x)
    sum_y = msum(y)
    sum_xx = msum([e*e for e in x])
    sum_yy = msum([e*e for e in y])
    sum_xy = msum([a*b for a, b in zip(x, y)])
    avg_x = sum_x/n
    avg_y = sum_y/n
    # Variance of X
    ss_x = sum_xx - 2*avg_x*sum_x + n*avg_x*avg_x
    # Variance of Y
    ss_y = sum_yy - 2*avg_y*sum_y + n*avg_y*avg_y
    # Covariance
    ss_xy = sum_xy - avg_y*sum_x - avg_x*sum_y + n*avg_x*avg_y
    return ([avg_x, avg_y], [ss_x, ss_y, ss_xy])

def lfit(x, y):
    """
    Perform a least squares fit to determine the coefficients for the
    equation:

      y = c[0] + c[1]*x

    @param x: list of X values
    @param y: list of Y values
    @returns: tuple of R^2, c
    """
    avgs, variance = stats(x, y)
    c = [0, 0]
    c[1] = variance[2]/variance[0]
    c[0] = avgs[1] - c[1]*avgs[0]
    residual = [c[0]+c[1]*e-avgs[1] for e in x]
    ss_e = msum([e*e for e in residual])
    return ss_e/variance[1], c

def read_params():
    """
    Read the current tick and frequency settings
    """
    output = Popen(['adjtimex'], stdout=PIPE).communicate()[0]
    params = []
    for line in output.split('\n'):
        if not line:
            continue
        name, value = line.split(':')
        if name.startswith('-'):
            name = name.split()[1]
        value = int(value.split()[0])
        params.append((name, value))
    return params

def set_params(ticks, frequency):
    return call(['adjtimex', '-t', str(ticks), '-f', str(frequency)])

def main():
    """
    %prog [options] file

    Read a file of time offsets created by ssclock.py and use a least-squares fit
    to calculate the appropriate adjtimex parameters to reduce the system-clock
    frequency error.
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(minrsq=0.8, maxdrift=1.0e-7, dryrun=False)
    parser.add_option('-r', '--min-rsq',
                      dest='minrsq',
                      type="float",
                      help='minimum R^2 allowed for fit [default %default]',
                      metavar='RSQ')
    parser.add_option('-d', '--max-drift',
                      dest='maxdrift',
                      type="float",
                      help='maximum allowed clock drift [default %default]',
                      metavar='DRIFT')
    parser.add_option('-n', '--dry-run',
                      action='store_true',
                      dest='dryrun',
                      help='print the adjtimex command but do not execute it')
    options, args = parser.parse_args()
    if len(args) < 1:
        parser.error('Missing file name')

    x, y = read_data(args[0])
    r_sq, c = lfit(x, y)
    print r_sq, c

    if r_sq >= options.minrsq and c[1] >= options.maxdrift:
        # c[1] is the frequency error in seconds/second. Convert to
        # parts-per-million
        ppm = 1e6 * c[1]

        # Calculate the adjtimex parameters
        ticks, freq = divmod(-ppm, 100)
        params = dict(read_params())
        print 'adjtimex -t %d -f %d' % (params['tick']+ticks, params['frequency']+int(freq*65536))
        if not options.dryrun:
            set_params(params['tick']+ticks, params['frequency']+int(freq*65536))
        return 0
    else:
        return 1
    
if __name__ == '__main__':
    sys.exit(main())
