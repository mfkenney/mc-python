#!/usr/bin/env python
#
# Listen for various data archive "events" in the form of D-bus signals and
# execute software in response.
#
import dbus
import dbus.service
import gobject
import dbus.mainloop.glib
from optparse import OptionParser
import sys
import os
import subprocess

def daemon_mode(dir, pidfile=None):
    """Move process into the background and disconnect from the
    controlling terminal.
    """
    pid = os.fork()
    if pid < 0:
        print >> sys.stderr, "Cannot fork a background process!"
        return
    if pid > 0:
        os._exit(0)
    os.setsid()
    os.chdir('/')
    fd = os.open('/dev/null', os.O_RDWR)
    if fd != -1:
        os.dup2(fd, 0)
        os.dup2(fd, 1)
        if fd > 2:
            os.close(fd)
    # Redirect stderr to a file so we can catch Python stack backtraces
    # if an exception occurs.
    fd = os.open(dir + '/archive_event_errors', os.O_WRONLY|os.O_CREAT)
    if fd != -1:
        os.dup2(fd, 2)
        if fd > 2:
            os.close(fd)
    if pidfile:
        f = open(pidfile, 'w')
        f.write(str(os.getpid()))
        f.close()

def create_handler_for_dir(progdir):
    def _handler(*args):
        cmd = ['run-parts']
        for a in args:
            cmd.append('-a')
            cmd.append(str(a))
        cmd.append(progdir)
        subprocess.call(cmd)
    return _handler

def main():
    """
    %prog [options] signal progdir

    Listen for the named D-bus signal from the data archiver and when it
    occurs, use 'run-parts' to run all executable files in PROGDIR.
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(daemon=True, interface='hmc.IArchive', pidfile=None)
    parser.add_option('-n', '--no-daemon',
                      action='store_false',
                      dest='daemon',
                      help='stay in the foreground')
    parser.add_option("-i", "--interface",
                      type="string",
                      dest="interface",
                      metavar="NAME",
                      help="D-bus interface name for signals (default %default)")
    parser.add_option('-p', '--pidfile',
                      dest='pidfile',
                      type="string",
                      help='write process-id to FILE',
                      metavar='FILE')
    opts, args = parser.parse_args()

    try:
        signame = args[0]
        progdir = args[1]
    except IndexError:
        parser.error('Missing arguments')
        
    if opts.daemon:
        daemon_mode('/tmp', pidfile=opts.pidfile)

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SystemBus()
    handler = create_handler_for_dir(progdir)
    bus.add_signal_receiver(handler, dbus_interface=opts.interface,
                            signal_name=signame)
    
    loop = gobject.MainLoop()
    try:
        loop.run()
    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == '__main__':
    main()
