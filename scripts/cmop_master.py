#!/usr/bin/env python
#
# Handle the "master" side of the CMOP file transfer. This script is run on
# the Bridge Node.
#
from __future__ import with_statement
from contextlib import contextmanager
from pymc.umodem.device import Umodem, TimeoutError
from pymc.umodem.protocol import *
from ConfigParser import RawConfigParser
from optparse import OptionParser
import logging
import logging.handlers
import os
import sys
import time

def daemon_mode(dir, pidfile=None):
    """Move process into the background and disconnect from the
    controlling terminal.
    """
    pid = os.fork()
    if pid < 0:
        print >> sys.stderr, "Cannot fork a background process!"
        return
    if pid > 0:
        os._exit(0)
    os.setsid()
    os.chdir('/')
    fd = os.open('/dev/null', os.O_RDWR)
    if fd != -1:
        os.dup2(fd, 0)
        os.dup2(fd, 1)
        os.dup2(fd, 2)
        if fd > 2:
            os.close(fd)
    if pidfile:
        f = open(pidfile, 'w')
        f.write(str(os.getpid()))
        f.close()

@contextmanager
def pushd(dir):
    cwd = os.getcwd()
    os.chdir(dir)
    try:
        yield
    finally:
        os.chdir(cwd)
        
def oldest_file(dir):
    """
    Return the name of the oldest file from the outbox
    directory or None if the directory is empty.
    """
    try:
        with pushd(dir):
            listing = [(os.stat(f).st_mtime, f) for f in os.listdir('.')]
    except OSError:
        return None
    listing.sort()
    try:
        return os.path.join(dir, listing[0][1])
    except IndexError:
        return None

def set_nvram(um, config):
    """
    Apply all of the modem NVRAM settings from the config dictionary.
    """
    keys = [k for k in config.keys() if k.startswith('nvram/')]
    for k in keys:
        prefix, name = k.split('/')
        um[name] = config[k]

def fetch_next_file(um, source, config):
    logger = config['logger']
    ptype = int(config['modem/ptype'])
    localname = '.incoming.tmp'
    name = ''
    timeout = int(um['PTO']) + 2
    with pushd(config['paths/inbox']):
        try:
            name, size = active_rx_file(um, source,
                                        localname=localname,
                                        ptype=ptype,
                                        timeout=timeout)
            logger.info('New file %s (%d bytes)' % (name, size))
            os.rename(localname, name)
            tx_signal(um, source, signals.XFER_ACK)
        except (TimeoutError, TypeError):
            logger.warning('File download failed (%s)' % name)
            tx_signal(um, source, signals.XFER_NAK)


def send_file(um, dest, filename, config):
    logger = config['logger']
    ptype = int(config['modem/ptype'])
    try:
        active_tx_file(um, dest, filename, ptype=0)
        who, what = rx_signal(um, 30)
        if what == signals.XFER_ACK:
            os.unlink(filename)
        else:
            logger.warning('No ACK on file transfer (%s)' % filename)
    except TimeoutError:
        logger.warning('File upload failed (%s)' % filename)

def wait_for_ready(um):
    who, what = rx_signal(um, 30)
    if what == signals.READY:
        return int(who)
    else:
        return None

def run(config):
    um = Umodem(config['modem/device'],
                uid=int(config['modem/uid']),
                baud=int(config['modem/baud']))
    set_nvram(um, config)
    try:
        while True:
            who = wait_for_ready(um)
            if who is not None:
                time.sleep(3)
                fetch_next_file(um, who, config)
                outfile = oldest_file(config['paths/outbox'])
                if outfile:
                    send_file(um, who, outfile, config)
    finally:
        um.close()

def main():
    """
    %prog [options] cfgfile

    Run as a daemon process and transfer files over the Micro Modem from
    one or more slave systems.
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(daemon=True, pidfile=None)
    parser.add_option('-n', '--no-daemon',
                      action='store_false',
                      dest='daemon',
                      help='stay in the foreground')
    parser.add_option('-p', '--pidfile',
                      dest='pidfile',
                      type="string",
                      help='write process-id to FILE',
                      metavar='FILE')
    
    options, args = parser.parse_args()
    cfg = RawConfigParser()
    
    try:
        cfg.read(args[0])
    except IndexError:
        parser.error('Missing config file')
    except:
        parser.error('Error reading config file, %s' % args[0])
        
    config = {'modem/device' : '/dev/ttyS2',
              'modem/baud' : 19200,
              'modem/ptype' : 3,
              'log/file' : '/media/cf/logs/xfer.log',
              'log/days' : 7,
              'log/keep' : 4}
    for s in cfg.sections():
        config.update(dict([(s+'/'+k, v) for k, v in cfg.items(s)]))

    logger = logging.getLogger()
    logger.setLevel(eval('logging.'+config['log/level']))
    formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                                  '%Y-%m-%d %H:%M:%S')

    try:
        os.makedirs(os.path.dirname(config['log/file']))
    except OSError:
        pass
    
    fh = logging.handlers.TimedRotatingFileHandler(config['log/file'], 'D',
                                                   int(config['log/days']),
                                                   int(config['log/keep']))
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    if options.daemon:
        daemon_mode('/tmp', pidfile=options.pidfile)
    else:
        ch = logging.StreamHandler()
        ch.setFormatter(logging.Formatter('%(name)-12s %(levelname)-8s %(message)s'))
        logger.addHandler(ch)

    config['logger'] = logger
    run(config)

if __name__ == '__main__':
    main()
