#!/bin/sh
#
# Start/stop a sensor using the D-bus interface
#
# Usage: sensor.sh start|stop|list [name [interval]]
#
PATH=/bin:/sbin:/usr/bin:/usr/sbin
export PATH

set -e

if [ "x$DBUS_SESSION_BUS_ADDRESS" = "x" ]
then
    BUS="--system"
else
    BUS="--session"
fi

BUSNAME="hmc.Sensors"
ENABLE="hmc.ISensor.enableSensor"
DISABLE="hmc.ISensor.disableSensor"
START="hmc.ISensor.startSample"
SEND="hmc.ISensor.sendCommand"
GETMETADATA="hmc.ISensor.getMetadata"
LIST="hmc.ISensorBus.listSensors"
QUIT="hmc.ISensorBus.quit"
GETDATA="hmc.ISensorBus.triggerSample"
SAMPLE_READY="type='signal',interface='hmc.ISensor',member='sampleReady'"
SENSOR_READY="type='signal',interface='hmc.ISensor'"

s_enable()
{
    # Enable the sensor
    for arg
    do
	dbus-send $BUS --print-reply --dest=$BUSNAME --type=method_call "/Sensor/$arg" $ENABLE
    done

    # dbus-monitor $BUS "$SENSOR_READY" |\
    #   awk '/member=sensorReady/ {print "Sensor ready"; exit 0}
    #        /member=sensorTimeout/ {print "Timeout"; exit 1}
    #    '
}

s_send ()
{
    # Send a command
    path="/Sensor/$1"
    cmd="$2"
    timeout="${3-2.0}"
    dbus-send $BUS --print-reply --dest=$BUSNAME --type=method_call $path \
      $SEND string:"$cmd" double:$timeout
}

s_start ()
{
    # Start the sampling process
    for arg
    do
	dbus-send $BUS --print-reply --dest=$BUSNAME --type=method_call "/Sensor/$arg" $START
    done
}

s_disable ()
{
    # Disable the sensor
    for arg
    do
	dbus-send $BUS --print-reply --dest=$BUSNAME --type=method_call "/Sensor/$arg" $DISABLE
    done
}

s_getdata ()
{
    dbus-send $BUS --print-reply --dest=$BUSNAME --type=method_call '/Sensors' $GETDATA
}

s_quit ()
{
    dbus-send $BUS --print-reply --dest=$BUSNAME --type=method_call '/Sensors' $QUIT
}

s_list ()
{
    # List the available sensors
    paths=`dbus-send $BUS --print-reply --dest=$BUSNAME --type=method_call '/Sensors' $LIST | sed -n -e '/object path/s/^.*"\([^"]*\)"/\1/p'`
    for p in $paths
    do
        echo $p | sed -e 's!/Sensor/!!'
    done

    # If '-v' was specified, display the sensor metadata
    if [ "x$1" = "x-v" ]
    then
	# Create a sed script to extract the actual reply string from 
	# the dbus-send output.
	sedfile="/tmp/sed.$$"
	cat<<EOF > $sedfile
:join
/string "[^"]*$/ { N; s/[      *]\n[   *]/ /; b join; }
s/.*string "\([^"]*\)"/\1/p
EOF

	for p in $paths
	do
	    echo '---'
	    dbus-send $BUS --print-reply --dest=$BUSNAME --type=method_call $p $GETMETADATA |\
             sed -n -f $sedfile
	done
	rm -f $sedfile
    fi
}

op="$1"
shift

case "$op" in
    enable)
	s_enable $@
	;;
    send)
	s_send $@
	;;
    start)
	s_start $@
	;;
    getdata)
	s_getdata
	;;
    disable)
	s_disable $@
	;;
    list)
	s_list $@
	;;
    *)
	echo "Usage: $0 {enable|start|getdata|disable|list} name [args ...]" >&2
	exit 1
	;;
esac

exit 0
