#!/usr/bin/env python
#
# D-bus service to manage sensors.
#
import gobject
import dbus.mainloop.glib
import threading
import logging
import logging.handlers
import pickle
import os
import sys
from optparse import OptionParser
from pymc.sensors.bus import SensorBus

def daemon_mode(pidfile=None):
    """
    Move process into the background and disconnect from the
    controlling terminal.
    """
    pid = os.fork()
    if pid < 0:
        logging.critical("Cannot fork a background process!")
        return
    if pid > 0:
        os._exit(0)
    os.setsid()
    os.chdir('/')
    fd = os.open('/dev/null', os.O_RDWR)
    if fd != -1:
        os.dup2(fd, 0)
        os.dup2(fd, 1)
        os.dup2(fd, 2)
        if fd > 2:
            os.close(fd)
    if pidfile:
        f = open(pidfile, 'w')
        f.write(str(os.getpid()))
        f.close()

def main():
    """
    Usage: %prog [options] sensorsfile
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(daemon=True, pidfile=None, logdir=None,
                        verbose=False, session=False)
    parser.add_option('-n', '--no-daemon',
                      action='store_false',
                      dest='daemon',
                      help='stay in the foreground')
    parser.add_option('-v', '--verbose',
                      action='store_true',
                      dest='verbose',
                      help='log debugging information')
    parser.add_option('-s', '--session',
                      action='store_true',
                      dest='session',
                      help='use session-bus rather than system-bus')
    parser.add_option('-p', '--pidfile',
                      dest='pidfile',
                      type="string",
                      help='write process-id to FILE',
                      metavar='FILE')
    parser.add_option('-l', '--logdir',
                      dest='logdir',
                      type="string",
                      help='log-file directory',
                      metavar='DIR')

    opts, args = parser.parse_args()
    if len(args) == 0:
        parser.error('You must specify a sensors file')

    sensors = pickle.load(open(args[0], 'rb'))
    
    gobject.threads_init()
    dbus.mainloop.glib.threads_init()
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    if opts.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO
        
    logger = logging.getLogger()
    logger.setLevel(level)
    formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                                  '%Y/%m/%d %H:%M:%S')

    if opts.logdir:
        try:
            os.makedirs(opts.logdir)
        except OSError:
            pass
        logfile = os.path.join(opts.logdir, 'sensorbus.log')
        fh = logging.handlers.TimedRotatingFileHandler(logfile, 'D', 7, 4)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        
    if opts.daemon:
        daemon_mode(pidfile=opts.pidfile)
    else:
        ch = logging.StreamHandler()
        ch.setFormatter(logging.Formatter('%(name)-12s %(levelname)-8s %(message)s'))
        logger.addHandler(ch)

    if opts.session:
        bus_class = dbus.SessionBus
    else:
        bus_class = dbus.SystemBus
        
    mainloop = gobject.MainLoop()
    SensorBus(sensors, bus_class, mainloop.quit)

    logger.info('Starting main loop')
    mainloop.run()

if __name__ == '__main__':
    main()
