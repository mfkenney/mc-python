#!/usr/bin/env python
#
"""
%prog [options] configfile sensor [sensor ...]

Enable, sample, and disable the named sensors.
"""
import logging
import logging.handlers
import os
import pickle
import dbus
from optparse import OptionParser
from pymc.sensors.manage import SensorGroup


def do_sample(devices, archiver=None, N=1):
    group = SensorGroup(devices)
    result = group.enable()
    if archiver:
        for name, metadata in result:
            archiver.addMetadata(name, metadata)
    else:
        print result

    for i in range(N):
        result = group.sample()
        if archiver:
            for name, data in result:
                if data:
                    secs, usecs, fmt, value = data
                    archiver.addRecord(name, secs, usecs, fmt, value)
        else:
            print result
    group.disable()

def main():
    parser = OptionParser(__doc__)
    parser.set_defaults(verbose=False, archive=False, session=False,
                        nsamples=1, logdir=None)

    parser.add_option('-v', '--verbose',
                      dest='verbose',
                      action='store_true',
                      help='verbose output')
    parser.add_option('-a', '--archive',
                      dest='archive',
                      action='store_true',
                      help='store data in archive')
    parser.add_option('-s', '--session',
                      dest='session',
                      action='store_true',
                      help='use session D-bus to access archive')
    parser.add_option('-n', '--nsamples',
                      dest='nsamples',
                      type='int',
                      help='number of samples to read (default %default)')
    parser.add_option('-l', '--logdir',
                      dest='logdir',
                      type="string",
                      help='log-file directory',
                      metavar='DIR')

    opts, args = parser.parse_args()
    if len(args) < 2:
        parser.error('Missing arguments')

    sensors = pickle.load(open(args[0], 'rb'))

    if opts.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logger = logging.getLogger()
    logger.setLevel(level)
    formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                                  '%Y/%m/%d %H:%M:%S')

    if opts.logdir:
        try:
            os.makedirs(opts.logdir)
        except OSError:
            pass
        logfile = os.path.join(opts.logdir, 'sample.log')
        fh = logging.handlers.RotatingFileHandler(logfile, maxBytes=100000, backupCount=10)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
    else:
        ch = logging.StreamHandler()
        ch.setFormatter(logging.Formatter('%(name)-12s %(levelname)-8s %(message)s'))
        logger.addHandler(ch)

    table = {}
    slaves = {}
    for k, c in sensors:
        table[c['name']] = k, c
        proxy = c.get('interface/proxy', None)
        if proxy:
            slaves.setdefault(proxy, []).append(c['name'])
            
    devices = []
    for name in args[1:]:
        try:
            klass, config = table[name]
        except KeyError:
            pass
        else:
            logging.info('Found sensor configuration %s', name)
            devices.append(klass(config))
            for slave in slaves.get(config['name'], []):
                logging.info('Initializing dependent sensor %s', slave)
                s_klass, s_config = table[slave]
                s_klass(s_config)
                
    if not devices:
        logging.critical('No sensors found in config file')
        sys.exit(1)

    if opts.archive:
        if opts.session:
            bus = dbus.SessionBus()
        else:
            bus = dbus.SystemBus()
        try:
            archiver = dbus.Interface(bus.get_object('hmc.Archiver', '/'), 'hmc.IArchive')
        except dbus.exceptions.DBusException:
            logging.warning('Archiver is not running')
            archiver = None
    else:
        archiver = None
    do_sample(devices, archiver, N=opts.nsamples)

if __name__ == '__main__':
    main()
