#!/usr/bin/env python
#
# Manage the data archive for Mooring Controller sensors.
#
from pymc.archive import Archiver
from pymc.archive.bus import ArchiverProxy
from optparse import OptionParser
import sys
import os
import logging
import logging.handlers
import dbus
import dbus.mainloop.glib
import gobject

def daemon_mode(dir, pidfile=None):
    """Move process into the background and disconnect from the
    controlling terminal.
    """
    pid = os.fork()
    if pid < 0:
        print >> sys.stderr, "Cannot fork a background process!"
        return
    if pid > 0:
        os._exit(0)
    os.setsid()
    os.chdir('/')
    fd = os.open('/dev/null', os.O_RDWR)
    if fd != -1:
        os.dup2(fd, 0)
        os.dup2(fd, 1)
        if fd > 2:
            os.close(fd)
    # Redirect stderr to a file so we can catch Python stack backtraces
    # if an exception occurs.
    fd = os.open(dir + '/error_log', os.O_WRONLY|os.O_CREAT)
    if fd != -1:
        os.dup2(fd, 2)
        if fd > 2:
            os.close(fd)
    if pidfile:
        f = open(pidfile, 'w')
        f.write(str(os.getpid()))
        f.close()

def make_linker(outbox, logger):
    """
    Return a function which will link completed archive files into
    the destination directory.
    """
    def _handler(oldfile, newfile):
        if oldfile and os.path.isfile(oldfile):
            dest = os.path.join(outbox, os.path.basename(oldfile))
            try:
                if os.path.isfile(dest):
                    os.remove(dest)
                os.link(oldfile, dest)
                logger.info('Copied %s to OUTBOX', oldfile)
            except os.error, e:
                logger.critical('Error linking %s into outbox (%s)' % (oldfile, str(e)))
    return _handler

def main():
    """
    %prog [options] [FILESPEC]

    Archive sensor data to a series of JSON files which are rotated according to
    a user defined schedule. FILESPEC is a file-path template which uses a
    strftime(3) compatible format to specify a time dependent path name. The
    default FILESPEC is:

    /media/cf/data/data-%Y%m%d.json
    
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(daemon=True, rollover=86400, pidfile=None,
                        verbose=False, session=False, logdir=None,
                        outbox=None)
    parser.add_option('-n', '--no-daemon',
                      action='store_false',
                      dest='daemon',
                      help='stay in the foreground')
    parser.add_option('-t', '--t-rollover',
                      dest='rollover',
                      type="int",
                      help='archive rollover time in seconds [default %default]',
                      metavar='SECONDS')
    parser.add_option('-p', '--pidfile',
                      dest='pidfile',
                      type="string",
                      help='write process-id to FILE',
                      metavar='FILE')
    parser.add_option('-s', '--session',
                      action='store_true',
                      dest='session',
                      help='use session-bus rather than system-bus')
    parser.add_option('-v', '--verbose',
                      action='store_true',
                      dest='verbose',
                      help='log debugging information')
    parser.add_option('-l', '--logdir',
                      dest='logdir',
                      type="string",
                      help='log-file directory',
                      metavar='DIR')
    parser.add_option('-o', '--outbox',
                      dest='outbox',
                      type="string",
                      help='link archive file into DIR when full',
                      metavar='DIR')
    
    opts, args = parser.parse_args()

    try:
        template = args[0]
    except:
        template = '/media/cf/data/data-%Y%m%d.json'
        
    if opts.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO
        
    logger = logging.getLogger()
    logger.setLevel(level)
    formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                                  '%Y/%m/%d %H:%M:%S')

    if opts.logdir:
        try:
            os.makedirs(opts.logdir)
        except OSError:
            pass
        logfile = os.path.join(opts.logdir, 'archive.log')
        fh = logging.handlers.TimedRotatingFileHandler(logfile, 'D', 1, 7)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    if opts.daemon:
        daemon_mode('/tmp', pidfile=opts.pidfile)
    else:
        ch = logging.StreamHandler()
        ch.setFormatter(logging.Formatter('%(name)-12s %(levelname)-8s %(message)s'))
        logger.addHandler(ch)

    if opts.session:
        bus_class = dbus.SessionBus
    else:
        bus_class = dbus.SystemBus

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    gobject.threads_init()
    
    ar = Archiver(template, t_rollover=opts.rollover)
    if opts.outbox:
        try:
            os.makedirs(opts.outbox)
        except OSError:
            pass
        ev_handler = make_linker(opts.outbox, logger)
        ar.newfile_event += ev_handler
        
    mainloop = gobject.MainLoop()
    proxy = ArchiverProxy(ar, bus_class, mainloop.quit)

    logger.info('Starting main loop')
    mainloop.run()
    
if __name__ == '__main__':
    main()
