#!/usr/bin/env python
#
# Handle the "slave" side of the CMOP file transfer. This script is run on
# the Base Station.
#
from __future__ import with_statement
from contextlib import contextmanager
from pymc.umodem.device import Umodem, TimeoutError
from pymc.umodem.protocol import *
from ConfigParser import RawConfigParser
from optparse import OptionParser
import logging
import logging.handlers
import os
import sys

@contextmanager
def pushd(dir):
    cwd = os.getcwd()
    os.chdir(dir)
    try:
        yield
    finally:
        os.chdir(cwd)
        
def oldest_file(dir):
    """
    Return the name of the oldest file from the outbox
    directory or None if the directory is empty.
    """
    try:
        with pushd(dir):
            listing = [(os.stat(f).st_mtime, f) for f in os.listdir('.')]
    except OSError:
        return None
    listing.sort()
    try:
        return os.path.join(dir, listing[0][1])
    except IndexError:
        return None

def set_nvram(um, config):
    """
    Apply all of the modem NVRAM settings from the config dictionary.
    """
    keys = [k for k in config.keys() if k.startswith('nvram/')]
    for k in keys:
        prefix, name = k.split('/')
        um[name] = config[k]
        
def maybe_send_file(um, filename, config):
    """
    Send a READY signal and wait for a file-transfer request from
    the master unit.
    """
    logger = config['logger']
    if tx_signal(um, int(config['modem/master']), signals.READY):
        try:
            passive_tx_file(um, filename)
            um.ignore_errors = True
            who, what = rx_signal(um, 30)
            um.ignore_errors = False
            if what == signals.XFER_ACK:
                os.unlink(filename)
            else:
                logger.warning('No ACK on file transfer (%s)' % filename)
        except (TimeoutError, ModemError):
            logger.warning('File transfer failed (%s)' % filename)

def waitfor_download(um, config):
    """
    Wait for a file download from the master unit.
    """
    logger = config['logger']
    localname = '.incoming.tmp'
    name = ''
    with pushd(config['paths/inbox']):
        try:
            name, size = passive_rx_file(um, localname=localname)
            logger.info('New file %s (%d bytes)' % (name, size))
            os.rename(localname, name)
            tx_signal(um, int(config['modem/master']), signals.XFER_ACK)
        except (TimeoutError, ModemError):
            logger.warning('File download failed (%s)' % name)
            tx_signal(um, int(config['modem/master']), signals.XFER_NAK)

def run(config):
    um = Umodem(config['modem/device'],
                uid=int(config['modem/uid']),
                baud=int(config['modem/baud']))
    set_nvram(um, config)
    try:
        outfile = oldest_file(config['paths/outbox'])
        if outfile:
            maybe_send_file(um, outfile, config)
            waitfor_download(um, config)
    finally:
        um.close()

def main():
    """
    %prog [options] cfgfile

    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(verbose=False)
    parser.add_option('-v', '--verbose',
                      action='store_true',
                      dest='verbose',
                      help='output log data to stderr')
    
    options, args = parser.parse_args()
    cfg = RawConfigParser()
    try:
        cfg.read(args[0])
    except IndexError:
        parser.error('Missing config file')
    except:
        parser.error('Error reading config file, %s' % args[0])

    config = {'modem/device' : '/dev/ttyS1',
              'modem/baud' : 19200,
              'modem/ptype' : 3,
              'log/file' : '/media/cf/logs/xfer.log',
              'log/days' : 7,
              'log/keep' : 4}
    for s in cfg.sections():
        config.update(dict([(s+'/'+k, v) for k, v in cfg.items(s)]))

    logger = logging.getLogger()
    logger.setLevel(eval('logging.'+config['log/level']))
    formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                                  '%Y-%m-%d %H:%M:%S')

    try:
        os.makedirs(os.path.dirname(config['log/file']))
    except OSError:
        pass
    
    fh = logging.handlers.RotatingFileHandler(config['log/file'], maxBytes=100000, backupCount=4)    

    fh.setFormatter(formatter)
    logger.addHandler(fh)
    if options.verbose:
        ch = logging.StreamHandler()
        ch.setFormatter(logging.Formatter('%(name)-12s %(levelname)-8s %(message)s'))
        logger.addHandler(ch)

    config['logger'] = logger
    run(config)

if __name__ == '__main__':
    main()

