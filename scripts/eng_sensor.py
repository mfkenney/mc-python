#!/usr/bin/env python
#
# D-bus interface to Mooring Controller engineering sensors.
#
import dbus
import dbus.service
import gobject
from pymc.adc import Sensor, i2c_adc
from pymc.seascan import Clock
import dbus.mainloop.glib
import struct
import sys
from optparse import OptionParser
try:
    import json
except ImportError:
    import simplejson as json
    
def add_lists(x, y):
    return [e[0]+e[1] for e in zip(x, y)]

def filtered_sensor(sensor, count=10):
    while True:
        x = []
        for i, rec in enumerate(sensor):
            name, t, data = rec
            x.append(data)
            if i == (count - 1):
                break
        accum = reduce(add_lists, x)
        yield name, t, map(lambda a: a/count, accum)

class ClockSensor(object):
    def __init__(self, clock):
        self.clock = clock

    def __iter__(self):
        return self
    
    def next(self):
        t_sync = None
        self.clock.wakeup()
        if self.clock.activate():
            timestamp, t_sync = self.clock.get_time()
            if timestamp:
                return 'rtc/1', t_sync, [timestamp.tounixtime()]
        return 'rtc/1', t_sync, None

class SensorControl(dbus.service.Object):
    def __init__(self, bus, sensor, format, metadata):
        dbus.service.Object.__init__(self, bus, '/Sensor/%s' % metadata['name'])
        self.bus = bus
        self.sensor = sensor
        self.format = format
        self._metadata = metadata
        
    def send_metadata(self):
        self.metadata(json.dumps(self._metadata))

    def sample(self, show=False):
        name, t, data = self.sensor.next()
        if data is None:
            return 0, 0
        if show:
            print ','.join(['%.5g' % e for e in data])
        pdata = struct.pack(self.format, *data)
        secs = int(t)
        usecs = int((t - secs)*1e6)
        self.sampleReady(secs, usecs, self.format, pdata)
        return secs, usecs

    @dbus.service.signal(dbus_interface='hmc.ISensor',
                         signature='uusay')
    def sampleReady(self, secs, usecs, format, data):
        pass

    @dbus.service.signal(dbus_interface='hmc.ISensor',
                         signature='s')
    def metadata(self, m):
        pass

def run(controllers, loop, show):
    """
    Emit the 'metadata' and 'sampleReady' signals then exit.
    """
    for sc in controllers:
        sc.send_metadata()
        sc.sample(show)
    loop.quit()
    return False

def main():
    """
    %prog [options] CONFIGFILE

    Sample the engineering sensors (via A/D channels) and real-time clock and
    broadcast the values over the D-bus. CONFIGFILE is used to specify the A/D
    channels and calibration coefficients.
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(verbose=False, average=1)
    parser.add_option('-v', '--verbose',
                      action='store_true',
                      dest='verbose',
                      help='write values to standard output')
    parser.add_option("-a", "--average",
                      type="int",
                      dest="average",
                      metavar="N",
                      help="return the average of N samples")
    opts, args = parser.parse_args()
    
    try:
        cfg = json.load(open(args[0], 'r'))
    except IndexError:
        parser.error('No configuration file specified')
        
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SystemBus()
    name = dbus.service.BusName("hmc.EngSensors", bus)
    
    channels = []
    metadata = {'name' : 'eng/1',
                'description' : 'Engineering sensors',
                'variables' : [],
                'precision' : {},
                'units' : {}}
    for item in cfg['channels']:
        metadata['variables'].append(item['name'])
        metadata['units'][item['name']] = item['units']
        metadata['precision'][item['name']] = item.get('precision', '0.001')
        channels.append((int(item['channel']),
                         [float(e) for e in item['coefficients']],
                         item['units']))
    addr = cfg['i2c_addr']
    sensor = Sensor('eng/1',
                    i2c_adc(addr),
                    channels)
    if opts.average > 1:
        sensor = filtered_sensor(sensor, count=opts.average)

    controllers = []
    format = '<%df' % len(channels)
    controllers.append(SensorControl(bus, sensor, format, metadata))

    try:
        clk = Clock(cfg['seascan']['device'],
                    cfg['seascan']['id'],
                    cfg['seascan']['wakeup'])
        sc = SensorControl(bus,
                           ClockSensor(clk),
                           '<l',
                           {'name': 'rtc/1',
                            'description': 'Real-time clock',
                            'variables' : ['timestamp'],
                            'units' : {'timestamp': 'seconds since 1/1/1970 UTC'}})
        controllers.append(sc)
    except KeyError:
        pass
    
    loop = gobject.MainLoop()

    if opts.verbose:
        print ','.join(metadata['variables'])
        
    gobject.timeout_add(1500, run, controllers, loop, opts.verbose)
    
    try:
        loop.run()
    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == '__main__':
    main()
