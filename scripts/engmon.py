#!/usr/bin/env python
#
# A console program to monitor the engineering sensors.
#
import urwid.curses_display
import urwid
import yaml
from pymc.adc import Sensor, i2c_adc
from optparse import OptionParser
from decimal import Decimal

class SensorBox(urwid.WidgetWrap):
    def __init__(self, name, units, precision):
        self.prec = Decimal(str(precision))
        w_name = urwid.Text(name, align='left', wrap='clip')
        self.w_value = urwid.Text(' ', align='center', wrap='clip')
        w_units = urwid.Text(units, align='right', wrap='clip')
        display_widget = urwid.GridFlow([w_name, self.w_value, w_units],
                                        15, 3, 1, 'left')
        urwid.WidgetWrap.__init__(self, display_widget)
        
    def update(self, value):
        self.w_value.set_text(str(Decimal(str(value)).quantize(self.prec)))

class Application(object):
    def __init__(self, cfg, title):
        channels = []
        self.widgets = []
        for item in cfg['channels']:
            self.widgets.append(SensorBox(item['name'], item['units'],
                                          item.get('precision', '0.001')))
            channels.append((int(item['channel']),
                             [float(e) for e in item['coefficients']],
                             item['units']))
        addr = cfg['i2c_addr']
        self.sensor = Sensor('eng/1',
                             i2c_adc(addr),
                             channels)
        self.widget_list = urwid.SimpleListWalker(self.widgets)
        self.listbox = urwid.ListBox(self.widget_list)
        title = urwid.Text(title, align='center')
        header = urwid.AttrWrap(title, 'header')
        footer = urwid.AttrWrap(urwid.Text('type q to exit', align='left'), 'header')
        self.top = urwid.Frame(self.listbox, header=header, footer=footer)

    def main(self):
        self.ui = urwid.curses_display.Screen()
        self.ui.register_palette([
			('header', 'black', 'dark cyan', 'standout'),
			('std', 'default', 'default', 'bold'),
			])
        self.ui.run_wrapper(self.run)

        
    def run(self):
        self.size = self.ui.get_cols_rows()
        for name, t, data in self.sensor:
            for w, value in zip(self.widgets, data):
                w.update(value)
            self.draw_screen()
            keys = self.ui.get_input()
            if 'q' in keys or 'Q' in keys:
                break
            for k in keys:
                if k == "window resize":
                    self.size = self.ui.get_cols_rows()

    def draw_screen(self):
        canvas = self.top.render(self.size, focus=True)
        self.ui.draw_screen(self.size, canvas)


def main():
    """
    %prog [options] CONFIGFILE

    Console UI to display A/D values. CONFIGFILE is used to specify the A/D channels
    and calibration coefficients.
    """
    parser = OptionParser(usage=main.__doc__)
    parser.set_defaults(title='ENGINEERING SENSORS')
    parser.add_option('-v', '--verbose',
                      action='store_true',
                      dest='verbose',
                      help='write values to standard output')
    parser.add_option("-t", "--title",
                      type="string",
                      dest="title",
                      metavar="STRING",
                      help="specify the title of the UI display")
    opts, args = parser.parse_args()
    
    try:
        cfg = yaml.load(open(args[0], 'r'))
    except IndexError:
        parser.error('No configuration file specified')

    app = Application(cfg, opts.title)
    app.main()
    
if __name__ == '__main__':
    main()
