#!/usr/bin/env python
#
# Ping a remote micro modem.
#
from pymc.umodem.device import Umodem
from optparse import OptionParser
from datetime import datetime
import sys

def main():
    """
    %prog [options] serialdev remoteid

    Send a ping to a remote micro-modem. Writes a timestamp and the one-way travel
    time to standard output. A travel time of -1 indicates no response.
    """
    parser = OptionParser(main.__doc__)
    parser.set_defaults(timeout=10, id=1)
    parser.add_option('-t', '--timeout',
                      dest='timeout',
                      type='int',
                      help='number of seconds to wait for a reply (default %default)')
    parser.add_option('-i', '--id',
                      dest='id',
                      type='int',
                      help='ID of local micromodem (default %default)')
    
    opts, args = parser.parse_args()
    if len(args) < 2:
        parser.error('Missing arguments')

    um = Umodem(args[0], uid=opts.id)
    um.setclock()
    um['DTO'] = 5
    um['PTO'] = 7
    t0 = datetime.utcnow()
    tt = um.ping(int(args[1]), timeout=opts.timeout)
    sys.stdout.write('%s,%.3f\n' % (t0.isoformat(sep=' '), tt))

if __name__ == '__main__':
    main()
