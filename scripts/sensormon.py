#!/usr/bin/env python
#
# A console program to monitor a sensor.
#
import urwid.curses_display
import urwid
import struct
import pickle
from optparse import OptionParser
from decimal import Decimal
try:
    import json
except ImportError:
    import simplejson as json
    
class SensorBox(urwid.WidgetWrap):
    def __init__(self, name, units, precision):
        self.prec = Decimal(str(precision))
        w_name = urwid.Text(name, align='left', wrap='clip')
        self.w_value = urwid.Text(' ', align='center', wrap='clip')
        w_units = urwid.Text(units, align='right', wrap='clip')
        display_widget = urwid.GridFlow([w_name, self.w_value, w_units],
                                        15, 3, 1, 'left')
        urwid.WidgetWrap.__init__(self, display_widget)
        
    def update(self, value):
        self.w_value.set_text(str(Decimal(str(value)).quantize(self.prec)))

class Application(object):
    def __init__(self, sensor, cfg, title):
        channels = []
        self.widgets = []
        self.sensor = sensor
        for name in cfg['variables']:
            self.widgets.append(SensorBox(name, cfg['units'][name],
                                          cfg['precision'].get(name, '0.001')))
        self.widget_list = urwid.SimpleListWalker(self.widgets)
        self.listbox = urwid.ListBox(self.widget_list)
        title = urwid.Text(title, align='center')
        header = urwid.AttrWrap(title, 'header')
        footer = urwid.AttrWrap(urwid.Text('type q to exit', align='left'), 'header')
        self.top = urwid.Frame(self.listbox, header=header, footer=footer)

    def main(self):
        self.ui = urwid.curses_display.Screen()
        self.ui.register_palette([
			('header', 'black', 'dark cyan', 'standout'),
			('std', 'default', 'default', 'bold'),
			])
        self.ui.run_wrapper(self.run)

        
    def run(self):
        self.sensor.enable()
        self.sensor.wait_for_ready()
        self.sensor.pre_sample()
        self.size = self.ui.get_cols_rows()
        fmt = self.sensor.sample_format()
        while True:
            t, record = self.sensor.sample()
            data = struct.unpack(fmt, record)
            for w, value in zip(self.widgets, data):
                w.update(value)
            self.draw_screen()
            keys = self.ui.get_input()
            if 'q' in keys or 'Q' in keys:
                break
            for k in keys:
                if k == "window resize":
                    self.size = self.ui.get_cols_rows()
        self.sensor.disable()
        
    def draw_screen(self):
        canvas = self.top.render(self.size, focus=True)
        self.ui.draw_screen(self.size, canvas)


def main():
    """
    %prog [options] CONFIGFILE SENSOR

    Console UI to display sensor values.
    """
    parser = OptionParser(usage=main.__doc__)

    opts, args = parser.parse_args()
    if len(args) < 2:
        parser.error('Missing arguments')

    sensors = pickle.load(open(args[0], 'rb'))
    title = 'SENSOR %s' % args[1]

    for klass, config in sensors:
        if config['name'] == args[1]:
            app = Application(klass(config), json.loads(config['source']), title)
            app.main()
    
if __name__ == '__main__':
    main()
