#!/usr/bin/python
#
# $Id: powermgr.py,v 16124d5af846 2008/10/02 05:14:51 mikek $
#
# D-bus based power-switch manager for the HOT Mooring controller.
#
import gobject
import dbus
import dbus.service
import dbus.mainloop.glib
import hmc.bus
import hmc.gpio
import sys
from optparse import OptionParser
from ConfigParser import RawConfigParser

def main():
    parser = OptionParser()
    parser.add_option('-s', '--session',
                      action='store_true',
                      dest='session',
                      help='use session bus')
    parser.add_option('-f', '--config-file',
                      dest='file',
                      default='/usr/share/hmc/powermgr.cfg',
                      help='configuration file',
                      metavar='FILE')
    options, args = parser.parse_args()

    cfg = RawConfigParser()
    infile = open(options.file, 'r')
    cfg.readfp(infile)
    infile.close()
    
    if not cfg.has_section('interface'):
        cfg.add_section('interface')
        cfg.set('interface', 'bus',
                options.session and 'session' or 'system')

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    
    if cfg.get('interface', 'bus') == 'session':
        bus = dbus.SessionBus()
    else:
        bus = dbus.SystemBus()
        
    gobject.threads_init()
    pbus = hmc.bus.PowerBus(bus)

    for name, gpio_line in cfg.items('switches'):
        pbus.add_switch(hmc.gpio.Gpio(int(gpio_line)), name)
    
    try:
        gobject.MainLoop().run()
    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == '__main__':
    main()
