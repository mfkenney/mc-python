#!/usr/bin/python
#
# $Id: off.py,v 16124d5af846 2008/10/02 05:14:51 mikek $
#
from optparse import OptionParser
from ConfigParser import RawConfigParser
from pymc.gpio import Gpio

def main():
    parser = OptionParser(usage='%prog [options] switchname [switchname ...]')
    parser.add_option('-l', '--list-only',
                      action='store_true',
                      dest='list',
                      help='list the all of the switch names')
    parser.add_option('-f', '--config-file',
                      dest='file',
                      default='/usr/share/pymc/gpio.cfg',
                      help='configuration file',
                      metavar='FILE')
    options, args = parser.parse_args()
        
    cfg = RawConfigParser()
    infile = open(options.file, 'r')
    cfg.readfp(infile)
    infile.close()

    switches = dict(cfg.items('switches'))
    if options.list:
        print '\n'.join(switches.keys())
    else:
        if not args:
            parser.error('No switches specified')
        for arg in args:
            try:
                gpio = Gpio(int(switches[arg]))
                gpio.clear(force=True)
            except KeyError:
                print 'Invalid switch name, %s' % arg
            except AssertionError:
                pass


if __name__ == '__main__':
    main()
