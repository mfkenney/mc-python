#!/usr/bin/env python
from __future__ import print_function
import paho.mqtt.client as mqtt
import time


def on_message(client, udata, msg):
    _, sensor = msg.topic.split('/', 1)
    print("Starting sensor {}".format(sensor))
    time.sleep(10)


def on_connect(client, udata, flags, rc):
    if rc == 0:
        print("Connected")
        client.subscribe('start/#')


def main():
    mb = mqtt.Client(protocol=mqtt.MQTTv31)
    mb.on_connect = on_connect
    mb.on_message = on_message
    mb.connect('localhost', 1883)
    try:
        mb.loop_forever()
    except KeyboardInterrupt:
        print('Exiting ...')
    finally:
        mb.disconnect()


if __name__ == '__main__':
    main()
