#!/usr/bin/env python
#
# Extract the Sontek ADP data from a CMOP data file and create a series of
# netCDF files, one file per profile.
#
import yaml
import sys
import os
import time
import numpy
from Scientific.IO.NetCDF import NetCDFFile
from hachoir_core.field import Int16, Int8, Int32, UInt16, UInt8, UInt32, GenericVector,\
     String, Enum, FieldSet, Parser, ParserError
from hachoir_core.stream import StringInputStream, LITTLE_ENDIAN

class DateTimeType(FieldSet):
    def createFields(self):
        yield Int16(self, 'year')
        yield Int8(self, 'day')
        yield Int8(self, 'month')
        yield Int8(self, 'minute')
        yield Int8(self, 'hour')
        yield Int8(self, 'sec100')
        yield Int8(self, 'sec')

class ProfileHeader(FieldSet):
    ORIENTATION = {
        0: 'down',
        1: 'up',
        2: 'side'}
    TEMP_MODE = {
        0: 'user value',
        1: 'measured'}
    COORD_SYS = {
        0: 'beam',
        1: 'xyz',
        2: 'enu'}
    def createFields(self):
        yield UInt8(self, 'sync_char')
        if self['sync_char'].value != 0xa5:
            ParserError('Invalid sync character')
        yield UInt8(self, 'data_type')
        if self['data_type'].value != 0x10:
            ParserError('Invalid data type')
        yield UInt16(self, 'nbytes')
        yield String(self, 'serial_number', 10)
        yield UInt32(self, 'profile_number')
        yield DateTimeType(self, 'profile_time')
        yield Int8(self, 'nbeams')
        yield Enum(Int8(self, 'sensor_orientation'), self.ORIENTATION)
        yield Enum(Int8(self, 'temp_mode'), self.TEMP_MODE)
        yield Enum(Int8(self, 'coord_system'), self.COORD_SYS)
        yield UInt16(self, 'ncells')
        yield UInt16(self, 'cell_size')
        yield UInt16(self, 'blank_distance')
        yield UInt16(self, 'avg_interval')
        yield UInt16(self, 'npings')
        yield Int16(self, 'mean_heading')
        yield Int16(self, 'mean_pitch')
        yield Int16(self, 'mean_roll')
        yield Int16(self, 'mean_temp')
        yield UInt16(self, 'mean_press')
        yield UInt8(self, 'std_heading')
        yield UInt8(self, 'std_pitch')
        yield UInt8(self, 'std_roll')
        yield UInt8(self, 'std_temp')
        yield UInt16(self, 'std_press')
        yield UInt16(self, 'sound_speed')
        yield UInt16(self, 'spare')
        yield String(self, 'status', 20)

class Int16Vector(GenericVector):
    def __init__(self, parent, name, n_items, description=None):
        GenericVector.__init__(self, parent, name, n_items, Int16, 'item', description)
    
class UInt8Vector(GenericVector):
    def __init__(self, parent, name, n_items, description=None):
        GenericVector.__init__(self, parent, name, n_items, UInt8, 'item', description)

class ProfileFormat(Parser):
    endian = LITTLE_ENDIAN
    def createFields(self):
        yield ProfileHeader(self, 'header')
        for i in range(self['header']['ncells'].value):
            yield Int16Vector(self, 'vel[]', self['header']['nbeams'].value)
        for i in range(self['header']['ncells'].value):
            yield UInt8Vector(self, 'vel_std[]', self['header']['nbeams'].value)
        for i in range(self['header']['ncells'].value):
            yield UInt8Vector(self, 'amp[]', self['header']['nbeams'].value)
        yield UInt16(self, 'checksum')
        
def displayTree(outf, parent):
    for field in parent:
        outf.write(field.path)
        if field.is_field_set:
            outf.write('\n')
            displayTree(outf, field)
        else:
            outf.write(' = %s\n' % repr(field.display))
            

def extract_sensor(infile, name):
    for rec in yaml.load(infile):
        if 'sensor' in rec:
            if rec['sensor'] == name:
                secs, usecs = rec['clock']
                t = secs + usecs*1.0e-6
                yield 'data', (t, rec['data'])
        else:
            if rec['name'] == name:
                yield 'metadata', rec

# netCDF variable dictionary (table)
#
# name => [TYPECODE, (DIMENSIONS), ATTRS, CONVERT]
VARTABLE = [("tstart", ['l', (), {'units' : "seconds since 1970-01-01 UTC"}, lambda x: int(x)]),
            ('cell_size', ['f', (), {'units' : 'meters'}, lambda x: x*0.01]),
            ('blank_distance', ['f', (), {'units' : 'meters'}, lambda x: x*0.01]),
            ('avg_interval', ['l', (), {'units' : 'seconds'}, None]),
            ('npings', ['l', (), {'description': 'number of pings averaged'}, None]),
            ('mean_heading', ['f', (), {'units': 'degrees'}, lambda x: x*0.1]),
            ('mean_pitch', ['f', (), {'units': 'degrees'}, lambda x: x*0.1]),
            ('mean_roll', ['f', (), {'units': 'degrees'}, lambda x: x*0.1]),
            ('mean_temp', ['f', (), {'units': 'degreesC'}, lambda x: x*0.01]),
            ('mean_press', ['l', (), {'units': 'counts'}, None]),
            ('std_heading', ['f', (), {'units': 'degrees'}, lambda x: x*0.1]),
            ('std_pitch', ['f', (), {'units': 'degrees'}, lambda x: x*0.1]),
            ('std_roll', ['f', (), {'units': 'degrees'}, lambda x: x*0.1]),
            ('std_temp', ['f', (), {'units': 'degreesC'}, lambda x: x*0.1]),
            ('std_press', ['l', (), {'units': 'counts'}, None]),
            ('sound_speed', ['f', (), {'units': 'm/s'}, lambda x: x*0.1]),
            ('vel', ['s', ('cell', 'beam'), {'units': 'mm/s',
                                             'description': 'average velocity'}, None]),
            ('vel_std', ['s', ('cell', 'beam'), {'units': 'mm/s',
                                                'description': 'std. dev. of velocity'}, None]),
            ('amp', ['s', ('cell', 'beam'), {'units': 'counts',
                                             'description': 'signal amplitude'}, None])]

def get_profile_var(profile, name):
    if name.startswith('/'):
        return profile[name].value
    else:
        return profile['/header/'+name].value

def save_profile(timestamp, profile, **kwds):
    ncfile = time.strftime('adp%Y%m%d_%H%M.nc', time.gmtime(timestamp))
    nc = NetCDFFile(ncfile, 'w')

    nbeams = profile['header']['nbeams'].value
    ncells = profile['header']['ncells'].value
    nc.createDimension('beam', nbeams)
    nc.createDimension('cell', ncells)
    setattr(nc, 'beam_coordinates', profile['header']['coord_system'].display)
    setattr(nc, 'orientation', profile['header']['sensor_orientation'].display)
    setattr(nc, 'serial_number', str(get_profile_var(profile, 'serial_number')))
    for k, v in kwds.items():
        setattr(nc, k, v)
        
    for name, desc in VARTABLE:
        var = nc.createVariable(name, desc[0], desc[1])
        for k, v in desc[2].items():
            setattr(var, k, v)
        if name == 'vel':
            vel_shape = var.shape
        if not desc[1]:
            cvt = desc[3] or (lambda x: x)
            if name == 'tstart':
                var.assignValue(cvt(timestamp))
            else:
                x = get_profile_var(profile, name)
                var.assignValue(cvt(x))
    vel = numpy.empty(vel_shape, numpy.int16)
    vel_std = numpy.empty(vel_shape, numpy.int16)
    amp = numpy.empty(vel_shape, numpy.int16)
    for i in range(ncells):
        for j in range(nbeams):
            vel[i,j] = get_profile_var(profile, '/vel[%d]/item[%d]' % (i, j))
            vel_std[i,j] = get_profile_var(profile, '/vel_std[%d]/item[%d]' % (i, j))
            amp[i,j] = get_profile_var(profile, '/amp[%d]/item[%d]' % (i, j))

    nc.variables['vel'].assignValue(vel)
    nc.variables['vel_std'].assignValue(vel_std)
    nc.variables['amp'].assignValue(amp)
    nc.close()
    
def main(args):
    file = open(args[0], 'r')
    for tag, contents in extract_sensor(file, 'adp/1'):
        if tag == 'data':
            timestamp, data = contents
            stream = StringInputStream(data)
            profile = ProfileFormat(stream)
            save_profile(timestamp, profile, source=os.path.basename(args[0]))
            #displayTree(sys.stdout, profile)

if __name__ == '__main__':
    main(sys.argv[1:])
