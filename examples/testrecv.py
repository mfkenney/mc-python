#!/usr/bin/env python
#
#
import gobject
import dbus
import dbus.mainloop.glib
import bus
import sensor
import sys
import logging
import struct

def handler(secs, usecs, fmt, data, path=''):
    values = struct.unpack(str(fmt), data)
    logging.info('%s %d.%06d %s' % (path, secs, usecs, values))
    
def main(args):
    logging.basicConfig(level=logging.DEBUG)
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    bus = dbus.SessionBus()
    bus.add_signal_receiver(handler, dbus_interface='hmc.ISensor',
                            signal_name='sampleReady',
                            byte_arrays=True,
                            path_keyword='path')
    gobject.threads_init()
    try:
        gobject.MainLoop().run()
    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == '__main__':
    main(sys.argv[1:])
