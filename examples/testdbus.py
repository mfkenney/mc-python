#!/usr/bin/env python
#
#
import gobject
import dbus
import dbus.service
import dbus.mainloop.glib
import bus
import sensor
import sys
import logging
import time

def main(args):
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(message)s')
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
    gobject.threads_init()
    if args and args[0] == '--system':
        sbus = bus.SensorBus(dbus.SystemBus())
    else:
        sbus = bus.SensorBus(dbus.SessionBus())
    sbus.add_sensor(sensor.DummySensor())
    sbus.add_sensor(sensor.DummyBinarySensor())
    try:
        gobject.MainLoop().run()
    except KeyboardInterrupt:
        sys.exit(0)

if __name__ == '__main__':
    main(sys.argv[1:])
