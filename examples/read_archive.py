#!/usr/bin/env python
#
#
import yaml
import sys
import csv
import binascii
from decimal import Decimal

def extract_sensor(infile, name):
    for rec in yaml.load(infile):
        if 'sensor' in rec:
            if rec['sensor'] == name:
                secs, usecs = rec['clock']
                t = secs + usecs*1.0e-6
                yield 'data', (t, rec['data'])
        else:
            if rec['name'] == name:
                yield 'metadata', rec

def print_csv_header(writer, metadata):
    try:
        writer.writerow(['time'] + metadata['variables'])
    except KeyError:
        writer.writerow(['time', 'data'])

def data_quantize(data, metadata):
    """
    Quantize a list of data values using the precision information from
    the metadata dictionary.
    """
    if 'precision' not in metadata:
        return data
    if 'dprecision' not in metadata:
        # Convert precision values to Decimal objects and cache.
        d = {}
        for name, val in metadata['precision'].items():
            d[name] = Decimal(str(val))
        metadata['dprecision'] = d
    vals = zip(metadata['variables'], [Decimal(str(d)) for d in data])
    data = []
    prec = metadata['dprecision']
    for name, val in vals:
        data.append(str(val.quantize(prec[name])))
    return data
    
def print_as_csv(writer, t, data, metadata):
    if isinstance(data, list):
        data = data_quantize(data, metadata)
        writer.writerow([t] + data)
    else:
        # binary string, convert to hex
        b = binascii.b2a_hex('data')
        writer.writerow([t, b])

def main(args):
    sensor_name = args[0]
    metadata = {}
    writer = csv.writer(sys.stdout)
    for filename in args[1:]:
        file = open(filename, 'r')
        for tag, contents in extract_sensor(file, sensor_name):
            if tag == 'data':
                timestamp, data = contents
                if metadata:
                    print_as_csv(writer, int(timestamp), data, metadata)
            elif tag == 'metadata':
                if not metadata:
                    metadata = contents
                    print_csv_header(writer, metadata)
                
if __name__ == '__main__':
    main(sys.argv[1:])
