#!/usr/bin/env python
#
"""
.. :module: data
    :synopsis: sensor data management
"""
import msgpack
import logging
from redis import RedisError
from Queue import Empty


class DataQueue(object):
    """
    Implement a data queue using a Redis list
    """
    def __init__(self, rd, key, maxlen=0):
        self.rd = rd
        self.key = key
        self.maxlen = maxlen
        self.logger = logging.getLogger('dq')

    def __len__(self):
        return self.llen(self.key)

    def put(self, entry):
        try:
            self.rd.lpush(self.key, msgpack.packb(entry))
        except RedisError as e:
            self.logger.critical('Cannot add to queue: %r', e)
        if self.maxlen > 0:
            self.rd.ltrim(self.key, 0, self.maxlen - 1)

    def get(self, timeout=None, block=True):
        if block:
            entry = self.rd.brpop(self.key, timeout=timeout)
        else:
            entry = self.rd.rpop(self.key)
        if not entry:
            raise Empty
        return msgpack.unpackb(entry[1])
