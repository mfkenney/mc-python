#!/usr/bin/python
#
# NMEA sentence module.
#


class BadFormat(Exception):
    """Bad NMEA format exception."""
    pass


class ChecksumError(Exception):
    """Checksum error exception."""
    pass


def buildSentence(id, *args):
    """
    Factory function to create a Sentence object.

    :param id: sentence id string
    :param args: sentence fields.
    :return new NMEA sentence.
    :rtype: Sentence
    """
    s = ','.join(['$'+id] + [str(a) for a in args])
    return Sentence(s)


class Sentence(object):
    """
    Class to parse an NMEA sentence.

    >>> sentence = Sentence('$GPGGA,163901,2115.8711,N,11735.8436,E,2,7,0.4,17,M,11,M,38,0653')
    >>> sentence.id
    'GPGGA'
    >>> len(sentence)
    14
    >>> sentence[0]
    '163901'
    >>> sentence[-1]
    '0653'

    :ivar fields: list of sentence fields.
    :ivar id: sentence ID string.
    """

    def __init__(self, s):
        """
        Initialize the sentence data structure.
        Check that the sentence is in the proper format, verify the
        checksum, and split the fields into an array of strings.

        :param s: NMEA sentence.
        :type s: str

        :raise BadFormat: s does not start with a '$' character.
        :raise ChecksumError:

        """
        try:
            if s[0] != '$':
                raise BadFormat("Bad start character, %s" % s[0])
        except IndexError:
            raise BadFormat("Empty string")
        try:
            end = s.index('*')
            csum = '%02X' % reduce(lambda a, b: a ^ b,
                                   [ord(e) for e in s[1:end]], 0)
            sum = s[end+1:end+3].upper()
            if csum != sum:
                raise ChecksumError("Expected %s got %s" % (sum, csum))
        except ValueError:
            # no checksum present
            end = s.find('\r')
            if end == -1:
                end = len(s)
        self.fields = s[1:end].split(',')
        self.id = self.fields.pop(0)

    def __len__(self):
        """Return the number of data fields."""
        return len(self.fields)

    def __getitem__(self, i):
        """Return the Ith field of the sentence."""
        return self.fields[i]

    def __setitem__(self, i, value):
        """Update the Ith field of the sentence."""
        self.fields[i] = str(value)

    def __str__(self):
        """Convert the sentence into a string."""
        s = ','.join([self.id] + self.fields)
        csum = '%02X' % reduce(lambda a, b: a ^ b,
                               [ord(e) for e in s], 0)
        return "$%s*%s" % (s, csum)


def _test():
    import doctest
    doctest.testmod()

if __name__ == "__main__":
    _test()
