#!/usr/bin/env python
#
# Interface to SeaTramp profiler
#
import time
from pymc.hsim import Modem, HsimError
try:
    from collections import namedtuple
except ImportError:
    from pymc.util import namedtuple

def coroutine(func):
    def start(*args,**kwargs):
        cr = func(*args,**kwargs)
        cr.next()
        return cr
    return start

SensorMetaRecord = namedtuple('SensorMetaRecord',
                              ('name', 'conn', 'offset', 'z',
                               'desc', 'method', 'range',
                               'accuracy', 't_const', 'comment',
                               'interface', 'power', 'command'))
SensorCalRecord = namedtuple('SensorCalRecord', ('units', 'A', 'B', 'C', 'D', 'E'))

@coroutine
def build_record(target):
    while True:
        rec = []
        for name in SensorMetaRecord._fields:
            rec.append((yield))
        target.send(SensorMetaRecord._make(rec))
        
class SeaTramp(object):
    """
    Class to represent a SeaTramp profiler. Communication with the device takes
    place over a Seabird High Speed Inductive Modem (HSIM)
    """
    def __init__(self, hsim):
        self.hsim = hsim
        self.id = hsim.remote_id()

    def setclock(self, timestamp=None):
        if timestamp is None:
            timestamp = time.time()
        return self.hsim.pt('%%CLK %d' % timestamp)
        
    def send(self, cfgfile):
        """
        Send a new configuration file.
        """
        pass

    def getdata(self):
        """
        Fetch the most recent dataset.
        """
        pass

class SensFile(object):
    """
    Class to parse a SeaTramp sensor metadata file.
    """
    records = []
    def __init__(self, infile):
        @coroutine
        def new_record():
            while True:
                self.records.append((yield))
        builder = build_record(new_record())
        for line in infile:
            value = line.strip('\r\n "')
            builder.send(value)

    def __getitem__(self, idx):
        return self.records[idx]

    def __len__(self):
        return len(self.records)
    
class SensorCal(object):
    """
    Class to store SeaTramp sensor calibration information.
    """
    sensors = {}
    def __setitem__(self, name, infile):
        values = []
        for field, line in zip(SensorCalRecord._fields, infile):
            values.append(line.strip())
        self.sensors[name] = SensorCalRecord._make(values)
        
    def __getitem__(self, name):
        return self.sensors[name]
