#!/usr/bin/env python
#
# Serial port interface
#
import serial
from array import array
import logging


class SerialPort(serial.Serial):
    logger = logging.getLogger('port')

    def read_until(self, c):
        """
        Wait for the specified character or timeout. The returned tuple
        contains the status (False on timeout) and a string containing all
        of the characters returned from the device.

        :param c: character to wait for
        :type c: string
        :return: status, buffer
        :rtype: boolean, string
        """
        match = array('c', c)
        n = len(match)
        buf = array('c')
        while True:
            data = self.read(1)
            if not data:
                return False, buf.tostring()
            buf.append(data)
            if buf[-n:] == match:
                return True, buf[:-n].tostring()
            self.logger.debug('IN: %s', str(buf))

    def read_record(self, start, end='\n'):
        """
        Read a record bracketed by the characters start and end or None on
        timeout.
        """
        status, result = self.read_until(start)
        if not status:
            self.logger.debug('Record start not found')
            return None
        status, result = self.read_until(end)
        return status and result or None

    def purge(self, timeo=0.5):
        """
        Purge the serial interface
        """
        self.reset_input_buffer()
        timeout = self.timeout
        self.timeout = timeo
        while True:
            data = self.read(1024)
            if len(data) < 1024:
                break
        self.timeout = timeout
