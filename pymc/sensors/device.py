#!/usr/bin/env python
#
# Mooring Controller sensor interfaces.
#
import sys
import time
import fcntl
import threading
import logging
import os
import binascii
import base64
import calendar
import xml.etree.ElementTree as ET
from decimal import Decimal
from pymc.power import PowerSwitch, FakeDio, TsPowerSwitch
from pymc.adc import adread, Calibration
from pymc import nmea
from ports import SerialPort
from registry import _Sensor
import pymc.observer as observer
from pymc.pathdict import pathdict


class SensorTimeout(Exception):
    pass


class SensorNotEnabled(Exception):
    pass


class SerialSensor(_Sensor):
    """
    Base class to provide an interface to a serial sensor. A Sensor
    accepts commands and sends responses. All responses are terminated
    with an end-of-record character sequence (usually the command prompt).
    """
    eol = b'\r'
    eor = '\n'
    timeouts = {}

    def __init__(self, config):
        """
        Instance initializer.

        @param config: device configuration
        """
        self.config = config
        self.limit = 0
        self.logger = logging.getLogger('sensor.%s' % self.config['name'])
        self.dev_ready = threading.Event()
        self.port = None
        self.enabled = False
        self.proxy = self.__class__.lookup(config.get('interface/proxy', None))
        if self.proxy:
            self.eor = self.proxy.eor
        else:
            self.eor = config.get('interface/eor', self.eor).encode('us-ascii')
        self.lock = threading.Lock()
        t_limit = config.get('interface/timeout', None)
        if t_limit is not None:
            t_limit = float(t_limit)/1000.
        self.timeouts['command'] = t_limit
        if 'interface/gpio' in self.config:
            self.sw = TsPowerSwitch(self.config['interface/gpio'])
        else:
            self.sw = PowerSwitch(1, FakeDio())

    def __str__(self):
        return '<SerialSensor name=%s device=%s>' % (
            self.name,
            self.config.get('interface/device', None))

    def _try_lock(self):
        """
        Use a file lock to insure exclusive access to the serial device. This
        will block if another process is using the same sensor.
        """
        dev = os.path.basename(self.config['interface/device'])
        name = os.path.join('/var', 'tmp', 'sensor-%s.lock' % dev)
        self.lfile = open(name, 'w')
        fcntl.flock(self.lfile, fcntl.LOCK_EX)

    def _unlock(self):
        if self.lfile:
            fcntl.flock(self.lfile, fcntl.LOCK_UN)
            self.lfile.close()
            self.lfile = None

    def _initialize(self, t_wait):
        """
        Thread task to send the initialization commands. Asserts the
        self.ready Event when finished.
        """
        self.enabled = True
        time.sleep(t_wait)
        self.wakeup()
        for command in self.commands:
            try:
                self.send(command, timeout=self.timeouts['command'])
            except SensorTimeout:
                self.logger.critical('Timeout on command "%s"', command)
                self.disable()
                return
            except:
                self.disable()
                return
        self.logger.info('Sensor ready')
        self.dev_ready.set()

    def wakeup(self):
        pass

    def process(self, record):
        return record

    def wait_for_ready(self):
        """
        Wait for the sensor to be initialized. Uses the value of
        self.timeouts['ready'] or None (wait forever). On timeout, raise
        the SensorTimeout exception.
        """
        timeout = self.timeouts.get('ready', None)
        self.dev_ready.wait(timeout)
        if not self.dev_ready.isSet():
            raise SensorTimeout(self.config['name'])
        return self.config.get('source', '')

    def purge_input(self):
        if self.port:
            self.port.purge()

    def send(self, text, timeout=None):
        """
        Send a command to the sensor and return the response.
        """
        if not self.port:
            raise SensorNotEnabled(self.config['name'])

        with self.lock:
            self.purge_input()
            if text == r'\B':
                self.logger.info('Sending BREAK')
                self.port.sendBreak()
            else:
                self.logger.debug('Sending: %s' % repr(text))
                self.port.write(text.encode('us-ascii') + self.eol)
            self.port.timeout = timeout
            status, response = self.port.read_until(self.eor)
            if not status:
                raise SensorTimeout(self.config['name'])
            return response

    def metadata(self):
        return self.config.get('source', '')

    def enable(self):
        """
        Power-on and initialize the sensor.
        """
        if self.enabled:
            return
        if self.proxy:
            self.port = self.proxy.port
        else:
            if 'interface/device' in self.config:
                self._try_lock()
                self.port = SerialPort(self.config['interface/device'],
                                       self.config['interface/baud'],
                                       timeout=self.timeouts['command'])
            else:
                self.lfile = None
                self.port = SerialPort()
            self.logger.debug('Serial interface open %s', str(self.port))

        self.commands = self.config.get('init', [])
        # The warmup time is specified in milliseconds in
        # the configuration file.
        interval = float(self.config.get('interface/warmup', 0))/1000.
        t = threading.Thread(target=self._initialize, args=(interval,))
        t.setDaemon(True)
        self.sw.on()
        self.purge_input()
        t.start()
        self.logger.info('Initializing sensor')

    def disable(self):
        """
        Power-off the sensor and stop the reader thread.
        """
        if self.enabled:
            self.enabled = False
            if not self.proxy:
                self.port.close()
                self._unlock()
            self.logger.debug('Serial interface closed')
            self.port = None
            self.sw.off()
            self.dev_ready.clear()
            self.logger.info('Sensor disabled')

    def __enter__(self):
        self.enable()
        return self

    def __exit__(self, *args):
        self.disable()
        return False

    def pre_sample(self):
        pass

    def sample(self):
        raise NotImplementedError


class Sbe(SerialSensor):
    eol = b'\r'
    timeouts = {'sample': 5, 'ready': 6, 'command': 2}

    def wakeup(self):
        with self.lock:
            self.port.write('\r')
            time.sleep(0.5)

    def sample(self):
        cmd = self.config.get('sample/command', 'ts')
        post = self.config.get('sample/post')
        linenum = self.config.get('result/line', 1)
        fieldrng = self.config.get('result/fields', [0, 3])
        sel = slice(*fieldrng)

        t_sample = time.time()
        result = self.send(cmd, self.timeouts['sample'])
        if post:
            self.port.write(post + self.eol)
        # The sample is on the next-to-last line
        sample = result.split('\r\n')[linenum]
        values = [float(s) for s in sample.split(',')[sel]]
        return t_sample, values


class Isus(SerialSensor):
    eol = b''
    timeouts = {'sample': 60, 'ready': 35}
    bad_record = [float(0)] * 6

    def sample(self):
        self.purge_input()
        self.port.timeout = self.timeouts['sample']
        self.port.write(b'\r')
        self.port.read_until("'g'")
        self.port.write(b'g')
        t_sample = time.time()
        while True:
            line = self.port.readline()
            if not line:
                self.logger.warning('Timeout reading sample')
                return t_sample, self.bad_record
            if line.startswith('ISUS will start'):
                break
            self.logger.debug('INPUT: %s', repr(line))
            if line.startswith('SATND'):
                dark = line[:]
            elif line.startswith('SATNL'):
                light = line.strip().split(',')
        tbase = time.strptime(light[1], '%Y%j')
        secs = int(light[2]) * 3600
        sensor_time = calendar.timegm(tbase) + secs
        values = [sensor_time] + [float(x) for x in light[3:8]]
        return t_sample, values


class Gps(SerialSensor):
    timeouts = {'sample': 3, 'ready': 3, 'fix': 60}
    sentences = ['GPRMC', 'GPGGA']

    def purge_input(self):
        pass

    def next_sentence(self):
        line = self.port.readline()
        if not line:
            raise SensorTimeout
        try:
            return nmea.Sentence(line.strip())
        except (nmea.BadFormat, nmea.ChecksumError):
            return None

    def wait_for_fix(self):
        limit = self.timeouts['fix']
        t0 = time.time()
        while (time.time() - t0) < limit:
            s = self.next_sentence()
            # The 6th field in the GGA sentence is Fix Quality
            if s and s.id == 'GPGGA' and int(s[5]) > 0:
                self.logger.info('GPS fix obtained')
                return True
        return False

    def sample(self):
        self.port.timeout = self.timeouts['sample']
        if not self.wait_for_fix():
            self.logger.warning('Cannot obtain a good fix')
        n = len(self.sentences)
        i = 0
        values = []
        t_sample = 0
        while i < n:
            t = time.time()
            s = self.next_sentence()
            if not s:
                continue
            if s.id == self.sentences[i]:
                if i == 0:
                    t_sample = t
                i += 1
                values.append(str(s))
        return t_sample, base64.b64encode('\n'.join(values))


class Wqm(SerialSensor):
    """
    The Wetlabs Water Quality Monitor is configured to output data records
    on a periodic basis, every second in the water and every 6-10 seconds
    in air. The data records may be interspersed with status records which
    is why we read three lines.

    Time from power-on to first sample is ~105 seconds
    """
    timeouts = {'sample': 105, 'ready': 2}

    def purge_input(self):
        pass

    def sample(self):
        indicies = self.config['result/fields']
        n = len(indicies)
        bad_record = [float(0)] * n

        limit = 3
        self.port.timeout = self.timeouts['sample']
        values = []
        while limit > 0:
            t_sample = time.time()
            line = self.port.readline()
            self.logger.debug('INPUT: %s', repr(line))
            if line:
                line = line.lstrip('\x00  \t')
                if line.startswith('WQM') and ('SN' not in line):
                    values = []
                    columns = line.strip().split(',')
                    for s in [columns[i] for i in indicies]:
                        if s:
                            values.append(float(s))
                        else:
                            values.append(0.)
                    break
            else:
                break
            limit -= 1
        if not values:
            self.logger.warning('Cannot read data record')
            values = bad_record
        return t_sample, values


class Workhorse(SerialSensor):
    timeouts = {'ready': 10, 'sample': None, 'command': None}

    def wakeup(self):
        with self.lock:
            self.port.sendBreak()
            time.sleep(3)

    def query(self, setting):
        """
        Query a parameter setting.
        """
        response = self.send('%s?' % setting,
                             self.timeouts['command']).split('\r\n')[1]
        # Response has the form: PARAM = VALUE ---------- DESCRIPTION
        param, eq, value, desc = response.split(' ', 3)
        return value

    def _set_timeout(self):
        wp = int(self.query('WP'), 10)
        tp = self.query('TP')
        # TP = MM:SS.ss
        min, sec = [float(t) for t in tp.split(':')]
        self.timeouts['sample'] = wp*(sec + min*60) + 2
        self.logger.debug('Timeout set to %.3f', self.timeouts['sample'])

    def pre_sample(self):
        self._set_timeout()

    def sample(self):
        """
        Take a sample, a single ensemble of pings. The number of pings is
        set by the WP parameter and the ping-interval is set by the TP
        parameter.
        """
        cmd = self.config.get('sample/command', 'cs')
        post = self.config.get('sample/post')
        t_sample = time.time()
        result = self.send(cmd, self.timeouts['sample'])
        if post:
            self.port.write(post + self.eol)
        sample = result.split('\r\n')[1]
        # Re-encode the hex output as base64 to save a bit of space
        # in the data archive.
        return t_sample, base64.b64encode(binascii.a2b_hex(sample))


class Master(SerialSensor):
    """
    Group multiple sensors together under a single serial interface. The
    Master acts as a proxy for all communication.
    """
    def __init__(self, config):
        SerialSensor.__init__(self, config)
        self.sample_event = observer.Event()
        self.ready_event = observer.Event()
        self.pre_sample_event = observer.Event()
        self.get_names = observer.Event()

    def wait_for_ready(self):
        super(Master, self).wait_for_ready()
        return self.ready()

    def ready(self):
        return self.ready_event(time.time())

    def get_name(self):
        return self.get_names()

    def pre_sample(self):
        self.pre_sample_event()

    def sample(self):
        """
        Trigger sampling of all Slave sensors
        """
        t_sample = time.time()
        results = self.sample_event()
        return t_sample, results


class Slave(SerialSensor):
    """
    All interaction (enable/sample/disable) is proxied by the associated
    Master.
    """
    def __init__(self, config):
        SerialSensor.__init__(self, config)
        self.proxy.ready_event += self.ready_cb
        self.proxy.sample_event += self.sample_cb
        self.proxy.pre_sample_event += self.pre_sample

    def ready_cb(self, t_power):
        """
        Called by the Master when enabled.
        """
        self.enabled = True
        self.port = self.proxy.port
        return self.config['name'], self.metadata()

    def sample_cb(self, *args, **kwds):
        """
        Called by the Master when sampling.
        """
        timestamp, data = self.sample(*args, **kwds)
        return self.config['name'], timestamp, data


class Sim(Master):
    """
    Seabird Surface Inductive Modem. A instance of this class is used
    to manage other sensors on the Inductive "bus" (subsensors). Each
    subsensor inherits from Uim.

    This class uses the Observer Pattern to notify the subsensors of
    when they are enabled and when it is time to read a sample.
    """
    eol = '\r'
    timeouts = {'command': 6, 'sample': 5}

    def ready(self):
        self.send('PWRON', self.timeouts['command'])
        results = self.ready_event(time.time())
        return results

    def sample(self):
        """
        Trigger sampling of all UIM sensors
        """
        t_sample = time.time()
        results = self.sample_event()
        self.send('PWROFF', self.timeouts['command'])
        return t_sample, results


class Uim(Slave):
    """
    Seabird Underwater Inductive Modem. Sensors inheriting from this class
    may not be addressed directly. All interaction (enable/sample/disable)
    is proxied by the associated Sim.
    """
    def __init__(self, config):
        Slave.__init__(self, config)
        self.t_warmup = float(self.config.get('interface/warmup', 0))/1000.

    def format_im_command(self, cmd):
        if cmd.startswith('!'):
            cmd = cmd[1:]
            prefix = '!'
        elif cmd.startswith('#'):
            cmd = cmd[1:]
            prefix = '#'
        else:
            prefix = '#'
        return '%s%02d%s' % (prefix,
                             self.config['interface/uim_addr'],
                             cmd)

    def ready_cb(self, t_power):
        t_wait = self.t_warmup - (time.time() - t_power)
        if t_wait > 0:
            time.sleep(t_wait)
        self.enabled = True
        self.port = self.proxy.port
        return self.config['name'], self.metadata()


class Sbeuim(Uim):
    """
    Class for Seabird sensors with integrated UIMs
    """
    eol = b'\r'
    timeouts = {'sample': 5, 'ready': 5, 'command': 5}
    delays = {}
    t_sample = 0
    bad_record = [float(-1)] * 10

    def pre_sample(self):
        cmd = self.config.get('sample/pre')
        if cmd:
            for c in cmd.split():
                self.send(self.format_im_command(c), self.timeouts['command'])
                if self.delays.get('command'):
                    time.sleep(self.delays.get('command'))
        self.t_sample = time.time() + self.delays.get('sample', 0)

    def sample(self):
        cmd = self.config.get('sample/command', 'ts')
        linenum = self.config.get('result/line', 1)
        fieldrng = self.config.get('result/fields', [1, 2])
        sel = slice(*fieldrng)
        if self.t_sample > time.time():
            try:
                time.sleep(self.t_sample - time.time())
            except IOError:
                pass

        values = []
        attempts = 2
        while attempts:
            attempts -= 1
            t_sample = time.time()
            result = self.send(self.format_im_command(cmd),
                               self.timeouts['command'])
            try:
                sample = result.split('\r\n')[linenum]
                fields = sample.split(',')
                values = [float(f) for f in fields[sel]]
            except Exception:
                self.logger.warning('Cannot parse data record: %r',
                                    result)
                values = []
            if values:
                break

        if not values:
            values = self.bad_record[sel]
        return t_sample, values


class Seafetim(Sbeuim):
    delays = {'command': 2, 'sample': 10}


class Wqmim(Uim):
    """
    Wetlabs Water Quality Monitor with attached UIM
    """
    timeouts = {'command': 5, 'sample': 120}
    t_start = 0

    def sample(self):
        indicies = self.config['result/fields']
        n = len(indicies)
        bad_record = [float(0)] * n

        self.logger.info('Retrieving previous sample')
        attempts = 2
        timeout = self.timeouts['command']
        while attempts:
            attempts -= 1
            result = self.send(self.format_im_command('!sendbreak'), 3)
            if 'Error' in result:
                self.logger.info('Retry the sendbreak command')
                self.send(self.format_im_command('!sendbreak'), 3)
            else:
                time.sleep(1)
            t_sample = time.time()
            result = self.send(self.format_im_command('$glso'), timeout)
            timeout = 5
            self.logger.debug('RESULT: %s', repr(result))
            values = []
            for line in result.split('\r\n'):
                if line.startswith('WQM,'):
                    columns = line.strip().split(',')
                    for s in [columns[i] for i in indicies]:
                        if s:
                            values.append(float(s))
                        else:
                            values.append(float(0))
                    break
            if values:
                break
        # Send a GDATA command to start the next sample. We try to send
        # the command twice, if both attempts fail, the next GLSO command
        # sent to the device will retrieve the same data record as we are
        # retrieving now.
        #
        # TODO: try to handle this better...
        self.logger.info('Starting next sample')
        try:
            self.send(self.format_im_command('!gdata'), 5)
        except SensorTimeout:
            try:
                self.send(self.format_im_command('!gdata'), 5)
            except SensorTimeout:
                self.logger.warning('Timeout sending GDATA command')
        if not values:
            self.logger.warning('Cannot read data record')
            values = bad_record
        return t_sample, values


class S9(Master):
    timeouts = {'sample': 3}

    def purge_input(self):
        pass

    def sample(self):
        # We use a fairly long warm-up to allow the sensor to
        # stabilize, therefore, we need to purge any existing
        # data from the serial input buffer.
        self.port.reset_input_buffer()
        ts, results = 0, []
        prefix = b'<creport'
        suffix = b'</creport>'
        self.port.timeout = self.timeouts['sample']
        rec = self.port.read_record(prefix, suffix)
        if rec:
            ts = time.time()
            try:
                doc = ET.fromstring(prefix + rec + suffix)
            except ET.ParseError:
                self.logger.warning('Cannot parse S9 data: %r',
                                    prefix + rec + suffix)
            else:
                results = self.sample_event(ts, doc)
        else:
            self.logger.critical('No data')
        return ts, results


class S9ica(Slave):
    def sample_cb(self, ts, doc):
        values = []
        elem = doc.find('S9CD')
        if elem is not None:
            try:
                values = [float(s) for s in elem.text.split(',')]
            except Exception:
                values = []
        return self.config['name'], ts, values


class S9icaraw(Slave):
    def sample(self, ts, doc):
        values = []
        elem = doc.find('S9CRD')
        if elem is not None:
            try:
                values = [float(s) for s in elem.text.split(',')]
            except Exception:
                values = []
        return ts, values


class S9wind(Slave):
    def sample(self, ts, doc):
        values = []
        elem = doc.find('PORT1')
        if elem is not None:
            # Skip the leading '$'
            line = elem.text.strip(' \t\n\r$').split(',')
            try:
                values = [float(s) for s in line]
            except Exception:
                values = []
        return ts, values


class Metpack(SerialSensor):
    timeouts = {'sample': 5}

    def purge_input(self):
        pass

    def sample(self):
        ts, values = 0, []
        self.port.timeout = self.timeouts['sample']
        rec = self.port.read_record('\x02', end=self.eor)
        ts = time.time()
        if rec:
            line = rec.strip().split(',')
            try:
                values = [float(x) for x in line[1:-2]]
            except Exception:
                values = []
        else:
            self.logger.critical('No data')
        return ts, values


class Seafet(SerialSensor):
    timeouts = {'sample': 6}

    def purge_input(self):
        pass

    def wakeup(self):
        with self.lock:
            self.port.write(b'\r')
            time.sleep(3)

    def sample(self):
        ts, values = 0, []
        fieldrng = self.config.get('result/fields', [1, 2])
        sel = slice(*fieldrng)

        self.port.timeout = self.timeouts['sample']
        self.port.write(b's\r')
        ts = time.time()
        rec = self.port.read_record(b'SAT', end=self.eor)
        if rec:
            line = rec.strip().split(',')
            try:
                values = [float(x) for x in line[sel]]
            except Exception:
                self.logger.critical('Cannot parse: %r', rec)
                values = []
        else:
            self.logger.critical('No data')
        return ts, values


class Bdl(Master):
    eol = b'\r'


class Bdldata(Slave):
    eol = b'\r'
    timeouts = {'sample': 20, 'ready': 5, 'command': 2}
    bad_record = [float(0)] * 10

    def sample(self):
        attempts = 2
        while attempts:
            attempts -= 1
            values = []
            t_sample = time.time()
            result = self.send('#D', self.timeouts['sample'])
            fields = result.strip().split()
            if 'UNINITIALIZED' in result or len(fields) < 10:
                self.logger.info('Device is not initialized')
                time.sleep(5)
                continue
            for f in fields[:10]:
                try:
                    values.append(float(f))
                except ValueError:
                    values.append(float(0))
        if not values:
            self.logger.warning('Cannot read data record')
            values = self.bad_record
        return t_sample, values


class Bdlclock(Slave):
    eol = b'\r'
    timeouts = {'sample': 5, 'ready': 5, 'command': 2}

    def sample(self):
        t_sample = time.time()
        result = self.send('#CLOCK', self.timeouts['sample'])
        dstr, tstr = result.strip().split()
        values = [int(e) for e in dstr.split('/')]
        values.extend([int(e) for e in tstr.split(':')])
        return t_sample, values


class Adc(_Sensor):
    """
    Class to represent a virtual analog sensor composed of one or more A/D
    channels.  This class provides the same interface as the serial
    sensors.
    """
    def __init__(self, config):
        self.config = config
        # Store calibration coefficients for each channel
        self.cal = []
        self.prec = []
        for name in config['variables']:
            self.cal.append(Calibration(
                [Decimal(c) for c in config['cal/%s' % name]],
                config['units/%s' % name]))
            self.prec.append(Decimal(config['precision/%s' % name]))
        self.n_avg = config.get('interface/average')

    def enable(self):
        pass

    def disable(self):
        pass

    def __enter__(self):
        self.enable()
        return self

    def __exit__(self, *args):
        self.disable()
        return False

    def send(self, text, timeout=None):
        return ''

    def wait_for_ready(self):
        return self.metadata()

    def metadata(self):
        return self.config.get('source', '')

    def pre_sample(self):
        pass

    def sample(self):
        t, rec = adread(self.n_avg)
        return t, [float((fn(x))[0].quantize(p))
                   for fn, x, p in zip(self.cal, rec, self.prec)]


class Dummy(_Sensor):

    def __init__(self, config):
        self.config = config
        self.ready = threading.Event()
        self.logger = logging.getLogger('sensor.%s' % self.config['name'])

    def _initialize(self, t_wait):
        """
        Thread task to send the initialization commands. Asserts the
        self.ready Event when finished.
        """
        time.sleep(t_wait)
        self.logger.info('Sensor ready')
        self.ready.set()

    def enable(self):
        interval = float(self.config.get('interface/warmup', 0))/1000.
        t = threading.Thread(target=self._initialize, args=(interval,))
        t.setDaemon(True)
        t.start()
        self.logger.info('Initializing sensor')

    def disable(self):
        pass

    def send(self, text, timeout=None):
        return ''

    def wait_for_ready(self):
        return self.metadata()

    def metadata(self):
        return self.config.get('source', '')

    def pre_sample(self):
        pass

    def sample(self):
        t_sample = time.time()
        time.sleep(5)
        return t_sample, [Decimal(e) for e in [1., 2., 3., 4.]]


def sensor_config(doc):
    """
    Convert a YAML document containing the sensor metadata into a
    configuration dictionary for a Sensor object and the Sensor class.

    Returns a tuple of config-dictionary, class. Class will be None if the
    classname specified in the document is invalid.
    """
    config = pathdict(doc)
    thismodule = sys.modules[__name__]
    klass = getattr(thismodule, config['interface/module'].capitalize(), None)
    return config, klass

if __name__ == '__main__':
    import pickle
    sensors = pickle.load(open(sys.argv[1], 'rb'))
    objs = {}
    for klass, config in sensors:
        objs[config['name']] = klass(config)

    for k, v in objs.items():
        obj = v
        cls = v.__class__
        print k, obj, cls.lookup('eng/1')
