#!/usr/bin/env python
#
# D-bus interface to the sensors.
#
import dbus.service
import threading
import logging

BUS_NAME = 'hmc.Sensors'
INTERFACE = 'hmc.ISensor'

class SensorNotReady(Exception):
    _dbus_error_name = 'hmc.ISensor.NotReady'

class SensorInUse(Exception):
    _dbus_error_name = 'hmc.ISensor.InUse'

class SensorProxy(dbus.service.Object):
    trigger_timeout = 30
    def __init__(self, bus_name, path, device, trigger):
        self.device = device
        self.logger = logging.getLogger(path[1:].lower().replace('/', '.'))
        self.ready = False
        self.inuse = False
        self.trigger = trigger
        dbus.service.Object.__init__(self, object_path=path, bus_name=bus_name)

    @dbus.service.method(dbus_interface=INTERFACE,
                         in_signature='', out_signature='s')
    def getMetadata(self):
        return self.device.metadata()

    @dbus.service.method(dbus_interface=INTERFACE,
                         in_signature='', out_signature='')
    def enableSensor(self):
        def runner():
            try:
                self.device.enable()
                metadata = self.device.wait_for_ready()
            except Exception:
                self.logger.info('Disabling sensor')
                self.device.disable()
                self.sensorTimeout()
            else:
                self.ready = True
                if isinstance(metadata, list):
                    for name, data in metadata:
                        self.sensorReady(metadata,
                                         path='/'.join([self.__dbus_object_path__, name]))
                    self.sensorReady('')
                else:
                    self.sensorReady(metadata)

        if not self.ready:
            self.logger.info('Enabling sensor')
            t = threading.Thread(target=runner)
            t.setDaemon(True)
            t.start()

    @dbus.service.method(dbus_interface=INTERFACE,
                         in_signature='', out_signature='')
    def disableSensor(self):
        if self.ready:
            self.logger.info('Disabling sensor')
            self.device.disable()
            self.ready = False

    @dbus.service.method(dbus_interface=INTERFACE,
                         in_signature='sd', out_signature='s')
    def sendCommand(self, command, timeout):
        return self.device.send(str(command), timeout)
    
    @dbus.service.method(dbus_interface=INTERFACE,
                         in_signature='', out_signature='')
    def startSample(self):
        def runner():
            try:
                self.device.pre_sample()
                self.sensorArmed()
                self.trigger.wait(timeout=self.trigger_timeout)
                if not self.trigger.isSet():
                    raise Exception
                timestamp, result = self.device.sample()
            except Exception:
                self.sensorTimeout()
            else:
                secs = int(timestamp)
                usecs = int((timestamp - secs)*1e6)
                if isinstance(result, list):
                    # If the sensor manages a group of "subsensors", the results are
                    # returned as a list. We emit a separate signal for each.
                    for name, fmt, data in result:
                        self.sampleReady(secs, usecs, fmt, data,
                                         path='/'.join([self.__dbus_object_path__, name]))
                    self.sampleReady(secs, usecs, '', '')
                else:
                    self.sampleReady(secs, usecs, self.device.sample_format(), result)
            self.inuse = False

        if self.inuse:
            raise SensorInUse
        if not self.ready:
            raise SensorNotReady
        self.trigger.clear()
        self.logger.info('Start sampling thread')
        t = threading.Thread(target=runner)
        t.setDaemon(True)
        t.start()
        self.inuse = True

    @dbus.service.signal(dbus_interface=INTERFACE, signature='uusay', path_keyword='path')
    def sampleReady(self, secs, usecs, fmt, record):
        pass

    @dbus.service.signal(dbus_interface=INTERFACE, signature='s', path_keyword='path')
    def sensorReady(self, contents):
        pass

    @dbus.service.signal(dbus_interface=INTERFACE, signature='')
    def sensorTimeout(self):
        pass

    @dbus.service.signal(dbus_interface=INTERFACE, signature='')
    def sensorArmed(self):
        pass

class SensorBus(dbus.service.Object):
    def __init__(self, sensors, bus_class, on_exit):
        bus_name = dbus.service.BusName(BUS_NAME, bus=bus_class())
        self.devices = []
        self.on_exit = on_exit
        self.logger = logging.getLogger('bus')
        self.trigger = threading.Event()
        for klass, config in sensors:
            if not 'interface/proxy' in config:
                path = '/Sensor/%s' % config['name']
                self.logger.debug('Adding sensor %s', path)
                dev = klass(config)
                self.devices.append((path, dev))
                SensorProxy(bus_name, path, dev, self.trigger)
        dbus.service.Object.__init__(self, object_path='/Sensors', bus_name=bus_name)

    @dbus.service.method(dbus_interface='hmc.ISensorBus',
                         in_signature='', out_signature='ao')
    def listSensors(self):
        return [path for path, dev in self.devices]

    @dbus.service.method(dbus_interface='hmc.ISensorBus',
                         in_signature='', out_signature='')
    def triggerSample(self):
        self.trigger.set()

    @dbus.service.method(dbus_interface='hmc.ISensorBus',
                         in_signature='', out_signature='')
    def quit(self):
        self.logger.info('Exiting ...')
        for path, dev in self.devices:
            try:
                dev.disable()
            except:
                pass
        self.on_exit()
