#!/usr/bin/env python
#
# Central registry for Sensors.
#
import weakref


class UnnamedSensor(Exception):
    pass


class _Sensor(object):
    _registry = weakref.WeakValueDictionary()

    def __new__(cls, config):
        global _registry
        try:
            name = config['name']
        except KeyError:
            raise UnnamedSensor
        obj = object.__new__(cls)
        obj._name = name
        _Sensor._registry[name] = obj
        return obj

    @classmethod
    def lookup(cls, name):
        return cls._registry.get(name, None)

    @property
    def name(self):
        return self._name
