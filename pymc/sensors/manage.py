#!/usr/bin/env python
#
# Module to manage a set of Sensors
#
from __future__ import with_statement
import threading
import logging
import Queue

def threaded(f):
    """ Decorator function to spawn methods into separate threads """
    def wrapper(*args, **kwargs):
        t=threading.Thread(target=f, args=args, kwargs=kwargs)
        t.setDaemon(True)
        t.start()
    return wrapper

class SensorAlreadyEnabled(Exception):
    pass

class SensorNotReady(Exception):
    pass

class SensorManager(object):
    """
    Manage the various phases of Sensor operation, enable/disable/sample.
    """
    def __init__(self, sensor):
        self.sensor = sensor
        self.ready = False
        self.lock = threading.Lock()
        
    @threaded
    def enable(self, callback):
        """
        Enable the sensor. When the sensor is ready, call the callback and pass
        it a tuple of:

            'ready', sensor_name, sensor_metadata
        """
        try:
            with self.lock:
                if self.ready:
                    raise SensorAlreadyEnabled
                self.sensor.enable()
                metadata = self.sensor.wait_for_ready()
                self.ready = True
                if isinstance(metadata, list):
                    for name, m in metadata:
                        callback(('ready', name, m))
                else:
                    callback(('ready', self.sensor.name, metadata))
        except Exception, e:
            name = self.sensor.get_name()
            if isinstance(name, list):
                map(lambda n: callback(('error', n, e)), name)
            else:
                callback(('error', name, e))

    @threaded
    def sample(self, callback, trigger):
        try:
            with self.lock:
                if not self.ready:
                    raise SensorNotReady
                self.sensor.pre_sample()
                name = self.sensor.get_name()
                if isinstance(name, list):
                    map(lambda n: callback(('armed', n, None)), name)
                else:
                    callback(('armed', name, None))
                trigger.wait()
                timestamp, result = self.sensor.sample()
                secs = int(timestamp)
                usecs = int((timestamp - secs)*1e6)
                response = []
                if isinstance(result, list):
                    for name, t, fmt, data in result:
                        s_secs = int(t)
                        s_usecs = int((t - s_secs)*1e6)
                        callback(('sample', name, (s_secs, s_usecs, fmt, data)))
                else:
                    callback(('sample', self.sensor.name, (secs, usecs,
                                                           self.sensor.sample_format(),
                                                           result)))
        except Exception, e:
            name = self.sensor.get_name()
            if isinstance(name, list):
                map(lambda n: callback(('error', n, e)), name)
            else:
                callback(('error', name, e))

    def command(self, text, timeout):
        if not self.ready:
            raise SensorNotReady
        return self.sensor.send(text, timeout)

    def disable(self):
        self.sensor.disable()
        self.ready = False

class SensorGroup(object):
    def __init__(self, sensors):
        self.sensors = [SensorManager(s) for s in sensors]
        names = []
        for s in sensors:
            name = s.get_name()
            if isinstance(name, list):
                names.extend(name)
            else:
                names.append(name)
        n = len(names)
        self.state = dict(zip(names, ['off']*n))
        self.logger = logging.getLogger('sensors')
        
    def enable(self):
        remaining = self.state.keys()
        queue = Queue.Queue()
        for s in self.sensors:
            s.enable(queue.put)
        response = []
        while remaining:
            self.logger.debug('Waiting for: %s', repr(remaining))
            status, name, result = queue.get()
            if name not in remaining:
                continue
            if isinstance(result, Exception):
                self.logger.warn('Failed to enable %s (%s)', name, repr(result))
            else:
                self.logger.debug('%s enabled', name)
                self.state[name] = 'enabled'
                response.append((name, result))
            remaining.remove(name)
        return response

    def sample(self):
        remaining = [name for name in self.state if self.state[name] == 'enabled']
        queue = Queue.Queue()
        trigger = threading.Event()
        for s in self.sensors:
            s.sample(queue.put, trigger)
        while remaining:
            self.logger.debug('Waiting for: %s', repr(remaining))
            status, name, result = queue.get()
            if name not in remaining:
                continue
            if isinstance(result, Exception):
                self.logger.warn('Failed to arm %s (%s)', name, repr(result))
            else:
                self.logger.debug('%s armed', name)
                self.state[name] = 'armed'
            remaining.remove(name)
        trigger.set()
        remaining = [name for name in self.state if self.state[name] == 'armed']
        response = []
        while remaining:
            self.logger.debug('Waiting for: %s', repr(remaining))
            status, name, result = queue.get()
            if name not in remaining:
                continue
            if isinstance(result, Exception):
                self.logger.warn('Failed to sample %s (%s)', name, repr(result))
            else:
                self.logger.debug('%s sampled', name)
                response.append((name, result))
            remaining.remove(name)
        return response

    def disable(self):
        for s in self.sensors:
            s.disable()
