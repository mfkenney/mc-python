#!/usr/bin/env python
#
# Interface to a Seabird High Speed Inductive Modem (HSIM)
#
import time
import serial
import fcntl
from xml.etree import ElementTree as ET
from collections import deque
from string import printable

CLEARBUF = '@'
DELETE = '\x08'
EOL = '\r'
ESC = '\x1b'

class HsimError(Exception):
    """
    Raised when an error is reported by the HSIM.

    @ivar type: error type code
    @ivar msg: error message
    """
    def __init__(self, type, msg):
        self.type = type
        self.msg = msg

    def __str__(self):
        return "<Error type='%s' msg='%s' />" % (self.type, self.msg)
    
class BuildStanza(ET.TreeBuilder):
    """
    Extend the TreeBuilder class to track the XML tree depth so we can
    parse a stream of XML stanzas. Each stanza is at a depth of 1 under
    the top-level <stream> element.
    """
    depth = 0
    stanza_depth = 1
    def __init__(self, stanza_cb, *args, **kwds):
        self.stanza_cb = stanza_cb
        ET.TreeBuilder.__init__(self, *args, **kwds)
        
    def start(self, tag, attrs):
        self.depth += 1
        return ET.TreeBuilder.start(self, tag, attrs)

    def end(self, tag):
        elem = ET.TreeBuilder.end(self, tag)
        self.depth -= 1
        if self.depth == self.stanza_depth:
            self.stanza_cb(elem)
        return elem

class Modem(object):
    icd_timeout = 1.7
    
    def __init__(self, port):
        """
        Instance initializer.

        @param port: serial port interface
        """
        self.port = port
        self.parser = ET.XMLTreeBuilder(target=BuildStanza(self._store_stanza))
        self.stanzas = deque()
        self.parser.feed('<stream>')

    def __del__(self):
        self.port.close()
        self.parser.feed('</stream>')
        self.parser.close()
        
    def _store_stanza(self, stanza):
        self.stanzas.append(stanza)

    def get_stanza(self):
        c = self.port.read(1)
        while c:
            if c not in printable:
                self.parser.feed('&#x%03X;' % ord(c))
            else:
                self.parser.feed(c)
            try:
                elem = self.stanzas.pop()
                if elem.tag == 'Error':
                    raise HsimError(elem.get('type'),
                                    elem.get('msg'))
                yield elem
            except IndexError:
                pass
            c = self.port.read(1)
            
    def wait_for_element(self, match='Executed'):
        for elem in self.get_stanza():
            if elem.tag == match:
                return elem
        return None

    def wakeup(self):
        self.port.write(EOL)
        return self.wait_for_element()

    def send_command(self, command, id=None, data=[]):
        """
        Send a command to the local or remote HSIM. The return value is a list
        of ElementTree.Element objects representing the XML-encoded response.

        @var command: command string
        @var id: ID of remote HSIM
        @var data: command data iterator
        """
        if id is not None:
            addr = '!%02d:' % id
        else:
            addr = ''
        self.port.write(addr + command + EOL)
        if data:
            time.sleep(100e-3)
            for chunk in data:
                self.port.write(chunk)
            time.sleep(self.icd_timeout)
        return list(self.get_stanza())

    def __getattr__(self, name):
        """
        Map a method name to a command.
        """
        def _do_cmd(id=None, data=[]):
            return self.send_command(name, id=id, data=data)
        return _do_cmd

    def remote_id(self):
        """
        Return the ID (address) of the remote HSIM.
        """
        response = self.send_command('id?')
        # First element of response is <RemoteReply>
        settings = response[0].find('.//Settings')
        if settings is not None:
            return int(settings.get('ID'))
        return None
        
    def binary_transfer(self, source, command='#PB00:', reply='PowerOff'):
        response = self.send_command(command, data=source)
        return (response[-1].tag == reply)

    def pb(self, source):
        """
        Peer-to-peer binary transfer.

        @var source: iterator providing binary data.
        @return: True if successful, otherwise False
        """
        return self.binary_transfer(source)

    def sampleappend(self, source):
        """
        Append binary data to sample buffer.
        
        @var source: iterator providing binary data.
        @return: True if successful, otherwise False
        """
        return self.binary_transfer(source, command='SAMPLEAPPEND', reply='Executed')
    
    def pt(self, text):
        """
        Peer-to-peer text transfer.

        @var text: string of text to send
        @return: True if successful, otherwise False
        """
        response = self.send_command('#PT00:'+text)
        return (response[-1].tag == 'Executed')

