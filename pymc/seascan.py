#!/usr/bin/env python
#
# Interface to a Seascan real-time clock.
#
import time
import serial
import fcntl
import subprocess
from array import array
from pymc.gpio import Gpio


def set_sys_time(tstamp):
    """
    Callback function for Clock.get_time. Converts the Seascan timestamp
    into an absolute time and passes that to the 'date' command.
    """
    t = tstamp.tounixtime()
    subprocess.call(['date', '-u',
                     time.strftime('%m%d%H%M%Y.%S', time.gmtime(t))])


class Timestamp(object):
    """
    Class to represent the Seascan timestamp.

    >>> Timestamp.fromunixtime(1234567890)
    Timestamp(0, 23, 21, 30, epoch=1234483200)
    >>>
    """
    def __init__(self, days, hours, minutes, seconds, epoch=0):
        self.days = days
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds
        self.epoch = epoch

    @staticmethod
    def fromunixtime(t):
        """
        Create a Timestamp from unix-time (seconds since 1/1/1970)
        """
        days, secs = divmod(int(t), 86400)
        return Timestamp.fromstring(time.strftime('0000:%H:%M:%S',
                                                  time.gmtime(t)),
                                    epoch=days*86400)

    @staticmethod
    def fromstring(tstr, epoch=0):
        """
        Create a Timestamp from a Seascan time string, DDDD:HH:MM:SS
        """
        args = [int(s) for s in tstr.split(':')]
        d = {'epoch': epoch}
        return Timestamp(*args, **d)

    def __repr__(self):
        return 'Timestamp(%d, %d, %d, %d, epoch=%d)' % (self.days, self.hours,
                                                        self.minutes,
                                                        self.seconds,
                                                        self.epoch)

    def __str__(self):
        return '%04d:%02d:%02d:%02d' % (self.days, self.hours,
                                        self.minutes, self.seconds)

    def tounixtime(self):
        return self.epoch + self.seconds + \
            60*(self.minutes + 60*(self.hours + 24*self.days))


class Clock(object):
    """
    Class to provide access to a Seascan clock. This is a SAIL-bus device
    which is accessed through a serial port. This class allows a user to
    perform the following functions:

        - wake the clock from low-power sleep
        - address the clock (allows it to use the SAIL-bus)
        - set the time
        - read the time
        - place the clock into low-power sleep

    """
    epoch_file = '/etc/seascan_epoch'
    lock_file = '/var/lock/seascan.lock'

    def __init__(self, device, id_num, wakeup_line):
        """
        Instance initializer.

        @param device: serial device
        @param id_num: clock's SAIL-bus ID number
        @param wakeup_line: GPIO line used to wake the clock
        """
        self.lfile = open(self.lock_file, 'w')
        fcntl.flock(self.lfile, fcntl.LOCK_EX)
        self.port = serial.Serial(device, 9600, timeout=8)
        self.addr = '#CK%02d?ID\r' % id_num
        self.wakeup_gpio = Gpio(wakeup_line)
        self.active = False

    def __del__(self):
        self.port.close()
        fcntl.flock(self.lfile, fcntl.LOCK_UN)
        self.lfile.close()

    def _write_epoch(self, epoch):
        open(self.epoch_file, 'w').write('%d\n' % epoch)

    def _read_epoch(self):
        try:
            contents = open(self.epoch_file, 'r').read()
            return int(contents.strip())
        except IOError:
            return 0

    def _wait_for_char(self, c=0x03):
        """
        Wait for the specified character or timeout. The returned tuple
        contains the status (False on timeout), a string containing all of
        the characters returned from the clock, and the timestamp of the
        last character received.

        :param c: character to wait for
        :type c: int
        :return: status, buffer, timestamp
        :rtype: boolean, string, float
        """
        buf = array('B')
        while True:
            data = self.port.read(1)
            t = time.time()
            if not data:
                return False, buf.tostring(), t
            data = ord(data) & 0x7f
            if data == c:
                return True, buf.tostring(), t
            buf.append(data)

    def _purge(self):
        """
        Purge the serial interface
        """
        timeout = self.port.timeout
        self.port.timeout = 1
        while True:
            data = self.port.read(1)
            if not data:
                break
        self.port.timeout = timeout

    def wakeup(self, count=3):
        """
        Wake the clock from low-power mode.

        @param count: number of pulses to send.
        """
        for i in range(count):
            self.wakeup_gpio.set()
            time.sleep(10e-3)
            self.wakeup_gpio.clear(force=True)
            time.sleep(10e-3)
        time.sleep(1.5)

    def activate(self):
        """
        Address the clock so it can use the SAIL bus.

        @return: True if successful, otherwise False
        """
        self.port.write(self.addr)
        self.active, buf, tchar = self._wait_for_char()
        return self.active

    def get_time(self):
        """
        Read the current time.

        @return: Timestamp or None
        """
        self._purge()
        tstamp = None
        self.port.write('?T\r')
        status, buf, tchar = self._wait_for_char(ord('@'))
        # The returned timestamp string is at the very end of the buffer and is
        # thirteen characters long.
        if status:
            tstamp = Timestamp.fromstring(buf[-13:], epoch=self._read_epoch())
        return tstamp, tchar

    def set_time(self):
        """
        Set the clock. Because the clock does not store absolute time, we
        write an 'epoch' timestamp to a file. This epoch is the starting
        second of the current day (UTC). Upon return from this method, the
        clock will be 'de-addressed'.
        """
        self._purge()
        t = int(time.time()) + 5
        tstamp = Timestamp.fromunixtime(t)
        self._write_epoch(tstamp.epoch)
        self.port.write('!T' + str(tstamp))
        while time.time() < t:
            pass
        self.port.write('@')
        self.active = False
        return tstamp

    def unlock(self):
        """
        Send an unlock command. This command must be sent before setting the
        time.
        """
        self._purge()
        self.port.write('!U\r')
        status, buf, tchar = self._wait_for_char()
        return status

    def deactivate(self):
        """
        Put the clock into low-power mode.
        """
        if not self.active:
            return
        self._purge()
        self.port.write('!D\r')
        status, buf, tchar = self._wait_for_char()
        if status:
            self.active = False
        return status

if __name__ == '__main__':
    import sys
    try:
        op = sys.argv[1]
    except IndexError:
        op = 'get_time'
    clk = Clock('/dev/ttyS2', 62, 76)
    clk.wakeup()
    if not clk.activate():
        print 'Cannot address clock'
        sys.exit(1)
    func = getattr(clk, op.lower())
    clk.unlock()
    if func:
        print repr(func())
        clk.deactivate()
