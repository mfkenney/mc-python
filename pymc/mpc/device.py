#!/usr/bin/env python
#
from array import array
from protocol import *
import serial
import logging
import time
import os

class CommTimeout(Exception):
    pass

class ImmError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return '<ImmError %s>' % repr(self.msg)
    
class IMM(object):
    """
    Interface to a Seabird Inductive Modem Module.

    Note that the IMM has a configurable prompt (so it can emulate the older
    SIM), this class requires that the prompt be set to IMM>

    @ivar port: serial port object which must provide the methods; C{read},
                C{write}, and C{readline} along with the attribute C{timeout}.
    """
    init_commands = ["",
                     "setdebuglevel=0",
                     "SETEnableHostWakeupCR=0",
                     "SETTermToHost=254",
                     "SETEnableFullPwrTX=1"]
    prompt = 'IMM>'
    eol = '\r\n'
    acquire_delay = 1
    
    def __init__(self, port):
        self.port = port
        self.logger = logging.getLogger('imm')
        self.t_in = 0

    def configure(self, commands=[]):
        if not commands:
            commands = self.init_commands
        for cmd in commands:
            self.logger.debug('Sending: %s', repr(cmd))
            self.port.write(cmd + self.eol)
            status, resp = self.waitfor(self.prompt)
            if not status:
                raise CommTimeout
        self.release_line()
        
    def __enter__(self):
        try:
            self.acquire_line()
            time.sleep(self.acquire_delay)
        except:
            self._purge()
            self.release_line()
            raise

    def __exit__(self, *args):
        self.release_line()
        return False

    def __getattr__(self, name):
        return getattr(self.port, name)
    
    def _purge(self):
        """
        Purge the serial interface
        """
        timeout = self.port.timeout
        self.port.timeout = 1
        while True:
            data = self.port.read(1)
            if not data:
                break
        self.port.timeout = timeout

    def wakeup(self):
        self.port.write('\r')
        status, resp = self.waitfor(self.prompt)
        if not status:
            raise CommTimeout
        if 'Error' in resp:
            raise ImmError(text)
        
    def waitfor(self, match):
        buf = array('c')
        match = array('c', match)
        n = len(match)
        while True:
            data = self.port.read(1)
            if not data:
                return False, buf.tostring()
            buf.append(data)
            if buf[-n:] == match:
                return True, buf.tostring()

    def readline(self):
        line = self.port.readline()
        self.t_in = time.time()
        return line
    
    def release_line(self):
        self.port.write('pwroff' + self.eol)
        status, resp = self.waitfor(self.eol[-1])
        if not status:
            raise CommTimeout
        if 'Error' in resp:
            raise ImmError(resp)
        self._purge()
        self.logger.info('Line released')

    def acquire_line(self):
        """
        Wake the IMM and capture the line.
        """
        self._purge()
        self.wakeup()
        self.port.write('forcecaptureline' + self.eol)
        status, resp = self.waitfor('>')
        if not status:
            raise CommTimeout
        if 'Error' in resp:
            raise ImmError(resp)
        self._purge()
        self.logger.info('Line acquired')

    def write(self, data):
        self.logger.debug('WRITE: %s', repr(data))
        self.port.write('#G0:%s%s' % (data, self.eol))
        status, text = self.waitfor(self.prompt)
        if not status:
            raise CommTimeout
        if 'Error' in text:
            raise ImmError(text)


@coroutine
def _linker(outbox):
    """
    Coroutine to link data files into an outbox directory.
    """
    logger = logging.getLogger('linker')
    while True:
        path = (yield)
        try:
            if os.path.isfile(path):
                dest = os.path.join(outbox, os.path.basename(path))
                if os.path.isfile(dest):
                    os.remove(dest)
                os.link(path, dest)
                logger.info('Copied %s to OUTBOX', path)
        except os.error, e:
            logger.critical('Error linking %s into outbox (%s)' % (path, str(e)))


def MPCService(imm, config):
    """
    Return a coroutine to process the MPC data stream.
    """
    outbox = config.get('outbox', None)
    if outbox:
        t_link = _linker(outbox)
    else:
        t_link = None
    t_respond = mpc_write_output(imm)
    t_store = mpc_data_splitter(config['archiver_service'],
                                config['data_dir_template'],
                                target=t_link,
                                store_bb2f=config['store_bb2f'])
    t_log_msg = mpc_msg_logger(config['msg_log_template'], target=t_link)
    t_eng_pkt = mpc_pkt_logger(config['eng_file_template'], target=t_link, strip_type=True)
    t_accel_pkt = mpc_pkt_logger(config['accel_file_template'], target=t_link, strip_type=True)
    t_sync = mpc_clocksync(t_respond)
    t_profile = mpc_profile([t_store, t_eng_pkt, t_accel_pkt])
    t_msg = mpc_msg_dispatcher([('MPCCLK', t_sync),
                                ('*', t_profile),
                                ('*', t_log_msg)] + config.get('msg_dispatch', []))
    t_pkt = mpc_pkt_dispatcher([(0x80, t_eng_pkt),
                                (0x40, t_accel_pkt),
                                (0x0f, t_store)] + config.get('pkt_dispatch', []))
    t_err = mpc_err_logger()
    return mpc_dispatcher(t_msg, t_pkt, t_err)
