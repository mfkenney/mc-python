#!/usr/bin/env python
#
# Various constants.
#
try:
    import json
except ImportError:
    import simplejson as json

MPC_METADATA = json.dumps({'name': 'mpc/1',
                           'description': 'Mooring Profiler Controller',
                           'manufacturer': 'APL',
                           'precision': { 'pressure': '0.01' },
                           'variables': ['batt_status',
                                         'mpc_time',
                                         'pressure',
                                         'profile'],
                           'units': {'mpc_time': 'seconds',
                                     'batt_status': 'volts*10',
                                     'pressure': 'decibars',
                                     'profile': ''}})


MPC_FORMAT = '>BLfL'
# Start, end bytes within the packet. The profile number is not
# included in the packet, it is added by the local processing.
MPC_RANGE = 1, 10

BB2F_VARS = ['blue_ref', 'blue',
             'red_ref', 'red',
             'chlorophyll', 'temperature']
BB2F_METADATA = json.dumps({'name': 'bb2f/1',
                            'description': 'Scattering meter and fluorometer',
                            'manufacturer': 'WET Labs Inc.',
                            'variables': BB2F_VARS,
                            'units': dict(zip(BB2F_VARS, ['counts'] * 6))})
BB2F_FORMAT = '>6h'
BB2F_RANGE = 10, 22

PRESSURE_AVAIL = 0x01

ACM_AVAIL = 0x04
ACM_RANGE = 22, 58

CTD_AVAIL = 0x08
CTD_RANGE = 58, 80
