#!/usr/bin/python
#
# arch-tag: dc68b65d-a433-49d8-9c1c-a8d84d8edebd
# $Id: imcodec.py 851 2008-06-05 20:27:50Z mike $
#
# A standard Python Codec to encode/decode a binary string
# for transmitting through a Seabird Inductive Modem.
#
"""
A standard Python Codec to encode/decode a binary string
for transmitting through a Seabird Inductive Modem.
"""
import codecs

def to_ord(elems):
    """Convert a list of ASCII characters to 8-bit integers"""
    return [ord(e) for e in elems]

def to_chr(elems):
    """Convert a list of 8-bit integers to ASCII characters"""
    return [chr(e) for e in elems]

def _encode_block(block):
    b = 0
    for i in range(7):
        if (block[i] & 0x80):
            b = b + (1 << i)
        block[i] |= 0x80
    return block + [b|0x80]

def _decode_block(block):
    b = block[7]
    for i in range(7):
        if not (b & (1 << i)):
            block[i] = block[i] ^ 0x80
    return block[0:7]

def im_encode(input, errors='strict'):
    """
    Encode a binary string for the IM.

    The Seabird Inductive Modem is not 8-bit clean. To avoid having to
    escape all byte values < 0x20, the following encoding scheme was
    devised.
    
      - the data string is broken into 7-byte blocks
      - the high-bit from each byte is stored in the corresponding bit
        position of a 'tag' byte.
      - the tag byte is appended to form an 8-byte block.
      - the high bit is set on all 8 bytes.
     
    All bytes in the encoded block are > 0x80 and will pass through the
    modem cleanly. Note that if the length of the input string is not
    a multiple of 7, the remainder bytes will not be encoded.

    @param input: string to encode.

    @return: C{tuple} of I{encoded string}, I{number of bytes encoded}.
    """
    assert errors == 'strict'
    blocks = len(input)/7
    n = blocks*7
    out = []
    for i in range(0, n, 7):
        out.extend(_encode_block(to_ord(input[i:i+7])))
    return ''.join(to_chr(out)), n

def im_decode(input, errors='strict'):
    """
    Decode the IM-encoded data blocks.

    Decode the binary data which was encoded to pass through the Inductive
    Modem. Only the lower 7-bits of each byte could be used and the high bit
    is always set.The data are divided into 8-byte blocks where the 8th byte
    contains the high-order bits for the previous seven bytes

    @param input: string to decode.

    @return: C{tuple} of I{decoded string}, I{number of bytes decoded}.
    """
    assert errors == 'strict'
    blocks = len(input)/8
    n = blocks*8
    out = []
    for i in range(0, n, 8):
        out.extend(_decode_block(to_ord(input[i:i+8])))
    return ''.join(to_chr(out)), n

class IMCodec(codecs.Codec):
    def encode(self, input, errors='strict'):
        return im_encode(input, errors)

    def decode(self, input, errors='strict'):
        return im_decode(input, errors)

class IMStreamWriter(IMCodec):
    def __init__(self, stream, errors='strict'):
        self.stream = stream
        self.errors = errors
        self.buf = ''
        
    def write(self, s):
        ss = self.buf + s
        output, n = self.encode(ss)
        self.buf = ss[n:]
        self.stream.write(output)

    def writelines(self, lines):
        for line in lines:
            self.write(line)

    def reset(self):
        pad = 7 - len(self.buf)
        self.write('\x00' * pad)

    def flush(self):
        self.reset()
        self.stream.flush()
        
    def __getattr__(self, name):
        return getattr(self.stream, name)
    
        
class IMStreamReader(IMCodec,codecs.StreamReader):
    pass

def search(encoding):
    if encoding == 'im':
        return (im_encode, im_decode, IMStreamReader, IMStreamWriter)
    else:
        return None
    
codecs.register(search)
