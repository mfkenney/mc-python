#!/usr/bin/env python
#
# Coroutines to manange to communication protocol with the MPC. The protocol is described in
# detail at http://aloha.apl.washington.edu/wiki/index.php/MPC_to/from_Shore_Server
#
from __future__ import with_statement
import time
import os
import struct
import logging
import imcodec
import sys
import traceback
import dbus
from array import array
from reedsolomon import Codec, UncorrectableError
from pymc import nmea
from settings import *

def _expand_path(template, *args):
    """
    Use strftime formatting followed by standard string formatting to
    expand a file path template. The tuple *args is passed to the standard
    string formatting operation.
    """
    path = time.strftime(template, time.gmtime())
    if args and ('%' in path):
        path = path % args
    return path

def coroutine(func):
    def start(*args,**kwargs):
        cr = func(*args,**kwargs)
        cr.next()
        return cr
    return start

def mpc_read_input(port, handler):
    """
    Head of the dataflow pipeline. Reads lines (records) of data from the
    input port and passes them to the handler.
    """
    while True:
        line = self.port.readline()
        if line:
            handler.send(line.rstrip())

@coroutine
def mpc_dispatcher(msg_handler, pkt_handler, err_handler):
    """
    Dispatch the input records to either the packet or message handler.
    """
    codec = Codec(96, 80)
    logger = logging.getLogger('mpc')
    while True:
        record = (yield)
        logger.debug('RECORD: ' + repr(record))
        try:
            if record.startswith('$'):
                msg_handler.send(nmea.Sentence(record))
            else:
                ecc_block = record.decode('im')
                block, errors = codec.decode(ecc_block[0:96])
                if errors:
                    err_handler.send('%d corrected packet errors' % len(errors))
                pkt_handler.send(block)
        except ValueError:
            pass   # Ignore packet record < 96 bytes in length
        except (nmea.BadFormat, nmea.ChecksumError):
            err_handler.send('Bad message: %s' % record)
        except UncorrectableError:
            err_handler.send('Uncorrectable packet errors')
        except:
            traceback.print_exc(file=sys.stdout)
            err_handler.send('Error in record decoding')

@coroutine
def mpc_pkt_dispatcher(handlers):
    """
    Dispatch a data packet to a registered handler based on the packet
    type. Handlers is a list of tuples of the form:

      TYPEMASK, TASK

    TYPEMASK is a bitmask which specifies which packet types TASK (a coroutine)
    is expecting. The message sent to TASK is the tuple: ('data', PACKET)
    """
    while True:
        pkt = (yield)
        pkt_type = ord(pkt[0])
        for typecode, target in handlers:
            if (pkt_type & typecode):
                target.send(('data', pkt))

@coroutine
def mpc_msg_dispatcher(handlers):
    """
    Dispatch an NMEA message to a registered handler based on the message ID (first
    field). Handlers is a list of tuples of the form:

      ID, TASK

    ID is a message ID string or '*' which means 'all messages'.
    """
    while True:
        msg = (yield)
        for cmd, target in handlers:
            if cmd == '*' or msg.id == cmd:
                target.send(msg)

@coroutine
def mpc_err_logger():
    logger = logging.getLogger('mpc')
    while True:
        text = (yield)
        logger.critical(text)

@coroutine
def mpc_pkt_logger(path_tmpl, target=None, strip_type=False):
    """
    Log data packets to a series of files. A new file is created at the start
    of each profile. Path_tmpl is an strftime(3) compatible format string. In
    addition to the standard strftime substitutions, the format specifier %%d
    is replaced with the current profile number.
    """
    ofile = None
    while True:
        tag, contents = (yield)
        if tag == 'data':
            if ofile:
                if strip_type:
                    ofile.write(contents[1:])
                else:
                    ofile.write(contents)
                ofile.flush()
        elif tag == 'start':
            profile = int(contents)
            path = _expand_path(path_tmpl, profile)
            try:
                os.makedirs(os.path.dirname(path))
            except os.error:
                pass
            if target and ofile:
                target.send(ofile.name)
            ofile = open(path, 'wb')

@coroutine
def mpc_clocksync(target):
    logger = logging.getLogger('mpc')
    while True:
        msg = (yield)
        try:
            msg[1] = int(time.time()+1)
        except IndexError:
            logger.critical('Malformed clock-sync message: %s', str(msg))
        else:
            target.send(str(msg))
            logger.info('Clock sync')

@coroutine
def mpc_msg_logger(path_tmpl, target=None):
    """
    Log NMEA messages to a series of files. A new file is created at the start
    of each profile. Path_tmpl is an strftime(3) compatible format string. In
    addition to the standard strftime substitutions, the format specifier %%d
    is replaced with the current profile number.
    """
    ofile = None
    while True:
        msg = (yield)
        if msg.id == 'MPCPSTART':
            path = _expand_path(path_tmpl, int(msg[1]))
            try:
                os.makedirs(os.path.dirname(path))
            except os.error:
                pass
            if target and ofile:
                target.send(ofile.name)
            ofile = open(path, 'w')
        if ofile:
            ofile.write('%.6f %s\n' % (time.time(), str(msg)))
            ofile.flush()

@coroutine
def mpc_profile(targets):
    """
    Send messages indicating the start and end of a profile. Targets is a
    list of coroutines which will receive message tuples of the form:

      'start', PROFILENUM
      'end', None

    """
    logger = logging.getLogger('mpc')
    while True:
        msg = (yield)
        if msg.id == 'MPCPSTART':
            pnum = int(msg[1])
            for target in targets:
                target.send(('start', pnum))
            logger.info('Starting profile %d', pnum)
        elif msg.id == 'MPCPEND':
            for target in targets:
                target.send(('end', None))
            logger.info('Profile done')

@coroutine
def mpc_write_output(port):
    """
    Write all recieved messages to the MPC
    """
    logger = logging.getLogger('mpc')
    while True:
        record = (yield)
        # We need to acquire the line before sending
        try:
            with port:
                port.write(record)
        except Exception, e:
            logger.critical('Cannot acquire line (%s)', str(e))

@coroutine
def mpc_data_splitter(archiver_svc, dir_tmpl, target=None, store_bb2f=False):
    """
    Split a standard data packet into an MMP section and an MPC section. The
    MPC section is sent to the Archiver which handles the data from the other
    mooring sensors while the MMP data is stored in files.
    """
    def close_file(f):
        if f:
            if target:
                target.send(f.name)
            f.close()
    ctdfile, acmfile = None, None
    archiver = archiver_svc()
    archiver.addMetadata('mpc/1', MPC_METADATA)
    if store_bb2f:
        archiver.addMetadata('bb2f/1', BB2F_METADATA)
    N = 0
    profile = struct.pack('>L', N)
    while True:
        tag, contents = (yield)
        t = time.time()
        secs = int(t)
        usecs = (t - secs)*1e6
        if tag == 'start':
            N = int(contents)
            # MMP data is from the previous profile, N-1.
            dirname = _expand_path(dir_tmpl)
            try:
                os.makedirs(dirname)
            except os.error:
                pass
            close_file(ctdfile)
            close_file(acmfile)
            ctdfile = open(os.path.join(dirname, 'c%07d.dat' % (N-1,)), 'wb')
            acmfile = open(os.path.join(dirname, 'a%07d.dat' % (N-1,)), 'wb')
            # Pack profile number into a long integer which will be appended
            # to the archived MPC "data record"
            profile = struct.pack('>L', N)
        elif tag == 'end':
            close_file(ctdfile)
            close_file(acmfile)
            acmfile, ctdfile = None, None
        elif tag == 'data':
            pkttype = ord(contents[0])
            if acmfile and (pkttype & ACM_AVAIL):
                acmfile.write(contents[ACM_RANGE[0]:ACM_RANGE[1]])
            if ctdfile and (pkttype & CTD_AVAIL):
                ctdfile.write(contents[CTD_RANGE[0]:CTD_RANGE[1]])
            try:
                if pkttype & PRESSURE_AVAIL:
                    archiver.addRecord('mpc/1', secs, usecs, MPC_FORMAT,
                                       contents[MPC_RANGE[0]:MPC_RANGE[1]]+profile)
                if store_bb2f:
                    archiver.addRecord('bb2f/1', secs, usecs, BB2F_FORMAT,
                                       contents[BB2F_RANGE[0]:BB2F_RANGE[1]])
            except dbus.exceptions.DBusException:
                archiver = archiver_svc()
