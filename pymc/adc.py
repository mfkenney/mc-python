#!/usr/bin/env python
#
"""
.. :module:: adc
    :synopsis: interface to the A/D converter.
"""
from __future__ import print_function
import os
import subprocess
from decimal import Decimal


# Path to the adread executable
ADREAD_PROG = os.path.expanduser('~/bin/adread')


class Calibration(object):
    """
    Calibration coefficients for an A/D channel. Used to convert from
    input voltage to engineering units. The equation is of the form::

    .. math::

        y = C_0 + C_1x + C_2x^2 + ... + C_{N-1}x^{N-1}

    >>> x = 2
    >>> c = Calibration([0, 2], None)
    >>> c(x)
    (4, None)
    >>> c = Calibration([1, 2, 3], None)
    >>> c(x)
    (17, None)
    """
    def __init__(self, coeffs, units):
        """
        Instance initializer.

        :param coeffs: list of equation coefficients
        :param units: engineering units
        :type units: string
        """
        assert isinstance(coeffs, list)
        self.C = coeffs[:]
        self.C.reverse()
        self.units = units

    def __call__(self, x):
        """
        Apply the calibration equation to the input value.

        :return: a tuple of the converted value and the units
        """
        y = reduce(lambda a, b: a*x + b, self.C, 0)
        return y, self.units


def adread(n, interval='250ms'):
    """
    Read N samples from all A/D channels and return
    the average.
    """
    def add_lists(x, y):
        return [e[0]+e[1] for e in zip(x, y)]
    proc = subprocess.Popen([ADREAD_PROG, '-interval={0}'.format(interval)],
                            stdout=subprocess.PIPE,
                            stderr=open(os.devnull, 'w'),
                            bufsize=1)
    lines = []
    i = n
    first = True
    while i > 0:
        rec = proc.stdout.readline().rstrip().split(',')
        if not first:
            lines.append([Decimal(e) for e in rec])
            i = i - 1
        else:
            first = False
    proc.terminate()
    # Save time-stamp from first record
    t = lines[0][0] + lines[0][1] / 1000000
    accum = reduce(add_lists, [l[2:] for l in lines])
    return round(float(t), 6), map(lambda x: x / n, accum)


if __name__ == '__main__':
    v = adread(10)
    print(*v)
