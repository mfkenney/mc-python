#!/usr/bin/env python
#
"""
.. module:: dio
   :synopsis: interface to the DIO registers of Technologic Systems
              CPU board.
"""
import logging
import subprocess


def peekpoke(bits, addr, value=None):
    if value is not None:
        cmd = ['sudo', 'peekpoke', str(bits), str(addr), str(value)]
    else:
        cmd = ['sudo', 'peekpoke', str(bits), str(addr)]
    try:
        rval = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
        return int(rval, 0)
    except Exception:
        logging.exception('Cannot execute peekpoke 0x%08x', addr)


class MemoryMap(object):
    def __init__(self, bits=16, func=None):
        self.bits = bits
        self.func = func
        self.contents = {}

    def __setitem__(self, addr, val):
        if self.func:
            self.func(self.bits, addr, val)
        self.contents[addr] = val

    def __getitem__(self, addr):
        if self.func:
            return self.func(self.bits, addr)
        else:
            return self.contents.get(addr, 0)


class DioPort(object):
    """
    An 8-bit DIO port on a Technologic Systems TS-SOCKET CPU and
    baseboard combination.
    """
    def __init__(self, regbase, muxbus_cfg, dir_reg=0x0a,
                 out_reg=0x06, in_reg=0x0e, mm=None):
        self.regbase = regbase
        self.muxbus_cfg = muxbus_cfg
        self.dir_reg = dir_reg
        self.out_reg = out_reg
        self.in_reg = in_reg
        self.mm = mm or MemoryMap(func=peekpoke)
        # Set MUXBUS configuration register
        self.mm[self.muxbus_cfg] = 0x181
        self._outputs = self.mm[self.regbase + self.dir_reg]

    @property
    def outputs(self):
        return self._outputs

    @outputs.setter
    def outputs(self, value):
        self.mm[self.regbase + self.dir_reg] = value
        self._outputs = value

    def set(self, mask):
        """
        Set one or more output lines in the DIO port. Only output
        lines may be set.

        :param mask: lines to be set
        """
        mask &= self._outputs
        addr = self.regbase + self.out_reg
        val = self.mm[addr]
        val |= mask
        self.mm[addr] = val

    def clear(self, mask):
        """
        Clear one or more output lines in the DIO port. Only output
        lines may be cleared.

        :param mask: lines to be cleared
        """
        mask &= self._outputs
        addr = self.regbase + self.out_reg
        val = self.mm[addr]
        val &= ~mask
        self.mm[addr] = val

    def test(self, line):
        """
        Test an output line.

        :return: ``True`` or ``False``
        """
        mask = 1 << line
        addr = self.regbase + self.out_reg
        return (self.mm[addr] & mask) == mask

    def __int__(self):
        val = self.mm[self.regbase + self.in_reg]
        return val | self.mm[self.regbase + self.out_reg]


def Ts4200LcdDio(outputs=0xff, mm=None):
    """
    LCD DIO port on the TS-4200 (with 8160 baseboard)
    """
    port = DioPort(0x30000400, 0x30000020, mm=mm)
    port.outputs = outputs
    return port


def Ts4800LcdDio(outputs=0x07, mm=None):
    """
    LCD DIO port on the TS-4800 (with 8100 baseboard)
    """
    port = DioPort(0xb0011000, 0xb0010012, mm=mm)
    port.outputs = outputs
    return port


def Ts4800Dio(outputs=0xff, mm=None):
    """
    DIO on the TS-4800 (with 8100 baseboard)
    """
    port = DioPort(0xb0011000, 0xb0010012, dir_reg=0x08,
                   out_reg=0x04, in_reg=0x0c, mm=mm)
    port.outputs = outputs
    return port
