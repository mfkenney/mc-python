#!/usr/bin/env python
"""
Manage the sensor data archive.
"""
import sys
import os
import hashlib
import simplejson as json
import time
import logging
import argparse
import signal
import msgpack
from decimal import Decimal
from Queue import Queue, Empty
from functools import partial
import paho.mqtt.client as mqtt
from pymc import __version__


def metadata_uid(m):
    h = hashlib.sha1()
    h.update(m)
    return h.digest()


def signal_handler(signum, frame):
    sys.exit(0)


def mp_decoder(obj):
    if b'__decimal__' in obj:
        obj = Decimal(obj['s'])
    return obj


class Archive(object):
    """
    Class to manage a JSON data archive file.

    :ivar ardir: archive directory path
    :ivar t_rollover: file roll-over interval in seconds
    :ivar ftmpl: filename template in :func:`time.strftime` format
    """

    def __init__(self, ardir, ftmpl, t_rollover=86400, logger=None,
                 on_close=None):
        """
        Instance initializer.

        :param ardir:
        :param ftmpl:
        :param t_rollover:
        :return:
        """
        self.ardir = ardir
        self.ftmpl = ftmpl
        self.t_rollover = t_rollover
        self.timestamp = 0, 0
        self._file = None
        if not os.path.isdir(self.ardir):
            os.makedirs(self.ardir)
        self.logger = logger or logging.getLogger()
        self.on_close = on_close
        self.mdcache = {}

    def _openfile(self):
        """
        Open the current archive file.

        :return: file object
        """
        t = divmod(int(time.time()), self.t_rollover)
        if (self._file is None) or (t[0] > self.timestamp[0]):
            fname = time.strftime(self.ftmpl, time.gmtime())
            self._closefile()
            pathname = os.path.join(self.ardir, fname)
            dname = os.path.dirname(pathname)
            if not os.path.isdir(dname):
                os.makedirs(dname)
            # If file exists, open in append mode. Otherwise open for writing
            # and add the metadata that we have cached.
            if os.path.isfile(pathname):
                self._file = open(pathname, 'a')
            else:
                self._file = open(pathname, 'w')
                for uid, m in self.mdcache.values():
                    self._file.write(json.dumps(m) + '\n')
            self.logger.info('Opened archive file %s', pathname)
            self.timestamp = t
        return self._file

    def _closefile(self):
        if self._file:
            self.logger.info('Closing %s', self._file.name)
            self._file.close()
            if self.on_close:
                self.on_close(self._file.name)
            self._file = None

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self._closefile()
        return False

    def add_record(self, name, secs, usecs, data):
        d = {'sensor': name, 'clock': [secs, usecs], 'data': data}
        ofp = self._openfile()
        ofp.write(json.dumps(d) + '\n')
        ofp.flush()

    def add_metadata(self, name, md):
        uid = metadata_uid(str(md))
        try:
            # Don't write the metadata unless it has changed
            old_uid, _ = self.mdcache[name]
            if old_uid == uid:
                return
        except KeyError:
            pass
        self.mdcache[name] = uid, md
        ofp = self._openfile()
        ofp.write(json.dumps(md) + '\n')
        ofp.flush()


def on_connect(client, udata, flags, rc):
    if rc == 0:
        lgr = logging.getLogger()
        lgr.info('Connected to message broker')
        client.subscribe('sensors/#')
    else:
        raise EnvironmentError((rc, 'Cannot connect to server'))


def on_message(client, udata, msg):
    assert isinstance(udata, Queue)
    udata.put((msg.topic, msg.payload))


def link_file(dest, src):
    link = os.path.join(dest, os.path.basename(src))
    try:
        if os.path.isfile(link):
            os.remove(link)
        os.link(src, link)
        logging.info('Link %s into %s', src, dest)
    except Exception:
        logging.exception('Link failed')


def make_linker(d):
    if not os.path.isdir(d):
        msg = 'Directory {} does not exist'.format(d)
        raise argparse.ArgumentTypeError(msg)
    return partial(link_file, d)


def q_get(q):
    """
    Work-around a shortcoming in the Python 2 Queue implementation
    that disables signals in a blocking call to Queue.get().
    """
    while True:
        try:
            return q.get(timeout=300)
        except Empty:
            pass


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('dir', help='archive directory')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    parser.add_argument('--host',
                        default='localhost',
                        help='MQTT host (%(default)s)')
    parser.add_argument('--port',
                        type=int,
                        default=1883,
                        help='MQTT TCP port (%(default)d)')
    parser.add_argument('--tmpl', '-t',
                        default='data-%Y%m%d.json',
                        help='file-name template (%(default)s)')
    parser.add_argument('--rollover', '-r',
                        metavar='SECS',
                        type=int,
                        default=86400,
                        help='archive roll-over time in secs (%(default)d)')
    parser.add_argument('--link', '-l',
                        metavar='DIR',
                        type=make_linker,
                        help='link each archive file into DIR when closed')
    parser.add_argument('-V', '--version',
                        action='version',
                        version='pymc {0}'.format(__version__))

    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    q = Queue()
    mb = mqtt.Client(client_id='data-archiver',
                     userdata=q,
                     protocol=mqtt.MQTTv31,
                     clean_session=False)
    mb.on_connect = on_connect
    mb.on_message = on_message

    signal.signal(signal.SIGTERM, signal_handler)

    mb.connect(args.host, args.port)
    mb.loop_start()

    try:
        with Archive(args.dir, args.tmpl, t_rollover=args.rollover,
                     on_close=args.link) as ar:
            while True:
                topic, payload = q_get(q)
                _, mtype, name = topic.split('/', 2)
                logging.debug('%s> %r', name, payload)
                if payload:
                    if mtype == 'data':
                        t_sec, t_usec, data = msgpack.unpackb(
                            payload, use_list=False, object_hook=mp_decoder)
                        ar.add_record(name, t_sec, t_usec, data)
                    elif mtype == 'metadata':
                        ar.add_metadata(name, json.loads(payload))
    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Uncaught exception:')
    finally:
        mb.loop_stop()


if __name__ == '__main__':
    main()
