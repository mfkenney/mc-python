#!/usr/bin/env python
"""
Monitor sensor data output.
"""
from __future__ import print_function
import time
import simplejson as json
import logging
import argparse
import msgpack
from decimal import Decimal
from Queue import Queue, Empty
import paho.mqtt.client as mqtt
from pymc import __version__


def mp_decoder(obj):
    if b'__decimal__' in obj:
        obj = Decimal(obj['s'])
    return obj


def on_connect(client, udata, flags, rc):
    if rc == 0:
        lgr = logging.getLogger()
        lgr.info('Connected to message broker')
        client.subscribe([('sensors/#', 0), ('events/#', 0)])
    else:
        raise EnvironmentError((rc, 'Cannot connect to server'))


def on_message(client, udata, msg):
    assert isinstance(udata, Queue)
    udata.put((msg.topic, msg.payload))


def q_get(q):
    """
    Work-around a shortcoming in the Python 2 Queue implementation
    that disables signals in a blocking call to Queue.get().
    """
    while True:
        try:
            return q.get(timeout=300)
        except Empty:
            pass


def display_record(name, rec, md, prec):
    t_sec, t_usec, data = msgpack.unpackb(rec,
                                          use_list=False,
                                          object_hook=mp_decoder)
    print(name, end=' ')
    if md:
        fields = ['{}={}'.format(k, v) for k, v in zip(md['variables'], data)]
    else:
        fields = data
    print(*fields, end=' ')
    if prec == 'u':
        print(t_sec*1000000 + t_usec)
    elif prec == 'rfc':
        print(time.strftime('%Y%m%dT%H%M%SZ', time.gmtime(t_sec)))
    else:
        print(t_sec)


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--host',
                        default='localhost',
                        help='MQTT host (%(default)s)')
    parser.add_argument('--port',
                        type=int,
                        default=1883,
                        help='MQTT TCP port (%(default)d)')
    parser.add_argument('--precision',
                        choices=['s', 'u', 'rfc'],
                        default='s',
                        help='timestamp precision')
    parser.add_argument('-V', '--version',
                        action='version',
                        version='pymc {0}'.format(__version__))
    args = parser.parse_args()

    q = Queue()
    mb = mqtt.Client(client_id='sens-monitor',
                     userdata=q,
                     protocol=mqtt.MQTTv31,
                     clean_session=True)
    mb.on_connect = on_connect
    mb.on_message = on_message
    mb.connect(args.host, args.port)
    mb.loop_start()

    mdcache = {}
    try:
        while True:
            topic, payload = q_get(q)
            if topic.startswith('events'):
                print(topic, repr(payload))
            else:
                _, mtype, name = topic.split('/', 2)
                if payload:
                    if mtype == 'metadata':
                        mdcache[name] = json.loads(payload)
                    elif mtype == 'data':
                        display_record(name, payload,
                                       mdcache.get(name),
                                       args.precision)
    except KeyboardInterrupt:
        pass
    finally:
        mb.loop_stop()

if __name__ == '__main__':
    main()
