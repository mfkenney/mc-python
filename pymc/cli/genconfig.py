#!/usr/bin/env python
#
# Convert a sensor configuration file from YAML to a more compact form.
#
from __future__ import print_function
import sys
import os
import argparse
import cPickle as pickle
import yaml
from cerberus import Validator
try:
    import simplejson as json
except ImportError:
    import json
from pymc.sensors.device import sensor_config
from pymc import __version__


METADATA_KEYS = ('name', 'description', 'variables',
                 'units', 'precision', 'cal')

SCHEMA = {
    'name': {'type': 'string', 'required': True},
    'description': {'type': 'string'},
    'variables': {'type': 'list'},
    'units': {'type': 'dict'},
    'precision': {'type': 'dict'},
    'cal': {'type': 'dict'},
    'interface': {
        'type': 'dict',
        'required': True,
        'schema': {
            'module': {'type': 'string', 'required': True},
            'device': {'type': 'string'},
            'proxy': {'type': 'string'},
            'baud': {
                'type': 'integer',
                'required': False,
                'dependencies': ['^interface.device']
            },
            'eor': {'type': 'string'},
            'gpio': {'type': 'string'},
            'warmup': {'type': 'integer'},
            'timeout': {'type': 'integer'}
        }
    }
}


def read_single_file(f):
    """
    Iterate over all of the sensor definitions in a single
    YAML file.
    """
    for doc in yaml.load_all(f):
        if isinstance(doc, list):
            # Document defines multiple sensors
            for d in doc:
                yield d
        else:
            yield doc


def read_file_list(file_list):
    for filename in file_list:
        with open(filename, 'rb') as f:
            yield read_single_file(f)


def main(argv=None):
    """
    Generate a FHLOO sensor configuration file from YAML 'source'.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('infile',
                        help='input file name')
    parser.add_argument('outfile',
                        help='output file name')
    parser.add_argument('-V', '--version',
                        action='version',
                        version='pymc {0}'.format(__version__))
    parser.add_argument('-i', '--ignore-errors',
                        default=False,
                        action='store_true',
                        help='ignore configuration file errors')
    args = parser.parse_args(args=argv)

    if os.path.isdir(args.infile):
        print('Reading all YAML documents in {}'.format(args.infile))
        files = [os.path.join(args.infile, f)
                 for f in os.listdir(args.infile) if f.endswith('.yaml')]
        documents = read_file_list(files)
    else:
        documents = read_single_file(open(args.infile, 'rb'))

    desc = []
    v = Validator(SCHEMA)
    v.allow_unknown = True
    for doc in documents:
        if v.validate(doc):
            config, klass = sensor_config(doc)
            if klass:
                src = {k: doc.get(k) for k in METADATA_KEYS}
                config['source'] = json.dumps(src)
                desc.append((klass, config))
            else:
                print(doc.get('name'), 'No driver class found',
                      sep=':', file=sys.stderr)
                if not args.ignore_errors:
                    return 2
        else:
            print(doc.get('name'), v.errors,
                  sep=':', file=sys.stderr)
            if not args.ignore_errors:
                return 1

    with open(args.outfile, 'wb') as f:
        pickle.dump(desc, f, pickle.HIGHEST_PROTOCOL)

    return 0


if __name__ == '__main__':
    main()
