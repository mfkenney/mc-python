#!/usr/bin/env python
#
"""
Service to process raw GPS data records.
"""
import sys
import signal
import logging
import argparse
import time
import calendar
import base64
from decimal import Decimal
from threading import Event
from Queue import Queue, Empty
import msgpack
import simplejson as json
import paho.mqtt.client as mqtt
from pymc import __version__, nmea


gps_precision = Decimal('0.000001')
gps_metadata = {
    'description': 'Garmin 15H GPS converted output',
    'variables': ['time', 'latitude', 'longitude'],
    'units': {
        'time': 'seconds',
        'latitude': 'degrees',
        'longitude': 'degrees',
    },
    'precision': {
        'time': '1',
        'latitude': '0.000001',
        'longitude': '0.000001'
    }
}


def signal_handler(signum, frame):
    if signum == signal.SIGTERM:
        sys.exit(1)
    else:
        sys.exit(2)


class Messenger(object):
    """
    Use an MQTT Broker to send and receive messages.
    """
    def __init__(self, client, msg_queue, sub=None):
        self.client = client
        self.mq = msg_queue
        self.sub = sub
        self.client.on_connect = self.on_connect
        self.client.on_publish = self.on_publish
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.on_disconnect
        self.logger = logging.getLogger()
        self.connected = Event()
        self.published = Event()

    def on_publish(self, client, udata, mid):
        self.logger.debug('Message %r published', mid)
        self.published.set()

    def on_connect(self, client, udata, flags, rc):
        if rc == 0:
            if self.sub:
                self.client.subscribe(self.sub)
                self.logger.info('Subscribed to %r', self.sub)
            self.connected.set()

    def on_message(self, client, udata, msg):
        self.logger.debug('%s> %r', msg.topic, msg.payload)
        self.mq.put((msg.topic, msg.payload))

    def on_disconnect(self, client, udata, rc):
        self.connected.clear()

    def __call__(self, chan, msg, retain=False):
        status = self.connected.wait(5)
        if not status:
            self.logger.critical('Timeout connecting to server')
            raise IOError
        self.published.clear()
        topic = '/'.join(chan)
        self.logger.info('Publishing to %s', topic)
        self.client.publish(topic,
                            payload=bytearray(msg),
                            qos=1, retain=retain)
        status = self.published.wait(5)
        if not status:
            self.logger.critical('Message delivery timeout')


def mp_encoder(obj):
    """
    Encode a Decimal instance for MessagePack.
    """
    if isinstance(obj, Decimal):
        return {'__decimal__': True, 's': str(obj)}
    return obj


def mp_decoder(obj):
    if b'__decimal__' in obj:
        obj = Decimal(obj['s'])
    return obj


def q_get(q):
    """
    Work-around a shortcoming in the Python 2 Queue implementation
    that disables signals in a blocking call to Queue.get().
    """
    while True:
        try:
            return q.get(timeout=300)
        except Empty:
            pass


def convert_gps(contents, no_check=False):
    scale = Decimal('60')
    msg = base64.b64decode(contents)
    for s in [nmea.Sentence(l) for l in msg.split('\n')]:
        if s.id == 'GPRMC':
            if s[1] == 'A' or no_check:
                t = time.strptime(s[8]+s[0]+' UTC', '%d%m%y%H%M%S %Z')
                slat = s[2]
                lat = Decimal(slat[0:2]) + Decimal(slat[2:])/scale
                if s[3] == 'S':
                    lat = -lat
                slon = s[4]
                lon = Decimal(slon[0:3]) + Decimal(slon[3:])/scale
                if s[5] == 'W':
                    lon = -lon
                return [calendar.timegm(t),
                        lat.quantize(gps_precision),
                        lon.quantize(gps_precision)]
            else:
                logging.info('Skipping invalid GPS fix')
    return [0, Decimal(0), Decimal(0)]


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('raw', help='raw GPS sensor name')
    parser.add_argument('proc', help='processed GPS sensor name')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    parser.add_argument('--host',
                        default='localhost',
                        help='Message broker host (%(default)s)')
    parser.add_argument('--port', '-p',
                        type=int,
                        default=1883,
                        help='Message broker port (%(default)d)')
    parser.add_argument('-V', '--version',
                        action='version',
                        version='pymc {0}'.format(__version__))
    args = parser.parse_args()

    gps_metadata['name'] = args.proc

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    mb = mqtt.Client(client_id='gps-convert',
                     protocol=mqtt.MQTTv31)
    q = Queue()
    m = Messenger(mb, q, 'sensors/data/{}'.format(args.raw))
    mb.connect(args.host, args.port)
    mb.loop_start()

    try:
        while True:
            _, payload = q_get(q)
            t_sec, t_usec, data = msgpack.unpackb(
                payload, use_list=False, object_hook=mp_decoder)
            m(['sensors', 'metadata', gps_metadata['name']],
              json.dumps(gps_metadata))
            try:
                result = convert_gps(data)
            except Exception:
                logging.exception('Cannot convert data')
                result = [0, Decimal(0), Decimal(0)]
            m(['sensors', 'data', gps_metadata['name']],
              msgpack.packb([t_sec, t_usec, result],
                            default=mp_encoder))
            m(['events', 'clockcheck'], str(result[0] - t_sec))
    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Uncaught exception')
    finally:
        mb.loop_stop()


if __name__ == '__main__':
    main()
