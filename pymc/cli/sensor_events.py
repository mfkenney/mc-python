#!/usr/bin/env python
"""
Wait for one or more sensor events.
"""
import sys
import argparse
import signal
import logging
from Queue import Queue, Empty
import paho.mqtt.client as mqtt
from pymc import __version__


def signal_handler(signum, frame):
    if signum == signal.SIGTERM:
        sys.exit(1)
    else:
        sys.exit(2)


def on_connect(client, udata, flags, rc):
    if rc == 0:
        client.subscribe('events/#', qos=2)


def on_message(client, udata, msg):
    assert isinstance(udata, Queue)
    udata.put((msg.topic, msg.payload))


def q_get(q):
    """
    Work-around a shortcoming in the Python 2 Queue implementation
    that disables signals in a blocking call to Queue.get().
    """
    while True:
        try:
            return q.get(timeout=300)
        except Empty:
            pass


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('evtype',
                        choices=['enable', 'disable', 'sample'],
                        help='Event type')
    parser.add_argument('sensor',
                        nargs='+',
                        help='sensor name')
    parser.add_argument('--host',
                        default='localhost',
                        help='Message broker host (%(default)s)')
    parser.add_argument('--port', '-p',
                        type=int,
                        default=1883,
                        help='Message broker port (%(default)d)')
    parser.add_argument('--timeout', '-t',
                        type=int,
                        default=0,
                        help='timeout in seconds (0 == wait forever)')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    parser.add_argument('-V', '--version',
                        action='version',
                        version='pymc {0}'.format(__version__))
    args = parser.parse_args()

    topics = set()
    for s in args.sensor:
        topics.add('/'.join(['events', args.evtype, s]))

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    for s in (signal.SIGTERM, signal.SIGALRM):
        signal.signal(s, signal_handler)

    q = Queue()
    mb = mqtt.Client(userdata=q, protocol=mqtt.MQTTv31)
    mb.on_connect = on_connect
    mb.on_message = on_message
    mb.connect(args.host, args.port)
    mb.loop_start()

    if args.timeout > 0:
        signal.alarm(args.timeout)

    status = 0
    logging.info('Waiting for messages')
    try:
        while len(topics):
            topic, _ = q_get(q)
            logging.debug('Got event: %s', topic)
            if topic in topics:
                topics.remove(topic)
            logging.debug('%d remaining', len(topics))
    except (KeyboardInterrupt, SystemExit):
        status = 1
    finally:
        signal.alarm(0)
        mb.loop_stop()
    sys.exit(status)


if __name__ == '__main__':
    main()
