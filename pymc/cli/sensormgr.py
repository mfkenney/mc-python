#!/usr/bin/env python
#
"""
Service to manage the sensor sampling process.
"""
import sys
import signal
import logging
import cPickle as pickle
import argparse
import time
from decimal import Decimal
from threading import Event
from Queue import Queue, Empty
from collections import deque
import msgpack
import paho.mqtt.client as mqtt
from pymc.pool import ThreadPool
from pymc.sensors.device import Master
from pymc import __version__


def signal_handler(signum, frame):
    if signum == signal.SIGTERM:
        sys.exit(1)
    else:
        sys.exit(2)


class Messenger(object):
    """
    Use an MQTT Broker to send and receive messages.
    """
    def __init__(self, client, msg_queue, sub=None, qos=1):
        self.client = client
        self.mq = msg_queue
        self.sub = sub
        self.qos = qos
        self.client.on_connect = self.on_connect
        self.client.on_publish = self.on_publish
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.on_disconnect
        self.logger = logging.getLogger()
        self.connected = Event()
        self.published = Event()

    def on_publish(self, client, udata, mid):
        self.logger.debug('Message %r published', mid)
        self.published.set()

    def on_connect(self, client, udata, flags, rc):
        if rc == 0:
            if self.sub:
                self.client.subscribe(self.sub, qos=self.qos)
                self.logger.info('Subscribed to %r', self.sub)
            self.connected.set()

    def on_message(self, client, udata, msg):
        self.logger.debug('> %r', msg)
        self.mq.put((msg.topic, msg.payload))

    def on_disconnect(self, client, udata, rc):
        self.connected.clear()

    def __call__(self, chan, msg, retain=False, qos=1, timeout=5):
        status = self.connected.wait(timeout)
        if not status:
            self.logger.critical('Timeout connecting to server')
            raise IOError
        self.published.clear()
        topic = '/'.join(chan)
        self.logger.debug('Publishing to %s', topic)
        self.client.publish(topic,
                            payload=bytearray(msg),
                            qos=1, retain=retain)
        status = self.published.wait(timeout)
        if not status:
            self.logger.critical('Message delivery timeout')


def str_to_params(line):
    """
    Convert a space separated string of key=value pairs to
    a dictionary.

    >>> str_to_params('foo=bar answer=42')
    {'foo': 'bar', 'answer': '42'}
    """
    return dict([s.split('=', 1) for s in line.split()])


def params_to_str(d):
    return ' '.join(['{0!s}={1!s}'.format(*e) for e in d.iteritems()])


def mp_encoder(obj):
    """
    Encode a Decimal instance for MessagePack.
    """
    if isinstance(obj, Decimal):
        return {'__decimal__': True, 's': str(obj)}
    return obj


def do_sample(name, device, publish, **kwds):
    """
    Enable, sample, and disable a sensor. Then publish the data record
    to an MQTT Broker.
    """
    timestamp = time.time()
    result = b''
    body = params_to_str(kwds)
    count = int(kwds.get('count', '1'))
    store = deque()

    publish(['events', 'enable', name], body, qos=2, timeout=8)
    with device:
        logging.info('Sampling %s', name)
        device.wait_for_ready()
        while count > 0:
            device.pre_sample()
            timestamp, result = device.sample()
            store.append((timestamp, result))
            publish(['events', 'sample', name], body)
            logging.debug('> %.6f %r', timestamp, result)
            count -= 1
    publish(['events', 'disable', name], body, qos=2, timeout=8)

    if isinstance(device, Master):
        for _, result in store:
            for s_name, t, data in result:
                if data:
                    s_secs = int(t)
                    s_usecs = int((t - s_secs)*1e6)
                    publish(['sensors', 'data', s_name],
                            msgpack.packb([s_secs, s_usecs, data],
                                          default=mp_encoder))
    else:
        for timestamp, result in store:
            if result:
                secs = int(timestamp)
                usecs = int((timestamp - secs)*1e6)
                publish(['sensors', 'data', name],
                        msgpack.packb([secs, usecs, result],
                                      default=mp_encoder))


def q_get(q):
    """
    Work-around a shortcoming in the Python 2 Queue implementation
    that disables signals in a blocking call to Queue.get().
    """
    while True:
        try:
            return q.get(timeout=300)
        except Empty:
            pass


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('cfgfile', help='configuration file',
                        type=argparse.FileType('rb'))
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    parser.add_argument('--host',
                        default='localhost',
                        help='Message broker host (%(default)s)')
    parser.add_argument('--port', '-p',
                        type=int,
                        default=1883,
                        help='Message broker port (%(default)d)')
    parser.add_argument('--threads', '-t',
                        type=int,
                        default=4,
                        help='# of worker threads (%(default)d)')
    parser.add_argument('-V', '--version',
                        action='version',
                        version='pymc {0}'.format(__version__))
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(thread)d %(message)s')

    try:
        cfg = pickle.load(args.cfgfile)
    except Exception:
        logging.exception('Error in config file')
        sys.exit(1)

    # Map sensor names to (class, configuration) pairs
    sensors = {}
    slaves = {}
    for k, c in cfg:
        sensors[c['name']] = k, c
        proxy = c.get('interface/proxy', None)
        # Mark this sensor as being a slave to the proxy
        if proxy:
            slaves.setdefault(proxy, []).append(c['name'])

    signal.signal(signal.SIGTERM, signal_handler)

    pool = ThreadPool(args.threads)
    mb = mqtt.Client(client_id='sensor-manager',
                     protocol=mqtt.MQTTv31)
    q = Queue()
    m = Messenger(mb, q, 'sample/#', qos=2)
    mb.connect(args.host, args.port)
    mb.loop_start()

    devices = {}
    try:
        while True:
            topic, body = q_get(q)
            _, name = topic.split('/', 1)
            device = devices.get(name)
            try:
                if not device:
                    klass, config = sensors[name]
                    logging.info('Configuring %s', name)
                    device = klass(config)
                    if not isinstance(device, Master):
                        m(['sensors', 'metadata', name],
                          device.metadata(),
                          retain=True)
                    for slave in slaves.get(config['name'], []):
                        logging.info('Initializing dependent sensor %s', slave)
                        s_klass, s_config = sensors[slave]
                        s_dev = s_klass(s_config)
                        m(['sensors', 'metadata', slave],
                          s_dev.metadata(),
                          retain=True)
                    devices[name] = device
            except KeyError:
                logging.critical('No configuration found for %s', name)
                m(['events', 'disable', name], 'No configuration')
            except Exception:
                logging.exception('Cannot configure %s', name)
                m(['events', 'disable', name],
                  'Configuration failed (see log)')
            else:
                try:
                    params = str_to_params(body)
                except ValueError:
                    params = {}
                    logging.critical('Cannot parse parameters: %r', body)
                pool.add_task(do_sample, name, device, m, **params)
    except (KeyboardInterrupt, SystemExit):
        logging.info('Waiting for tasks to terminate')
        pool.wait_completion()
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Uncaught exception')
    finally:
        mb.loop_stop()


if __name__ == '__main__':
    main()
