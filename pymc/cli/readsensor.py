#!/usr/bin/env python
#
"""
Enable, sample, and disable a single sensor.
"""
import logging
import logging.handlers
import redis
import cPickle as pickle
import sys
import argparse
import msgpack
from decimal import Decimal
from threading import Event
from functools import partial
import paho.mqtt.client as mqtt
from pymc.sensors.device import Master
from pymc import __version__


class MqttPublisher(object):
    def __init__(self, client):
        self.client = client
        self.client.on_connect = self.on_connect
        self.client.on_publish = self.on_publish
        self.logger = logging.getLogger()
        self.connected = Event()
        self.published = Event()

    def on_publish(self, client, udata, mid):
        self.logger.info('Message %r published', mid)
        self.published.set()

    def on_connect(self, client, udata, flags, rc):
        if rc == 0:
            self.connected.set()

    def __call__(self, chan, msg, retain=False):
        status = self.connected.wait(5)
        if not status:
            self.logger.critical('Timeout connecting to server')
            raise IOError
        self.published.clear()
        topic = '/'.join(['sensors']+chan)
        self.logger.info('Publishing to %s', topic)
        result = self.client.publish(topic,
                                     payload=bytearray(msg),
                                     qos=1, retain=retain)
        self.logger.debug('Result: %r', result)
        status = self.published.wait(5)
        if not status:
            self.logger.critical('Message delivery timeout')


def redis_publisher(rd, chan, msg, **kwds):
    rd.publish('.'.join(chan), msg)


def mp_encoder(obj):
    if isinstance(obj, Decimal):
        return {'__decimal__': True, 's': str(obj)}
    return obj


def do_sample(logger, name, device, publish):
    device.enable()
    with device:
        device.wait_for_ready()
        device.pre_sample()
        timestamp, result = device.sample()
        logger.debug('> %f %r', timestamp, result)

    secs = int(timestamp)
    usecs = int((timestamp - secs)*1e6)
    if isinstance(device, Master):
        for s_name, t, data in result:
            s_secs = int(t)
            s_usecs = int((t - s_secs)*1e6)
            publish(['data', s_name],
                    msgpack.packb([s_secs, s_usecs, data],
                                  default=mp_encoder))
    else:
        publish(['data', name],
                msgpack.packb([secs, usecs, result],
                              default=mp_encoder))


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('cfgfile', help='configuration file',
                        type=argparse.FileType('rb'))
    parser.add_argument('name', help='sensor name')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    parser.add_argument('--mqtt', '-m',
                        action='store_true',
                        help='use MQTT message broker rather than Redis')
    parser.add_argument('--host',
                        default='localhost',
                        help='Message broker host (%(default)s)')
    parser.add_argument('--port',
                        type=int,
                        default=0,
                        help='Message broker port (%(default)d)')
    parser.add_argument('-V', '--version',
                        action='version',
                        version='pymc {0}'.format(__version__))
    args = parser.parse_args()
    sensors = pickle.load(args.cfgfile)

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logger = logging.getLogger()
    logger.setLevel(level)
    console = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        '%Y/%m/%d %H:%M:%S')
    console.setFormatter(formatter)
    logger.addHandler(console)

    table = {}
    slaves = {}
    for k, c in sensors:
        table[c['name']] = k, c
        proxy = c.get('interface/proxy', None)
        if proxy:
            slaves.setdefault(proxy, []).append(c['name'])

    if args.mqtt:
        mb = mqtt.Client(protocol=mqtt.MQTTv31)
        publish = MqttPublisher(mb)
        mb.connect(args.host, args.port or 1883)
        mb.loop_start()
    else:
        rd = redis.StrictRedis(host=args.host,
                               port=args.port or 6379)
        publish = partial(redis_publisher, rd)

    try:
        klass, config = table[args.name]
        logging.info('Found sensor configuration %s', args.name)
        device = klass(config)
        if not isinstance(device, Master):
            publish(['metadata', args.name],
                    device.metadata(),
                    retain=True)
        for slave in slaves.get(config['name'], []):
            logging.info('Initializing dependent sensor %s', slave)
            s_klass, s_config = table[slave]
            s_dev = s_klass(s_config)
            publish(['metadata', slave],
                    s_dev.metadata(),
                    retain=True)
    except Exception:
        logging.exception('Cannot initialize sensor: %s', args.name)
        sys.exit(1)

    do_sample(logger, args.name, device, publish)
    if args.mqtt:
        mb.loop_stop()


if __name__ == '__main__':
    main()
