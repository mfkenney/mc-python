#!/usr/bin/env python
"""
.. module:: pool
   :synopsis: implement a pool of worker threads.
   :ref: https://www.metachris.com/2016/04/python-threadpool/
"""
import logging
from Queue import Queue
from threading import Thread


class Worker(Thread):
    """ Thread executing tasks from a given tasks queue """
    def __init__(self, tasks, logger):
        Thread.__init__(self)
        self.tasks = tasks
        self.logger = logger
        self.daemon = True
        self.start()

    def run(self):
        while True:
            func, args, kargs = self.tasks.get()
            self.logger.debug('Calling %s(%r, %r)', func.__name__,
                              args, kargs)
            try:
                func(*args, **kargs)
            except Exception:
                # An exception happened in this thread
                self.logger.exception('%s(%r, %r)', func.__name__,
                                      args, kargs)
            finally:
                # Mark this task as done, whether an exception happened or not
                self.tasks.task_done()


class ThreadPool(object):
    """ Pool of threads consuming tasks from a queue """
    def __init__(self, num_threads, logger=None):
        self.tasks = Queue(num_threads)
        self.logger = logger or logging.getLogger()
        for _ in range(num_threads):
            Worker(self.tasks, self.logger)

    def add_task(self, func, *args, **kargs):
        """ Add a task to the queue """
        self.tasks.put((func, args, kargs))

    def map(self, func, args_list):
        """ Add a list of tasks to the queue """
        for args in args_list:
            self.add_task(func, args)

    def wait_completion(self):
        """ Wait for completion of all the tasks in the queue """
        self.tasks.join()
