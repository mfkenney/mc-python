#!/usr/bin/env python
#
# Implement a simple file-transfer protocol using Micro Modems.
#
#
import time
import binascii
import os
import struct
import logging
import math
from device import PACKET_SIZE, TimeoutError, ModemError
from pymc import nmea


class signals:
    READY = 0xa5a
    XFER_ACK = 0x200
    XFER_NAK = 0x500

mod_logger = logging.getLogger('umodem.protocol')

def tx_signal(um, dest, code):
    """
    Transmit a 'signal' to a remote unit.

    @param um: Umodem instance
    @param dest: remote unit ID
    @param code: signal value
    @param timeout: seconds to wait for delivery confirmation
    @return True if successful, otherwise False
    """
    try:
        um.tx_mini_packet(dest, code)
        return True
    except (TimeoutError, ModemError):
        mod_logger.warning('Signal delivery failed (%04x -> %d)' % (code, int(dest)))
        return False

def rx_signal(um, timeout):
    try:
        return um.rx_mini_packet(timeout)
    except (TimeoutError, ModemError):
        return None, None

class IncomingFile(object):
    """
    Simple class to use with Umodem.uplink_init to store each frame
    as it arrives and track the number of bytes still expected.
    """
    def __init__(self, name, size):
        self.f = open(name, 'wb')
        self.expected = size
        self.logger = logging.getLogger('umodem.protocol.rx')

    def __call__(self, block):
        self.f.write(block)
        self.expected -= len(block)
        self.logger.info('Frame received, %d bytes left' % self.expected)

class OutgoingFile(object):
    """
    Simple class to use with Umodem.downlink_init to provide each frame
    as it is needed and track the number of bytes remaining.
    """
    def __init__(self, name, size, frame_size):
        self.f = open(name, 'rb')
        self.remaining = size
        self.frame_size = frame_size
        self.logger = logging.getLogger('umodem.protocol.tx')

    def next(self):
        block = self.f.read(self.frame_size)
        if not block:
            raise StopIteration
        self.remaining -= len(block)
        self.logger.info('Frame read, %d bytes left' % self.remaining)
        return block

    def __iter__(self):
        return self

def passive_tx_file(um, filename, rname='', timeouts=(30, 10), ack=0):
    """
    Passively transmit a file to a remote unit. Wait for a CADRQ message and
    return a single frame 'header' containing the file name and size. Subsequent
    CADRQ messages will cause successive blocks of the file to be sent.

    @param um: Umodem instance
    @param filename: name of file to transfer
    @param rname: name of file on the remote system.
    @param timeouts: initial request timeout and intra-frame timeout
    """
    logger = logging.getLogger('umodem.protocol.tx')
    info = os.stat(filename)
    if not rname:
        rname = os.path.basename(filename)
    nbytes = info.st_size
    header = struct.pack('>28sL', rname, nbytes)
    logger.info('Ready to transmit file %s' % filename)
    r = um.waitfor(('CADRQ',), timeout=timeouts[0])
    s = nmea.buildSentence('CCTXD', r[1], r[2], ack, binascii.b2a_hex(header))
    if ack:
        um.tx_with_ack(s)
    else:
        um.send(s, response=('CATXD',))
    infile = open(filename, 'rb')
    while nbytes > 0:
        r = um.waitfor(('CADRQ',), timeout=timeouts[1])
        n = int(r[4])
        block = infile.read(n)
        if not block:
            break
        s = nmea.buildSentence('CCTXD', r[1], r[2], ack, binascii.b2a_hex(block))
        if ack:
            um.tx_with_ack(s)
        else:
            um.send(s, response=('CATXD',))
        nbytes -= len(block)
        logger.info('Frame sent, %d bytes left' % nbytes)

def active_rx_file(um, remote_uid, localname='', ptype=6, timeout=10):
    """
    Recieve a file from a remote unit. The file is transfered via a sequence of
    uplink cycles. The first cycle requests a single frame containing the file
    name and size. Subsequent cycles are used to transfer the file contents.

    @param um: Umodem instance.
    @param remote_uid: unit id of remote system.
    @param localname: name of file on local system
    @param ptype: packet type for file transfer
    @param timeout: number of seconds to wait for each packet
    @return: tuple of filename, size
    """
    logger = logging.getLogger('umodem.protocol.rx')
    header = []
    um.uplink_init(int(remote_uid), lambda f: header.append(f), timeout=timeout)
    filename, size = struct.unpack('>28sL', header[0])
    filename = filename.strip('\x00')
    logger.info('Ready to receive file %s (%d bytes)' % (filename, size))
    maxframes, frame_size = PACKET_SIZE[ptype]
    psize = maxframes*frame_size
    outfile = IncomingFile(localname or filename, size)
    t0 = time.time()
    while outfile.expected > 0:
        frames = int(math.ceil(min(outfile.expected, psize)/float(frame_size)))
        um.uplink_init(int(remote_uid), outfile, ptype=ptype, frames=frames)
    t = time.time() - t0
    logger.info('File transfer time %.3f seconds' % t)
    return filename, size

def active_tx_file(um, dest, filename, rname='', ptype=3, ack=0):
    """
    Actively transmit a file to a remote unit (downlink). The first cycle uses
    packet type 0 to send a single header frame containing the file name and
    size. Subsequent packets use 'ptype' as the packet type.

    @param um: Umodem instance
    @param filename: name of file to transfer
    @param rname: name of file on the remote system.
    @param ptype: packet type
    """
    logger = logging.getLogger('umodem.protocol.tx')
    info = os.stat(filename)
    if not rname:
        rname = os.path.basename(filename)
    nbytes = info.st_size
    header = struct.pack('>28sL', rname, nbytes)
    logger.info('Ready to transmit file %s' % filename)
    um.downlink_init(int(dest), iter([header]))
    maxframes, frame_size = PACKET_SIZE[ptype]
    psize = maxframes*frame_size
    infile = OutgoingFile(filename, nbytes, frame_size)
    t0 = time.time()
    while infile.remaining > 0:
        frames = int(math.ceil(min(infile.remaining, psize)/float(frame_size)))
        um.downlink_init(int(dest), iter(infile), ptype=ptype, frames=frames)
    t = time.time() - t0
    logger.info('File transfer time %.3f seconds' % t)

def passive_rx_file(um, localname='', timeouts=(30, 10)):
    """
    Recieve a file from a remote unit. The file is transfered via a sequence of
    downlink cycles. The first cycle consists of a single frame containing the file
    name and size. Subsequent cycles are used to transfer the file contents.

    @param um: Umodem instance.
    @param localname: name to give file on local system.
    @return: tuple of filename, size
    """
    logger = logging.getLogger('umodem.protocol.rx')
    r = um.waitfor(('CARXD',), timeout=timeouts[0])
    filename, size = struct.unpack('>28sL', binascii.a2b_hex(r[4]))
    filename = filename.strip('\x00')
    logger.info('Ready to receive file %s (%d bytes)' % (filename, size))
    outfile = IncomingFile(localname or filename, size)
    t0 = time.time()
    while outfile.expected > 0:
        r = um.waitfor(('CARXD',), timeout=timeouts[1])
        outfile(binascii.a2b_hex(r[4]))
    t = time.time() - t0
    logger.info('File transfer time %.3f seconds' % t)
    return filename, size

if __name__ == '__main__':
    import sys
    from device import Umodem

    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(levelname)s - %(message)s')
    logger = logging.getLogger()
    dev, src, dest = sys.argv[1:4]
    um = Umodem(dev, uid=int(src))
    um.setclock()
    um['DTO'] = 5
    um['PTO'] = 7
    try:
        passive_tx_file(um, sys.argv[4])
    except IndexError:
        name, size = active_rx_file(um, int(dest), ptype=3)
        print name, size
    um.close()
