#!/usr/bin/env python
#
# Interface to WHOI Micro Modem
#
import time
import serial
import fcntl
import threading
import Queue
import binascii
import logging
from pymc import nmea


# Packet_type => (Frames, Bytes/Frame)
# Only currently implemented packet types are included.
PACKET_SIZE = { 0: (1, 32),
                2: (3, 64),
                3: (2, 256),
                5: (8, 256),
                6: (6, 32) }

class LinkError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return 'Link Error:' + self.msg

class TimeoutError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return 'Timeout:' + self.msg

class ModemError(Exception):
    def __init__(self, args):
        self.module = args[1]
        self.code = args[2]
        self.message = args[3]

    def __str__(self):
        return self.message

class NmeaReader(threading.Thread):
    def __init__(self, port, queue):
        threading.Thread.__init__(self)
        self.port = port
        self.queue = queue
        self.done = True
        self.logger = logging.getLogger('umodem.nmea')
        self.setDaemon(True)

    def stop(self):
        self.done = True

    def run(self):
        self.done = False
        while not self.done:
            line = self.port.readline()
            if line:
                self.queue.put(line)
                self.logger.debug(line.rstrip())

class Umodem(object):
    lock_file = '/var/lock/umodem.lock'
    eol = '\r\n'

    def __init__(self, device, uid=0, baud=19200):
        """
        Instance initializer.

        @param device: serial device
        """
        self.lfile = open(self.lock_file, 'w')
        fcntl.flock(self.lfile, fcntl.LOCK_EX)
        self.ignore_errors = False
        self.logger = logging.getLogger('umodem')
        self.msgqueue = Queue.Queue()
        self.port = serial.Serial(device, baud, timeout=5)
        self.reader = NmeaReader(self.port, self.msgqueue)
        self.reader.start()
        self.uid = uid
        self['REV'] = 0
        self['SRC'] = uid
        self['RXD'] = 1
        self.logger.info('Modem ready on %s' % device)

    def close(self):
        self.reader.stop()
        self.reader.join(6)

    def waitfor(self, msg_ids, timeout=5):
        """
        Wait for specific NMEA messages or timeout.
        """
        while True:
            try:
                line = self.msgqueue.get(timeout=timeout)
                s = nmea.Sentence(line)
            except (nmea.BadFormat, nmea.ChecksumError):
                continue
            except Queue.Empty:
                raise TimeoutError('Expecting %s' % str(msg_ids))
            if s.id in msg_ids:
                return s
            elif s.id == 'CAERR' and not self.ignore_errors:
                raise ModemError(s)

    def send(self, s, response=(), timeout=5):
        """
        Send a message to the device

        @param s: NMEA message
        @type s: nmea.Sentence
        @param response: tuple of expected responses (NMEA message code)
        @param timeout: maximum number of seconds to wait for a response
        @return: response message
        """
        self.port.write(str(s) + self.eol)
        if response:
            return self.waitfor(response, timeout)

    def __setitem__(self, name, value):
        """
        Set an NVRAM configuration variable.
        """
        s = nmea.buildSentence('CCCFG', name.upper(), value)
        self.send(s, response=('CACFG',))

    def __getitem__(self, name):
        """
        Get the value of an NVRAM configuration variable.
        """
        s = nmea.buildSentence('CCCFQ', name.upper())
        r = self.send(s, response=('CACFG',))
        return r[1]

    def setclock(self):
        """
        Set the modem clock to the current UTC time.
        """
        self.logger.info('Setting clock')
        t = int(time.time()) + 3
        args = time.strftime('%Y %m %d %H %M %S', time.gmtime(t)).split()
        s = nmea.buildSentence('CCCLK', *args)
        while time.time() < t:
            pass
        return self.send(s, response=('CACLK',))

    def ping(self, dest, timeout=10):
        """
        Ping a remote unit.

        @param dest: destination unit ID
        @param timeout: maximum number of seconds to wait.
        @return one-way travel time in seconds or -1 if no response
        """
        self.logger.info('Pinging unit %d' % dest)
        s = nmea.buildSentence('CCMPC', self.uid, dest)
        self.send(s, response=('CAMPC',))
        try:
            dest = -1
            while dest != self.uid:
                r = self.waitfor(('CAMPR',), timeout=timeout)
                dest = int(r[1])
            tt = float(r[2])
        except TimeoutError:
            tt = -1
        return tt

    def tx_mini_packet(self, dest, data):
        """
        Send a mini-packet to a remote unit.

        @param dest: destination unit ID
        @param data: user data value (0 - 0x1fff)
        @return modem reply sentence.
        """
        data = data & 0x1fff
        self.logger.info('Sending 0x%04x to unit %d' % (data, dest))
        s = nmea.buildSentence('CCMUC', self.uid, dest, '%04x' % data)
        return self.send(s, response=('CAMUC',))

    def rx_mini_packet(self, timeout):
        """
        Wait for a mini-packet.

        @param timeout: time to wait in seconds
        @return tuple of source, data or None, None on timeout
        """
        try:
            dest = -1
            while dest != self.uid:
                r = self.waitfor(('CAMUA',), timeout=timeout)
                dest = int(r[1])
            return int(r[0]), int(r[2], 16)
        except TimeoutError:
            return None, None

    def tx_with_ack(self, s, tries=2):
        assert s.id == 'CCTXD'
        while tries > 0:
            self.send(s, response=('CATXD',))
            try:
                self.waitfor(('CAACK',))
                return
            except TimeoutError:
                pass
            tries -= 1
        raise TimeoutError('No ACK from remote unit')

    def downlink_init(self, dest, frame_source, ptype=0, frames=1, timeout=10,
                      ack=0):
        """
        Initiate a downlink cycle.

        @param dest: destination unit ID
        @param frame_source: generator to produce data frames
        @param ptype: packet type
        @param frames: frame count
        @param timeout: maximum number of seconds to wait for CADRQ
        """
        assert frames <= PACKET_SIZE[ptype][0]
        self.logger.info('Initiating downlink of %d frames to unit %d' % (frames, dest))
        s = nmea.buildSentence('CCCYC', 1, self.uid, dest, ptype, 0, frames)
        self.send(s, response=('CACYC',))
        idx = 0
        while idx < frames:
            frame = frame_source.next()
            self.waitfor(('CADRQ',), timeout=timeout)
            s = nmea.buildSentence('CCTXD', self.uid, dest, ack, binascii.b2a_hex(frame))
            if ack:
                self.tx_with_ack(s)
            else:
                self.send(s, response=('CATXD',))
            idx += 1

    def uplink_init(self, src, frame_sink, ptype=0, frames=1, timeout=10):
        """
        Initiate an uplink cycle.

        @param src: source unit ID
        @param frame_sink: callable to accept data frames
        @param ptype: packet type (data ptype)
        @param frames: frame count
        @param timeout: maximum number of seconds to wait for CARXD
        """
        assert frames <= PACKET_SIZE[ptype][0]
        self.logger.info('Initiating uplink of %d frames from unit %d' % (frames, src))
        s = nmea.buildSentence('CCCYC', 1, src, self.uid, ptype, 0, frames)
        self.send(s, response=('CACYC',))
        idx = 0
        while idx < frames:
            r = self.waitfor(('CARXD',), timeout=timeout)
            frame_sink(binascii.a2b_hex(r[4]))
            idx += 1
