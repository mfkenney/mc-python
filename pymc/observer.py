#!/usr/bin/env python
#
# Class to implement the Observer pattern.
#
# Ref: http://www.suttoncourtenay.org.uk/duncan/accu/pythonpatterns.html
#


class Delegate(object):
    """
    Handles a list of methods and functions
    Usage:
        d = Delegate()
        d += function    # Add function to end of delegate list
        d(*args, **kw)   # Call all functions, returns a list of results
        d -= function    # Removes last matching function from list
        d -= object      # Removes all methods of object from list
    """
    def __init__(self):
        self._delegates = []

    def __iadd__(self, callback):
        self._delegates.append(callback)
        return self

    def __isub__(self, callback):
        # If callback is a class instance,
        # remove all callbacks for that instance
        self._delegates = [cb for cb in self._delegates
                           if getattr(cb, 'im_self', None) != callback]

        # If callback is callable, remove the last
        # matching callback
        if callable(callback):
            for i in range(len(self._delegates)-1, -1, -1):
                if self._delegates[i] == callback:
                    del self._delegates[i]
                    return self
        return self

    def __call__(self, *args, **kw):
        return [callback(*args, **kw)
                for callback in self._delegates]


class Event(Delegate):
    """
    Class event notifier
    Usage:
        class C:
            TheEvent = Event()
            def OnTheEvent(self):
                self.TheEvent(context)

        instance = C()
        instance.TheEvent += callback
        instance.OnTheEvent()
        instance.TheEvent -= callback
    """
    pass
