#!/usr/bin/env python
#
# Manage power switches.
#
from weakref import WeakValueDictionary
import logging
import time
import subprocess
from pytsctl.client import TsClient
from pytsctl.system import map_lookup_msg
from pytsctl.dio import setasync_msg


def tsctl_set(name, val):
    """
    Call the `tsctl` program to set the state of a DIO line.
    """
    cmd = ['sudo', 'tsctl', 'DIO', 'setasync']
    state = 'LOW' if val == 0 else 'HIGH'
    return subprocess.call(cmd + [state])


class FakeDio(object):
    """
    Fake DIO port for testing the PowerSwitch class
    """
    def __init__(self):
        self.inreg = 0
        self.outreg = 0
        self.outputs = 0

    def set(self, mask):
        mask &= self.outputs
        self.outreg |= mask

    def clear(self, mask):
        mask &= self.outputs
        self.outreg &= ~mask

    def test(self, line):
        mask = 1 << line
        if mask & self.outputs:
            val = self.outreg
        else:
            val = self.inreg
        return (val & mask) == mask


class _Switch(object):
    """
    Class to use Python's built-in reference counting to manage the
    state of a power switch. Creating a new object instance turns on
    the switch and deleting the instance turns it off.
    """
    _table = WeakValueDictionary()

    def __new__(cls, idx, port):
        sw = _Switch._table.get(idx)
        if not sw:
            sw = object.__new__(cls)
            sw.name = 'sw-{0}'.format(idx)
            sw.idx = int(idx)
            sw.t_on = time.time()
            sw.port = port
            logging.info('Power on %s', sw.name)
            port.set(1 << sw.idx)
            _Switch._table[sw.idx] = sw
        return sw

    def __del__(self):
        logging.info('Power off %s', self.name)
        self.port.clear(1 << self.idx)


class _TsSwitch(object):
    """
    Variant of :cls:`_Switch` which uses the Tsctl server to
    access the DIO lines.
    """
    _table = WeakValueDictionary()

    def __new__(cls, name, client):
        sw = cls._table.get(name)
        if not sw:
            sw = object.__new__(cls)
            sw.name = name
            resp = client.sendreq(map_lookup_msg(name))
            sw.idx = int(resp[0])
            sw.t_on = time.time()
            sw.client = client
            logging.info('Power on %s', sw.name)
            client.sendreq(setasync_msg(sw.idx, 1))
            cls._table[sw.name] = sw
        return sw

    def __del__(self):
        logging.info('Power off %s', self.name)
        self.client.sendreq(setasync_msg(self.idx, 0))


class _TsProcSwitch(object):
    """
    Variant of :cls:`_Switch` which uses the Tsctl program to
    access the DIO lines.
    """
    _table = WeakValueDictionary()

    def __new__(cls, name, *args):
        sw = cls._table.get(name)
        if not sw:
            sw = object.__new__(cls)
            sw.name = name
            sw.t_on = time.time()
            logging.info('Power on %s', sw.name)
            if tsctl_set(sw.name, 1) != 0:
                logging.warning('Switch not set')
            cls._table[sw.idx] = sw
        return sw

    def __del__(self):
        logging.info('Power off %s', self.name)
        if tsctl_set(self.name, 0) != 0:
            logging.warning('Switch not cleared')


class PowerSwitch(object):
    """
    Class to manage a power switch.

    >>> import logging
    >>> logging.basicConfig(level=logging.INFO)
    >>> sw1 = PowerSwitch(1, FakeDio())
    >>> sw2 = PowerSwitch(1, FakeDio())
    >>> sw1.on()
    INFO:root:Power on sw-1
    >>> sw2.on()
    >>> sw1.off()
    >>> sw2.off()
    INFO:root:Power off sw-1
    """
    def __init__(self, idx, port):
        self.idx = idx
        self.refs = []
        self.port = port

    def on(self):
        self.refs.append(_Switch(self.idx, self.port))

    def off(self):
        if self.refs:
            self.refs.pop()
        else:
            # Bit of a hack here so we can force a
            # switch off before PowerSwitch.on has
            # been called.
            self.port.clear(1 << self.idx)

    def test(self):
        return len(self.refs) > 0

    def timer(self):
        """
        Return the switch on time in seconds or zero if the
        switch is off.
        """
        try:
            return time.time() - self.refs[-1].t_on
        except IndexError:
            return 0


class TsPowerSwitch(object):
    """
    Variant of :cls:`PowerSwitch` which uses the Tsctl server
    or the Tsctl program to access the DIO lines.
    """
    def __init__(self, name, client=None):
        """
        Instance initializer.

        :param name: DIO line name
        :param client: TsClient instance. If set to ``None``, use
                       the Tsctl program directly.
        """
        self.name = name
        self.refs = []
        if client:
            self.client = client
            self.swclass = _TsSwitch
        else:
            self.client = None
            self.swclass = _TsProcSwitch

    def on(self):
        """
        Turn switch on and increment reference count.
        """
        self.refs.append(self.swclass(self.name, self.client))

    def off(self):
        """
        Decrement reference count. When count reaches zero, the
        switch is turned off.
        """
        if self.refs:
            self.refs.pop()

    def test(self):
        """
        Return boolean switch state.
        """
        return len(self.refs) > 0

    def timer(self):
        """
        Return the switch on time in seconds or zero if the
        switch is off.
        """
        try:
            return time.time() - self.refs[-1].t_on
        except IndexError:
            return 0
