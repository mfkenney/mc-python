#!/usr/bin/env python
"""
"""


class pathdict(dict):
    """
    Implement a multi-level dictionary where the keys are
    specified as paths:

    d['a/b/c'] = d['a']['b']['c']
    """
    _sep = '/'

    def __getitem__(self, key):
        parts = list(key.split(self._sep))
        val = super(pathdict, self).__getitem__(parts.pop(0))
        while isinstance(val, dict) and parts:
            val = val.__getitem__(parts.pop(0))
        if parts:
            raise KeyError
        return val

    def __contains__(self, key):
        try:
            _ = self[key]
        except KeyError:
            return False
        return True

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default
