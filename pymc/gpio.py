#!/usr/bin/env python
#
# $Id: gpio.py,v e92f6d4c51d9 2008/10/14 23:37:33 mikek $
#
from __future__ import with_statement
from threading import Lock

class Gpio(object):
    """
    Class to provide thread-safe access to the GPIO lines on a Gumstix. In order
    to use this class, the proc-gpio kernel module must loaded.

    A cache is used to insure that only a single instance is created for each
    line.  Each instance uses a state counter which is incremented in the
    'set' method and decremented in the 'clear' method, only when the counter
    reaches zero is the switch actually disabled.
    
    """
    
    _lines = {}
    def __new__(cls, gpio):
        """
        Return a new Gpio object or one from the cache.

        @param gpio: GPIO line number
        @type gpio: C{int}
        """
        if not gpio in cls._lines:
            l = object.__new__(cls)
            l.filename =  cls._gpiofilename(gpio)
            l._state = 0
            l.lock = Lock()
            l.line = gpio
            cls._lines[gpio] = l
        return cls._lines[gpio]

    @staticmethod
    def _gpiofilename(gpio):
        return '/proc/gpio/GPIO%d' % gpio
    
    def __init__(self, gpio):
        self._state = self.test()

    def __getnewargs__(self):
        return (self.line,)

    def __getstate__(self):
        odict = self.__dict__.copy()
        del odict['lock']
        return odict

    def __setstate__(self, d):
        self.__dict__.update(d)
        self._state = self.test()
        self.lock = Lock()
        
    def __str__(self):
        return '<Gpio %s %d>' % (self.filename, self._state)

    def __enter__(self):
        self.set()

    def __exit__(self, *args):
        self.clear()
        return False

    def __nonzero__(self):
        return self._state > 0

    def test(self):
        with self.lock:
            line = open(self.filename, 'r').readline()
            fields = line.split()
            return (fields[-1] == 'set') and 1 or 0
        
    def set(self):
        """
        Enable the line and increment the state counter.
        """
        with self.lock:
            self._state += 1
            try:
                open(self.filename, 'w').write('GPIO out set\n')
            except IOError:
                raise ValueError('Invalid GPIO line, %s' % self.filename)
        return self._state > 0
    
    def clear(self, force=False):
        """
        Decrement the state counter and disable the line
        if the counter has reached zero.

        @param force: force the counter to zero
        """
        assert self._state > 0
        with self.lock:
            self._state -= 1
            if force:
                self._state = 0
            if self._state == 0:
                try:
                    open(self.filename, 'w').write('GPIO out clear\n')
                except IOError:
                    raise ValueError('Invalid GPIO line, %s' % self.filename)
        return self._state > 0


class DummyGpio(Gpio):
    def set(self):
        with self.lock:
            self._state += 1
        return self._state > 0

    def clear(self, force=False):
        assert self._state > 0
        with self.lock:
            self._state -= 1
            if force:
                self._state = 0
        return self._state > 0

    def test(self):
        return self._state
    
if __name__ == '__main__':
    a = Gpio(1)
    b = Gpio(2)
    c = Gpio(1)
    print a, b, c
    print a.filename
    print b.filename
    print c.filename
